<?php

require_once(BP . DS . 'lib' . DS . 'Sunovisio' . DS . 'Tcpdf' . DS . 'tcpdf.php');

class Unleaded_WishlistPrinter_Model_Wishlist extends Mage_Core_Model_Abstract {

    CONST PAGE_WIDTH = 550;
    CONST COL_WIDTH_1 = '20%';
    CONST COL_WIDTH_2 = '30%';
    CONST COL_WIDTH_3 = '15%';
    CONST COL_WIDTH_4 = '15%';
    CONST COL_WIDTH_5 = '8%';
    CONST COL_WIDTH_6 = '12%';

    public function getWishlistItems($wishlistId) {
        $tableItoris = 'itoris_mwishlist_items';

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerId = $customer->getId();
        $wishlist = Mage::getModel('wishlist/wishlist');
        $wishlist->loadByCustomer($customer, true);

        $collection = Mage::getResourceModel('itoris_mwishlist/item_collection');
        $collection->addWishlistFilter($wishlist);
        $collection->getSelect()->join($tableItoris, "wishlist_item_id = $tableItoris.item_id");
        $collection->getSelect()->where("multiwishlist_id = $wishlistId");
        $collection->setOrderByProductAttribute('name', 'asc');
        $collection->getSelect()->group('main_table.wishlist_item_id');

        return $collection;
    }

    public function getWishlistPdf($pdfHtml) {
        $pageOrientation = 'P';
        $unit = 'px';
        $pageFormat = 'A4';
        
        $pdf = new TCPDF($pageOrientation, $unit, $pageFormat, true, 'UTF-8', false);

        //$fontname = $pdf->addTTFfont(BP . DS . 'lib' . DS . 'LinLibertineFont' . DS . 'LinLibertineC_Re-2.8.0.ttf', 'TrueTypeUnicode', '', 96);
        $fontname = $pdf->addTTFfont(BP . DS . 'skin' . DS . 'frontend' . DS . 'liton' . DS . 'default' . DS . 'fonts' . DS . 'myriad-web-pro.ttf', 'TrueTypeUnicode', '', 96);

        $pdf->SetFont($fontname, '', 12);

        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        $pdf->AddPage();

        $pdf->setJPEGQuality(100);
        
        $pdf->writeHTML($pdfHtml, true, false, true, false, '');

        return $pdf;
    }

}

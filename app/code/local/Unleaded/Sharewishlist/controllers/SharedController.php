<?php

require_once Mage::getModuleDir('controllers', 'Itoris_MWishlist') . DS . 'SharedController.php';

class Unleaded_Sharewishlist_SharedController extends Itoris_MWishlist_SharedController {

    protected function _getWishlist() {
        $code = (string) $this->getRequest()->getParam('code');
        if (empty($code)) {
            return false;
        }

        $wishlist = Mage::getModel('wishlist/wishlist')->loadByCode($code);

        $collection = $wishlist->getItemCollection()
                ->setVisibilityFilter();
        if ($this->getRequest()->getParam('mw', '')) {
            $this->prepareCollection($collection);
        }

        if (!$wishlist->getId()) {
            return false;
        }

        Mage::getSingleton('checkout/session')->setSharedWishlist($code);

        return $wishlist;
    }

}

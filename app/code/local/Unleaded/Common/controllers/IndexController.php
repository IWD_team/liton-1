<?php

class Unleaded_Common_IndexController extends Mage_Core_Controller_Front_Action {
    public function indexAction() {
        $simpleId = $this->getRequest()->getPost('simpleId');
        
        $_product = Mage::getModel('catalog/product')->load($simpleId);
        $wishlistUrl = Mage::helper('wishlist')->getAddUrl($_product);
        echo $wishlistUrl;
    }
}

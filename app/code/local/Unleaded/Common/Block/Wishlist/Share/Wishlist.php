<?php

class Unleaded_Common_Block_Wishlist_Share_Wishlist extends Mage_Wishlist_Block_Share_Wishlist {
    public function getHeader()
    {
        $nameArr = Mage::getModel('itoris_mwishlist/mwishlistnames')->getNameById($this->_getWishlist()->getId());
        return Mage::helper('wishlist')->__("%s Project Folder", $this->escapeHtml($nameArr['multiwishlist_name']));
    }
}

<?php
class Unleaded_ProductImport_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getCategoryTree($line,$field,$split = ";") {

        if(is_array($line[$field])){
            $line[$field] = implode($split, $line[$field]);
        }

        if($line[$field] == ""):
            return;
        endif;

        $categories = explode($split,$line[$field]);

        $cattmp = array();

        foreach ($categories as $category => $value):
            $path = explode("/",$this->_getCategoryPath($value));
            $tmp = array();
            foreach ($path as $cat):
                if($cat != 1):
                    $tmp[] = $this->_getCategoryName($cat);
                endif;
            endforeach;
            $cattmp[] = implode("/", $tmp);
        endforeach;

        return $cattmp;
    }

    public function getStatus($line,$field) {

        $status = $line[$field];

        if($status == 0):
            $status = 2;
        endif;

        return $status;

    }

    public function capitalize($line,$field) {

        $value = $line[$field];

        if(is_array($value)):
            return $value;
        endif;

        return ucwords($value);

    }

    public function setPrice($line, $field)
    {

        $s = $this->_getMapper()->mapItem($field);

        if(count($s) == 0):
            return 0;
        endif;

        // return float
        return (float) $this->_formatPrice($s) * 2;
    }

    public function setGroupPrice($line, $field)
    {

        $s = $this->_getMapper()->mapItem($field);

        if(count($s) == 0):
            return 0;
        endif;

        // return float
        return (float) $this->_formatPrice($s) * 1.1;
    }

    public function parsePrice($line, $field)
    {

        $s = $this->_getMapper()->mapItem($field);

        if(count($s) == 0):
            return 0;
        endif;

        // return float
        return (float) $this->_formatPrice($s);
    }

    public function fixHttps($line, $field)
    {

        $s = $this->_getMapper()->mapItem($field);

        $s = str_replace("http://", "https://", $s);

        return $s;
    }

    public function makeArray($line, $field)
    {
        die(print_r($line));
        $s = $this->_getMapper()->mapItem($field);

        $s = str_replace("http://", "https://", $s);

        return $s;
    }

    public function checkIfValid($line)
    {
        $items = $line->getItems();
        $skip = false;

        if($this->_checkProduct($items[0]['sku'])):
            $skip = true;
        endif;

        if($skip):
            return $line->setSkip(1);
        endif;

    }

    public function mapCategories($line)
    {
        $tmp = array();
        $tmp[] = $line['Main Category'];
        $tmp[] = $line['Main Category'] . "/" . $line['Sub cat1'];
        $tmp[] = $line['Main Category'] . "/" . $line['Sub cat1'] . "/" . $line['sub cat2'];
        $tmp[] = $line['Main Category'] . "/" . $line['Sub cat1'] . "/" . $line['sub cat2'] . "/" . $line['Sub Cat3'];
        return $tmp;
    }

    public function catArray($line,$value)
    {
        $tmp = array($value,$value,$value,$value);
        return $tmp;
    }


    /**
     * @return Ho_Import_Model_Mapper
     */
    protected function _formatPrice($price)
    {
        if(is_array($price)):
            return $price;
        endif;

        // convert "," to "."
        $price = str_replace(',', '.', $price);

        // remove everything except numbers and dot "."
        $price = preg_replace("/[^0-9\.]/", "", $price);

        // remove all seperators from first part and keep the end
        $price = str_replace('.', '', substr($price, 0, -3)) . substr($price, -3);

        return $price;
    }


    /**
     * @return Ho_Import_Model_Mapper
     */
    protected function _getCategoryPath($name)
    {
        $category = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('name', $name)->getFirstItem();
        return $category->getPath();
    }


    /**
     * @return Ho_Import_Model_Mapper
     */
    protected function _getCategoryName($id)
    {
        $category = Mage::getModel('catalog/category')->load($id);
        return $category->getName();
    }


    /**
     * @return Ho_Import_Model_Mapper
     */
    protected function _getMapper()
    {
        return Mage::getSingleton('ho_import/mapper');
    }

    /**
     * @return Boolean
     */
    protected function _checkProduct($sku)
    {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
        if(!is_object($product)):
            return false;
        endif;
        return true;
    }

}

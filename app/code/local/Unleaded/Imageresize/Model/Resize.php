<?php class Unleaded_Imageresize_Model_Resize extends Mage_Catalog_Model_Category 
{

    public function getResizedImage($width, $height = null, $thumbnail) 
	{
		
        $_file_name = $thumbnail; // Here $cat is category data array                
        $_media_dir = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'category' . DS;
        $cache_dir = $_media_dir . 'cache' . DS; // Here i create a resize folder. for upload new category image
		if (!is_dir($cache_dir)) {
                mkdir($cache_dir);
            }
        if (getimagesize($cache_dir . $_file_name)) {
            $catImg = Mage::getBaseUrl('media') . 'catalog' . DS . 'category' . DS . 'cache' . DS . $_file_name;
            $image_size = getimagesize($catImg);

            if ($image_size[0] == $width && $image_size[1] == $height) {
                return $catImg;
            } else {
                unlink($cache_dir . $_file_name);
                if (!file_exists($_media_dir . $_file_name)) {
                    $default_img = $_media_dir . 'default_image.jpg';
                } else {
                    $default_img = $_media_dir . $_file_name;
                }
                $_image = new Varien_Image($default_img);
                $_image->constrainOnly(true);
                $_image->keepAspectRatio(true);
                $_image->keepFrame(false);
                $_image->keepTransparency(true);
                $_image->resize($width, $height);
                $_image->save($cache_dir . $_file_name);
                $catImg = Mage::getBaseUrl('media') . 'catalog' . DS . 'category' . DS . 'cache' . DS . $_file_name;
                return $catImg;
            }
        } elseif (getimagesize($_media_dir . $_file_name)) {

            $_image = new Varien_Image($_media_dir . $_file_name);
            $_image->constrainOnly(true);
            $_image->keepAspectRatio(true);
            $_image->keepFrame(false);
            $_image->keepTransparency(true);
            $_image->resize($width, $height);
            $_image->save($cache_dir . $_file_name);
            $catImg = Mage::getBaseUrl('media') . 'catalog' . DS . 'category' . DS . 'cache' . DS . $_file_name;
            return $catImg;
        } else {
            $_file_name = "default_image.jpg";
            unlink($cache_dir . $_file_name);
            $_image = new Varien_Image($_media_dir . $_file_name);
            $_image->constrainOnly(true);
            $_image->keepAspectRatio(true);
            $_image->keepFrame(false);
            $_image->keepTransparency(true);
            $_image->resize($width, $height);
            $_image->save($cache_dir . $_file_name);
            $catImg = Mage::getBaseUrl('media') . 'catalog' . DS . 'category' . DS . 'cache' . DS . $_file_name;
            return $catImg;
        }
    }

}

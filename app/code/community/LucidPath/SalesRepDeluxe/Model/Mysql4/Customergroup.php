<?php
class LucidPath_SalesRepDeluxe_Model_Mysql4_Customergroup extends Mage_Core_Model_Mysql4_Abstract {

  public function _construct() {
    $this->_init('salesrep/customergroup', 'id');
  }
}

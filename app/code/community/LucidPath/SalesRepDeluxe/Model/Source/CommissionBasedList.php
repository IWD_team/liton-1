<?php
class LucidPath_SalesRepDeluxe_Model_Source_CommissionBasedList {

  public function toOptionArray() {
    $result = array();
    $result[] = array('value' => '1', 'label' => 'Order Subtotal');
    $result[] = array('value' => '2', 'label' => 'Employee Commission');

    return $result;
  }
}
?>

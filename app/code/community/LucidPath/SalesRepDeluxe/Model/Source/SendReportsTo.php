<?php
class LucidPath_SalesRepDeluxe_Model_Source_SendReportsTo {
  const EMPLOYEE_ONLY       = 1;
  const EMPLOYEE_AND_ADMIN  = 2;

  public function toOptionArray() {
    return array(
        array(
            'label' => 'Sales Rep & Manager',
            'value' => self::EMPLOYEE_ONLY),
        array(
            'label' => 'Sales Rep, Manager, & Admin',
            'value' => self::EMPLOYEE_AND_ADMIN)
    );
  }
}
?>

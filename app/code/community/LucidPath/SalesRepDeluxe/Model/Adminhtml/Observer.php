<?php
class LucidPath_SalesRepDeluxe_Model_Adminhtml_Observer {

  public function addColumnsToOrderGrid(Mage_Adminhtml_Block_Sales_Order_Grid $block) {
    if ($collection = $block->getCollection()) {
      $joined_tables = array_keys($collection->getSelect()->getPart('from'));

      if (!in_array('salesrep', $joined_tables)) {
        $order_detail_page     = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/access_order_detail_page');
        $order_detail_page_all = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/access_order_detail_page/all_orders');
        $order_detail_page_sub = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/access_order_detail_page/orders_of_subordinate');
        $order_detail_page_own = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/access_order_detail_page/own_orders_only');

        if ($order_detail_page) {
          $collection->getSelect()->joinLeft(array('salesrep' => $collection->getTable('salesrep/salesrep')), 'salesrep.order_id=main_table.entity_id');

          if (!$order_detail_page_all) {
            $admin_user_ids = array();

            if ($order_detail_page_sub) {
              $admin_user_collection = Mage::getResourceModel('admin/user_collection');
              $admin_user_collection->addFieldToFilter('salesrep_manager_id', Mage::getSingleton('admin/session')->getUser()->getId());

              foreach ($admin_user_collection as $admin_user) {
                $admin_user_ids[] = $admin_user->getId();
              }
            }

            $admin_user_ids[] = Mage::getSingleton('admin/session')->getUser()->getId();

            $collection->addAttributeToFilter('salesrep.rep_id', array('in' => $admin_user_ids));
          }


          $view_rep_comm_ps = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_grid/view_rep_commission_status');

          if ($view_rep_comm_ps) {
            $block->addColumnAfter('rep_commission_status',
                                   array(
                                         'header' => Mage::helper('salesrep')->__('Comm. Status'),
                                         'index' => 'rep_commission_status',
                                         'type'  => 'options',
                                         'align' => 'center',
                                         'width' => '10px',
                                         'options' => Mage::helper('salesrep')->getStatusListFilter(),
                                         'renderer' => 'LucidPath_SalesRepDeluxe_Block_Adminhtml_Order_Grid_Renderer_PaymentStatus',
                                         'options' => Mage::helper('salesrep')->getCommissionStatusList()),
                                   'status');
          }

          $view_rep_comm_amount = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_grid/view_rep_commission_amount');

          if ($view_rep_comm_amount) {
            $block->addColumnAfter('rep_commission_earned',
                                   array(
                                         'header' => Mage::helper('salesrep')->__('Comm. Amount'),
                                         'index' => 'rep_commission_earned',
                                         'align' => 'center',
                                         'width' => '10px',
                                         'renderer' => 'LucidPath_SalesRepDeluxe_Block_Adminhtml_Order_Grid_Renderer_Amount'),
                                   'status');
          }

          $view_rep_name = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_grid/view_rep_name');

          if ($view_rep_name) {
            $block->addColumnAfter('rep_name',
                                   array(
                                         'header' => Mage::helper('salesrep')->__('Comm. Earner'),
                                         'index' => 'rep_name',
                                         'align' => 'center',
                                         'width' => '10px',
                                         'renderer' => 'LucidPath_SalesRepDeluxe_Block_Adminhtml_Order_Grid_Renderer_Earner'),
                                   'status');
          }

          $block->sortColumnsByOrder();


          $filter = $block->getParam($block->getVarNameFilter(), null);

          if (is_string($filter)) {
            $filter = $block->helper('adminhtml')->prepareFilterString($filter);
          } else if ($filter && is_array ($filter)) {
          } else if (0 !== sizeof($block->_defaultFilter)) {
            $filter = $block->_defaultFilter;
          }

          $params = array('rep_name', 'rep_commission_earned', 'rep_commission_status');

          foreach ($params as $param) {
            $column = $block->getColumn($param);

            if (isset($filter[$param]) && (!empty($filter[$param]) || strlen($filter[$param]) > 0) && $column->getFilter()) {
              $column->getFilter()->setValue($filter[$param]);

              $collection = $block->getCollection();

              if ($collection) {
                $field = ($column->getFilterIndex()) ? $column->getFilterIndex() : $column->getIndex();

                if ($column->getFilterConditionCallback()) {
                  call_user_func($column->getFilterConditionCallback(), $collection, $column);
                } else {
                  $cond = $column->getFilter()->getCondition();

                  if ($field && isset ($cond)) {
                    $collection->addFieldToFilter('salesrep.'. $param, $cond);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  public function beforeBlockToHtml(Varien_Event_Observer $observer) {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return $this;
    }

    $block = $observer->getBlock();

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/product_grid/view_commission_rate')) {
      if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Grid) {
        if (!Mage::app()->isSingleStoreMode()) {
          $after_column = 'websites';
        } else {
          $after_column = 'status';
        }

        $block->addColumnAfter(
          'salesrep',
          array(
            'header' => Mage::helper('salesrep')->__('Comm. %'),
            // 'index'  => 'firstname',
            'align' => 'center',
            'width' => '10px',
            'renderer' => 'LucidPath_SalesRepDeluxe_Block_Adminhtml_Catalog_Product_Grid_Renderer_Comm'
          ),
          $after_column
        );
      }
    }

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep')) {
      if ($block instanceof Mage_Adminhtml_Block_Customer_Grid) {
        $block->addColumnAfter(
          'rep_name',
          array(
            'header' => Mage::helper('salesrep')->__('Sales Rep.'),
            // 'index'  => 'firstname',
            'align' => 'center',
            'width' => '10px',
            'renderer' => 'LucidPath_SalesRepDeluxe_Block_Adminhtml_Customer_Grid_Renderer_Earner'
          ),
          'website_id'
        );
      }
    }

    return $this;
  }

  public function core_collection_abstract_load_before(Varien_Event_Observer $observer) {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return $this;
    }

    $collection = $observer->getCollection();
    if (!isset($collection)) return;

    if ($collection instanceof Mage_Sales_Model_Resource_Order_Grid_Collection) {
      if (($block = Mage::app()->getLayout()->getBlock('sales_order.grid')) != false) {
        $this->addColumnsToOrderGrid($block);
      }
    }

    return $this;
  }

  public function eav_collection_abstract_load_before(Varien_Event_Observer $observer) {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return $this;
    }

    $collection = $observer->getCollection();
    if (!isset ($collection)) return;

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep')) {
      if ($collection instanceof Mage_Customer_Model_Resource_Customer_Collection) {
        if (($block = Mage::app()->getLayout()->getBlock('customer.grid')) != false ||
            ($block = Mage::app()->getLayout()->getBlock('admin.customer.grid')) != false ||
            ($block = Mage::app()->getLayout()->getBlock('customer')) != false
        ) {
          $joined_tables = array_keys($collection->getSelect()->getPart('from'));

          if (!in_array('admin_user', $joined_tables)) {
            $collection->getSelect()->joinLeft(array('admin_user' => $collection->getTable('admin/user')), 'admin_user.user_id=e.salesrep_rep_id', array('firstname as rep_firstname', 'lastname as rep_lastname'));

            /*********/
            $is_admin           = Mage::getSingleton('admin/session')->isAllowed('system/config');
            $view_rep_name_all  = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep/all_customers');
            $view_rep_name_sub  = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep/customers_of_subordinate');
            $view_own_customers = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep/own_customers_only');

            if ($is_admin || $view_rep_name_all) {
            } else {
              $admin_user_ids = array(Mage::getSingleton('admin/session')->getUser()->getId());

              if ($view_rep_name_sub) {
                $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());
              }

              $collection->getSelect()->where('e.salesrep_rep_id IN('. implode(', ', $admin_user_ids) .')');
            }

            $filter = $block->getParam($block->getVarNameFilter(), null);

            if (is_string($filter)) {
              $filter = $block->helper('adminhtml')->prepareFilterString($filter);
            } else if ($filter && is_array ($filter)) {
            } else if (0 !== sizeof($block->_defaultFilter)) {
              $filter = $block->_defaultFilter;
            }

            $param = 'rep_name';

            $column = $block->getColumn($param);

            if (isset($filter[$param]) && (!empty($filter[$param]) || strlen($filter[$param]) > 0) && $column->getFilter()) {
              $column->getFilter()->setValue($filter[$param]);

              $cond = $column->getFilter()->getCondition();

              if (isset($cond)) {
                $collection->getSelect()->where('admin_user.firstname LIKE ? OR admin_user.lastname LIKE ?', $cond, $cond);
              }
            }
          }
        }
      }
    }

    return $this;
  }

  public function catalog_product_save_after(Varien_Event_Observer $observer) {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return $this;
    }

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/change_rep')) {
      $product = $observer->getEvent()->getProduct();
      $write   = Mage::getSingleton('core/resource')->getConnection('core_write');
      $table   = Mage::getSingleton('core/resource')->getTableName('catalog/product');

      if (Mage::app()->getFrontController()->getRequest()->getParam('salesrep_rep_commission_rate') == '') {
        $salesrep_rep_commission_rate = new Zend_Db_Expr('null');
      } else {
        $salesrep_rep_commission_rate = Mage::helper('salesrep')->getPercent(Mage::app()->getFrontController()->getRequest()->getParam('salesrep_rep_commission_rate'), true);
      }

      $write->query("UPDATE {$table} SET salesrep_rep_commission_rate = ". $salesrep_rep_commission_rate ." WHERE entity_id = ". $product->getId() .";");
    }

    return $this;
  }

  public function adminhtml_customer_prepare_save(Varien_Event_Observer $observer) {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return $this;
    }

    $customer = $observer->getEvent()->getCustomer();

    $is_admin       = Mage::getSingleton('admin/session')->isAllowed('system/config');
    $can_change_rep = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_management/change_rep');

    $rep_id = null;

    if ($customer->isObjectNew() && null === $customer->getCreatedAt()) {
      $rep_id = Mage::getSingleton('admin/session')->getUser()->getId();
    } else {
      if ($is_admin || $can_change_rep) {
        $rep_id = Mage::app()->getRequest()->getPost('salesrep_rep');
      }
    }

    Mage::getSingleton('core/session')->setSalesrepRepId($rep_id);

    return $this;
  }

  public function adminhtml_customer_save_after(Varien_Event_Observer $observer) {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return true;
    }

    $customer = $observer->getEvent()->getCustomer();

    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
    $table = Mage::getSingleton('core/resource')->getTableName('customer/entity');

    $write->query("UPDATE {$table} SET salesrep_rep_id = '". intval(Mage::getSingleton('core/session')->getSalesrepRepId()) ."' WHERE entity_id = ". $customer->getId() .";");

    Mage::getSingleton('core/session')->setSalesrepRepId('');

    return $this;
  }
}
?>

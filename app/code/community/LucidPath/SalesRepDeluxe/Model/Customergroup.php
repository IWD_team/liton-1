<?php
class LucidPath_SalesRepDeluxe_Model_Customergroup extends Mage_Core_Model_Abstract {

  public function _construct() {
    parent::_construct();
    $this->_init('salesrep/customergroup');
  }

}

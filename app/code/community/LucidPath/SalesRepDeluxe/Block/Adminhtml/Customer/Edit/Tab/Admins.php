<?php
class LucidPath_SalesRepDeluxe_Block_Adminhtml_Customer_Edit_Tab_Admins extends Mage_Adminhtml_Block_Template
                                                                  implements Mage_Adminhtml_Block_Widget_Tab_Interface {

  public function __construct() {
    parent::__construct();
    $this->setTemplate('salesrep/customer/edit/tab/admins.phtml');
  }


  public function getAdmins() {
    return LucidPath_SalesRepDeluxe_Model_Source_UsersList::toOptionArray();
  }

  /**
  * Prepare label for tab
  *
  * @return string
  */
  public function getTabLabel() {
    return $this->__('Sales Representative');
  }

  /**
  * Prepare title for tab
  *
  * @return string
  */
  public function getTabTitle() {
    return $this->__('Sales Representative');
  }

  /**
  * Returns status flag about this tab can be shown or not
  *
  * @return true
  */
  public function canShowTab() {
    if (Mage::registry('current_customer')->getId() && $this->hasPemissions()) {
      return true;
    }
    return false;
  }

  /**
  * Returns status flag about this tab hidden or not
  *
  * @return true
  */
  public function isHidden() {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return true;
    }

    if (!Mage::registry('current_customer')->getId() || !$this->hasPemissions()) {
      return true;
    }

    return false;
  }

  private function hasPemissions() {
    $is_admin       = Mage::getSingleton('admin/session')->isAllowed('system/config');
    $can_change_rep = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_management/change_rep');

    return $is_admin || $can_change_rep;
  }
}
?>

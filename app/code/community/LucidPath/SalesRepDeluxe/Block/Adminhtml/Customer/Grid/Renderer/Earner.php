<?php
class LucidPath_SalesRepDeluxe_Block_Adminhtml_Customer_Grid_Renderer_Earner extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

  public function render(Varien_Object $row) {
    if (!$row->getSalesrepRepId()) {
      return '';
    }

    $value = $row->getRepFirstname() .' '. $row->getRepLastname();

    /*********/
    $is_admin          = Mage::getSingleton('admin/session')->isAllowed('system/config');
    $view_rep_name_all = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep/all_customers');

    if ($is_admin || $view_rep_name_all) {
      return $value;
    }

    /*********/
    $view_rep_name_sub = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep/customers_of_subordinate');

    if ($view_rep_name_sub) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($row->getSalesrepRepId(), $subordinate_ids)) {
        return $value;
      }
    }

    /*********/
    $view_own_customers = Mage::getSingleton('admin/session')->isAllowed('salesrep/customer_grid/view_rep/own_customers_only');

    if ($view_own_customers) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $row->getSalesrepRepId()) {
        return $value;
      }
    }

    return '';
  }
}
?>

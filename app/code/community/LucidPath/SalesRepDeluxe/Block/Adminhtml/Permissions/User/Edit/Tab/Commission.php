<?php
class LucidPath_SalesRepDeluxe_Block_Adminhtml_Permissions_User_Edit_Tab_Commission
  extends Mage_Adminhtml_Block_Template
  implements Mage_Adminhtml_Block_Widget_Tab_Interface {

  public function __construct() {
    parent::__construct();
    $this->setTemplate('salesrep/permissions/user/edit/tab/commission.phtml');
  }

  public function getAdmins() {
    $current_user = Mage::registry('permissions_user');
    $collection   = Mage::getResourceModel('admin/user_collection')->setOrder('firstname', 'asc')->load();

    $result   = array();
    $result[] = array('value' => "", 'label' => "No Manager");

    foreach ($collection as $admin) {
      if ($current_user->getId() != $admin->getId()) {
        $result[] = array('value' => $admin->getId(), 'label' => $admin->getFirstname() .' '. $admin->getLastname() .' ('. $admin->getUsername() .')');
      }
    }

    return $result;
  }

  public function getRepCommissionRate() {
    $current_user = Mage::registry('permissions_user');

    if ($current_user->getSalesrepRepCommissionRate() > 0) {
      return $current_user->getSalesrepRepCommissionRate();
    }
    return '';

  }

  /**
  * Prepare label for tab
  *
  * @return string
  */
  public function getTabLabel() {
    return $this->__('Commission');
  }

  /**
  * Prepare title for tab
  *
  * @return string
  */
  public function getTabTitle() {
    return $this->__('Commission');
  }

  /**
  * Returns status flag about this tab can be shown or not
  *
  * @return true
  */
  public function canShowTab() {
    $is_admin                       = Mage::getSingleton('admin/session')->isAllowed('system/config');
    $edit_sales_rep_commission_rate = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_sales_rep_commission_rate');
    $edit_manager_commission_rate   = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_manager_commission_rate');
    $edit_assigned_manager          = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_assigned_manager');

    if ($is_admin || $edit_sales_rep_commission_rate || $edit_manager_commission_rate || $edit_assigned_manager) {
      return true;
    }
    return false;
  }

  /**
  * Returns status flag about this tab hidden or not
  *
  * @return true
  */
  public function isHidden() {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return true;
    }

    $is_admin                       = Mage::getSingleton('admin/session')->isAllowed('system/config');
    $edit_sales_rep_commission_rate = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_sales_rep_commission_rate');
    $edit_manager_commission_rate   = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_manager_commission_rate');
    $edit_assigned_manager          = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_assigned_manager');

    if ($is_admin || $edit_sales_rep_commission_rate || $edit_manager_commission_rate || $edit_assigned_manager) {
      return false;
    }

    return true;
  }

  public function getCustomerGroupCollection(){

    return Mage::getModel('customer/group')->getCollection();

  }

}

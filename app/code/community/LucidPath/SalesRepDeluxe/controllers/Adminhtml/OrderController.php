<?php
class LucidPath_SalesRepDeluxe_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action {

  public function changeSalesrepRepAction() {
    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/change_rep')) {
      $order_id = $this->getRequest()->getPost('order_id');
      $rep_id   = $this->getRequest()->getPost('salesrep_rep_id');

      $salesrep = Mage::helper('salesrep')->setCommissionEarned($order_id, $rep_id, null);

      $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('success' => 1, 'salesrep' => $this->getSalesrepOutput($salesrep))));
    }
  }

  public function changeSalesrepManagerAction() {
    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/change_manager')) {
      $order_id   = $this->getRequest()->getPost('order_id');
      $manager_id = $this->getRequest()->getPost('salesrep_manager_id');

      $salesrep = Mage::helper('salesrep')->setCommissionEarned($order_id, null, $manager_id);

      $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('success' => 1, 'salesrep' => $this->getSalesrepOutput($salesrep))));
    }
  }

  public function changeRepCommissionStatusAction() {
    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/change_rep_commission_status')) {
      foreach ((array)$this->getRequest()->getPost('order_id') as $order_id) {
        $salesrep = Mage::getModel('salesrep/salesrep')->loadByOrder($order_id);
        $salesrep->setRepCommissionStatus(strtolower($this->getRequest()->getPost('rep_commission_status')));
        $salesrep->save();

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('success' => 1, 'salesrep' => $this->getSalesrepOutput($salesrep))));
      }
    }
  }

  public function changeManagerCommissionStatusAction() {
    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/change_manager_commission_status')) {
      foreach ((array)$this->getRequest()->getPost('order_id') as $order_id) {
        $salesrep = Mage::getModel('salesrep/salesrep')->loadByOrder($order_id);
        $salesrep->setManagerCommissionStatus(strtolower($this->getRequest()->getPost('manager_commission_status')));
        $salesrep->save();

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('success' => 1, 'salesrep' => $this->getSalesrepOutput($salesrep))));
      }
    }
  }

  public function changeCommissionStatusAction() {
    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/change_rep_commission_status')) {
      foreach ((array)$this->getRequest()->getPost('order_id') as $order_id) {
        $salesrep = Mage::getModel('salesrep/salesrep')->loadByOrder($order_id);

        if ($rep_id = $this->getRequest()->getPost('rep_id')) {
          if ($salesrep->getRepId() == $rep_id) {
            $salesrep->setRepCommissionStatus(strtolower($this->getRequest()->getPost('rep_commission_status')));
          }

          if ($salesrep->getManagerId() == $rep_id) {
            $salesrep->setManagerCommissionStatus(strtolower($this->getRequest()->getPost('rep_commission_status')));
          }

          $salesrep->save();
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('success' => 1, 'salesrep' => $this->getSalesrepOutput($salesrep))));
      }
    }
  }

  private function getSalesrepOutput($salesrep) {
    $result = array();

    // name
    $show_rep_name = $show_manager_name = false;

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_name/all_orders')) {
      $show_rep_name = true;
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_name/orders_of_subordinate')) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($salesrep->getRepId(), $subordinate_ids)) {
        $show_rep_name = true;
      }
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_name/own_orders_only')) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $salesrep->getRepId()) {
        $show_rep_name = true;
      }
    }

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_name/all_orders')) {
      $show_manager_name = true;
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_name/orders_of_subordinate')) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($salesrep->getRepId(), $subordinate_ids)) {
        $show_manager_name = true;
      }
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_name/own_orders_only')) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $salesrep->getRepId()) {
        $show_manager_name = true;
      }
    }

    if ($show_rep_name) {
      $result['rep_id']   = $salesrep->getRepId();
      $result['rep_name'] = $salesrep->getRepName();
    }

    if ($show_manager_name) {
      $result['manager_id']   = $salesrep->getManagerId();
      $result['manager_name'] = $salesrep->getManagerName();
    }

    // commission amount
    $show_rep_commission = $show_manager_commission = false;

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_commission_amount/all_orders')) {
      $show_rep_commission = true;
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_commission_amount/orders_of_subordinate')) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($salesrep->getRepId(), $subordinate_ids)) {
        $show_rep_commission = true;
      }
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_commission_amount/own_orders_only')) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $salesrep->getRepId()) {
        $show_rep_commission = true;
      }
    }

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_commission_amount/all_orders')) {
      $show_manager_commission = true;
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_commission_amount/orders_of_subordinate')) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($salesrep->getRepId(), $subordinate_ids)) {
        $show_manager_commission = true;
      }
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_commission_amount/own_orders_only')) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $salesrep->getRepId()) {
        $show_manager_commission = true;
      }
    }

    if ($show_rep_commission) {
      $result['rep_id']                     = $salesrep->getRepId();
      $result['rep_commission_earned']      = $salesrep->getRepCommissionEarned();
      $result['rep_commission_earned_text'] = Mage::helper('core')->currency($salesrep->getRepCommissionEarned(), true, false);
    }

    if ($show_manager_commission) {
      $result['manager_id']                     = $salesrep->getManagerId();
      $result['manager_commission_earned']      = $salesrep->getManagerCommissionEarned();
      $result['manager_commission_earned_text'] = Mage::helper('core')->currency($salesrep->getManagerCommissionEarned(), true, false);
    }

    // status
    $show_rep_commission_status = $show_manager_name = false;

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_commission_status/all_orders')) {
      $show_rep_commission_status = true;
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_commission_status/orders_of_subordinate')) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($salesrep->getRepId(), $subordinate_ids)) {
        $show_rep_commission_status = true;
      }
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_rep_commission_status/own_orders_only')) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $salesrep->getRepId()) {
        $show_rep_commission_status = true;
      }
    }

    if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_commission_status/all_orders')) {
      $show_manager_commission_status = true;
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_commission_status/orders_of_subordinate')) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($salesrep->getRepId(), $subordinate_ids)) {
        $show_manager_commission_status = true;
      }
    } else if (Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/view_manager_commission_status/own_orders_only')) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $salesrep->getRepId()) {
        $show_manager_commission_status = true;
      }
    }

    if ($show_rep_commission_status) {
      $result['rep_commission_status'] = $salesrep->getRepCommissionStatus();
    }

    if ($show_manager_commission_status) {
      $result['manager_commission_status'] = $salesrep->getManagerCommissionStatus();
    }

    return $result;
  }

  protected function _isAllowed() {
    return true;
  }
}
?>

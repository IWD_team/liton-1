<?php
require_once 'Mage/Adminhtml/controllers/Permissions/UserController.php';

class LucidPath_SalesRepDeluxe_Adminhtml_Permissions_UserController extends Mage_Adminhtml_Permissions_UserController {

  public function saveAction() {
    if ($data = $this->getRequest()->getPost()) {
      $id = $this->getRequest()->getParam('user_id');
      $model = Mage::getModel('admin/user')->load($id);
      if (!$model->getId() && $id) {
        Mage::getSingleton('adminhtml/session')->addError($this->__('This user no longer exists.'));
        $this->_redirect('*/*/');
        return;
      }

      $is_admin                       = Mage::getSingleton('admin/session')->isAllowed('system/config');
      $edit_sales_rep_commission_rate = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_sales_rep_commission_rate');
      $edit_manager_commission_rate   = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_manager_commission_rate');
      $edit_assigned_manager          = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_assigned_manager');

      if ($is_admin || $edit_sales_rep_commission_rate) {
        if ($data['salesrep_rep_commission_rate'] == '') {
          $salesrep_rep_commission_rate = new Zend_Db_Expr('null');
        } else {
          $salesrep_rep_commission_rate = Mage::helper('salesrep')->getPercent($data['salesrep_rep_commission_rate'], true);
        }

        $data['salesrep_rep_commission_rate'] = $salesrep_rep_commission_rate;
      }

      if ($is_admin || $edit_manager_commission_rate) {
        if (($salesrep_manager_id = intval($data['salesrep_manager_id'])) == 0) {
          $salesrep_manager_id = new Zend_Db_Expr('null');
        }

        $data['salesrep_manager_id'] = $salesrep_manager_id;
      }

      if ($is_admin || $edit_assigned_manager) {
        if ($data['salesrep_manager_commission_rate'] == '') {
          $salesrep_manager_commission_rate = new Zend_Db_Expr('null');
        } else {
          $salesrep_manager_commission_rate = Mage::helper('salesrep')->getPercent($data['salesrep_manager_commission_rate'], true);
        }

        $data['salesrep_manager_commission_rate'] = $salesrep_manager_commission_rate;
      }

      $customerGroupCollection = Mage::getModel('customer/group')->getCollection();

      $customerGroupArray = array();

      foreach($customerGroupCollection as $customerGroup){
        $customerGroupArray[] = $customerGroup->getId();
      }

      $salesrepCustomerGroupModel = Mage::getModel('salesrep/customergroup');

      $currentSalesrepGroupCollection = $salesrepCustomerGroupModel
          ->getCollection()
          ->addFieldToFilter('admin_id', array('eq' => $id));

      if($currentSalesrepGroupCollection->getSize()) {
        foreach ($currentSalesrepGroupCollection as $group) {
          $group->delete();
        }
      }

      $postCommission = $data['salesrep_commission_customer_group'];
      foreach($customerGroupArray as $customerGroup){
        $salesrepCustomerGroupModel = Mage::getModel('salesrep/customergroup');

        $salesrepCustomerGroupModel
          ->setAdminId($id)
          ->setCustomerGroupId($customerGroup)
          ->setCommission($postCommission[$customerGroup])
          ->save();
      }

      $this->getRequest()->setPost($data);
    }

    parent::saveAction();
  }

  protected function _isAllowed() {
    return true;
  }
}

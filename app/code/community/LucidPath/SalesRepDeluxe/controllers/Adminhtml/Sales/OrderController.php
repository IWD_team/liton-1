<?php
include_once('Mage/Adminhtml/controllers/Sales/OrderController.php');

class LucidPath_SalesRepDeluxe_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController {

  public function viewAction() {
    $order_id        = $this->getRequest()->getParam('order_id');
    $salesrep        = Mage::getModel('salesrep/salesrep')->loadByOrder($order_id);

    $order_detail_page_all = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/access_order_detail_page/all_orders');
    $order_detail_page_sub = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/access_order_detail_page/orders_of_subordinate');
    $order_detail_page_own = Mage::getSingleton('admin/session')->isAllowed('salesrep/order_detail_page/access_order_detail_page/own_orders_only');

    $view = false;

    if ($order_detail_page_all) {
      $view = true;
    } else if ($order_detail_page_sub) {
      $subordinate_ids = Mage::helper('salesrep')->getSubordinateIds(Mage::getSingleton('admin/session')->getUser()->getId());

      if (in_array($salesrep->getRepId(), $subordinate_ids)) {
        $view = true;
      }
    } else if ($order_detail_page_own) {
      if (Mage::getSingleton('admin/session')->getUser()->getId() == $salesrep->getRepId()) {
        $view = true;
      }
    }

    if ($view) {
      parent::viewAction();
    } else {
      $this->_getSession()->addError($this->__('You don\'t have permissions to view this order.'));

      $this->_redirect('*/sales_order/');
    }
  }

  protected function _isAllowed() {
    return true;
  }
}
?>

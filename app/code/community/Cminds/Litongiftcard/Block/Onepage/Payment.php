<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Litongiftcard_Block_Onepage_Payment extends Cminds_GiftCard_Block_Onepage_Payment
{
    /**
     * Gets they payment methods
     * @return object $html
     */
    protected function _toHtml()
    {
        $this->setTemplate('cminds_litongiftcard/checkout/onepage/gc-payment.phtml');
        if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
    }

    public function getOrderExpectedOptions()
    {
        return Cminds_Litongiftcard_Model_Source_OrderExpected::toOptionArray();
    }
}

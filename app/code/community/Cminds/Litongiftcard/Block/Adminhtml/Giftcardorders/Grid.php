<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Litongiftcard_Block_Adminhtml_Giftcardorders_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Constructs the edit grid
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('giftcardGrid');
        $this->setDefaultSort('status');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
  /**
   * Prepares the data to be edited
   * Gets the Gift Card info from the order
   */
    protected function _prepareCollection()
    {
        $salesFlatOrderGridTable = Mage::getSingleton('core/resource')
            ->getTableName('sales/order_grid');
        $collection = Mage::getModel('giftcard/payment')->getCollection();
        $collection->getSelect()
        ->joinLeft(
            array('o' => $salesFlatOrderGridTable),
            'main_table.order_id = o.entity_id',
            array('*')
        )->where('main_table.order_id != ?', 0);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
  /**
   * Prepares the columns for editing
   */
    protected function _prepareColumns()
    {

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'index' => 'shipping_name',
        ));
        $this->addColumn('amount', array(
            'header' => Mage::helper('sales')->__('Credit Applied'),
            'index' => 'amount',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Sample Order Status'),
            'index' => 'status',
            'renderer' => 'Cminds_Litongiftcard_Block_Adminhtml_Giftcardorders_Renderer_SampleStatus'
        ));
        $this->addColumn('job_name', array(
            'header' => Mage::helper('sales')->__('Job Name'),
            'index' => 'job_name',
        ));
        $this->addColumn('sales_person_requesting', array(
            'header' => Mage::helper('sales')->__('Sales Person Requesting'),
            'index' => 'sales_person_requesting',
        ));
        $this->addColumn('order_expected', array(
            'header' => Mage::helper('sales')->__('Order Expected Date'),
            'index' => 'order_expected',
            'renderer' => 'Cminds_Litongiftcard_Block_Adminhtml_Giftcardorders_Renderer_OrderExpected'
        ));


        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn(
                'action',
                array(
                    'header'    => Mage::helper('sales')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getOrderId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('View'),
                            'url'     => array('base'=>'*/sales_order/view'),
                            'field'   => 'order_id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
                )
            );
        }


        $this->addExportType('*/*/exportCsv', Mage::helper('giftcard')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('giftcard')->__('XML'));

        return parent::_prepareColumns();
    }
    /**
     * Prepares the delete mass action to delete multiple cards at once
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('giftcard');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('giftcard')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('giftcard')->__('Are you sure?')
        ));
        return $this;
    }
  /**
   * Gets the grid URL for each row
   */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}

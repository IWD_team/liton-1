<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Litongiftcard_Block_Adminhtml_Giftcardorders extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_giftcardorders';
        $this->_blockGroup = 'cminds_litongiftcard';
        $this->_headerText = Mage::helper('cminds_litongiftcard')->__('Manage Giftcard Orders');

        parent::__construct();
        $this->removeButton('add');
    }
}

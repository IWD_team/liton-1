<?php

class Cminds_Litongiftcard_Model_Observer
{
    public function orderStatusChange(Varien_Event_Observer $observer)
    {
        $order = $observer->getOrder();
        if ($order->getState() == 'sample_ordered') {
            $statusHasChanged = false;
            $orderBefore = $order->getOrigData();
            $orderCurrent = $order->getData();

            if ($orderBefore['status'] != $orderCurrent['status']) {
                $statusHasChanged = true;
            }

            if ($statusHasChanged) {
                Mage::getModel('cminds_litongiftcard/history')->setStatusHasChanged($order);
            }
        }
    }

    public function saveOrderItemAttributes($observer)
    {
        $orderId    = $observer->getEvent()->getOrderId();
        $order      = Mage::getModel('sales/order')->load($orderId);
        $quoteId    = $observer->getEvent()->getQuoteId();
        $quote      = Mage::getModel('sales/quote')->load($quoteId);

        foreach ($quote->getAllItems() as $quoteItem) {
            if ($quoteItem->getIsGiftcard()) {
                if ($quoteItem->getGiftcardNum() && $quoteItem->getIsGcRefill()) {
                    /**
                     * item is a giftcard balance increase
                     */
                    $card = Mage::getModel('giftcard/giftcard')->getCollection()
                        ->addFieldToFilter('number', $quoteItem->getGiftcardNum())
                        ->getFirstItem();

                    $refillOrders = $card->getRefillOrderIds();
                    $refillArray = explode(',', $refillOrders);
                    $refillArray[] = $orderId;
                    $card->setRefillOrderIds(implode(',', $refillArray));

                    $card->setBal($card->getBal() + $quoteItem->getPrice());
                    $card->setUpdatedAt(now());
                    $card->save();

                    $payment = Mage::getModel('giftcard/payment');
                    $payment->setId(null);
                    $payment->setGiftcardId($card->getId());
                    $payment->setQuoteId($quote->getId());
                    $payment->setOrderId($order->getId());
                    $payment->setAmount($quoteItem->getPrice());
                    $payment->setCreatedAt(now());
                    $payment->setIsRefill(1);
                    $payment->save();
                } else {
                    /**
                     * new card, generate number/pin
                     */
                    $card = Mage::getModel('giftcard/giftcard');
                    $card->setId(null);
                    $card->setNumber(Mage::helper('giftcard')->generateCardNumber());
                    $quoteItem->setGiftcardNum($card->getNumber());
                    $quoteItem->save();
                    $card->setBal($quoteItem->getPrice());
                    $card->setOrderId($order->getId());
                    $card->setOrderAmount($quoteItem->getPrice());
                    $card->setCreatedAt(now());
                    $card->setUpdatedAt(now());
                    if ($quoteItem->getGiftcardEmail()) {
                        $card->setType(2);
                        $card->setToEmail($quoteItem->getGiftcardEmail());
                        $card->setToMsg($quoteItem->getGiftcardMsg());
                    } else {
                        $card->setShipped(0);
                        $card->setType(1);
                    }
                    $card->save();
                }
            }
        }
        /**
         * set order number on gift card payments
         */
        $giftcards = Mage::getModel('giftcard/payment')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId());
        foreach ($giftcards as $giftcard) {
            $giftcard->setOrderId($order->getId());
            $giftcard->save();

            $order->setState('sample_ordered');
            $order->setStatus('sampleorder_new');
            $order->save();
        }
    }
}
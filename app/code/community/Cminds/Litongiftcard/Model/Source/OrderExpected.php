<?php

class Cminds_Litongiftcard_Model_Source_OrderExpected
{
    public static function toOptionArray()
    {
        $opt = array();
        $opt[] = array('label' => '--Please select--', 'value' => '');
        $opt[] = array('label' => 'Immediate', 'value' => '1');
        $opt[] = array('label' => 'Weeks', 'value' => '2');
        $opt[] = array('label' => 'Months', 'value' => '3');
        $opt[] = array('label' => 'Can\'t Tell', 'value' => '4');
        return $opt;
    }
}
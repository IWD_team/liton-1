<?php

class Cminds_Litongiftcard_Model_History extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('cminds_litongiftcard/history');
    }

    /**
     * Method to set date when order status has been modified.
     *
     * @param $order
     * @return $this
     */
    public function setStatusHasChanged($order)
    {
        if ($order->getId()) {
            $date = date('Y-m-d h:m:s');

            $historyModel = $this->load($order->getId(), 'order_id');
            $giftcardPaymentModel = Mage::getModel('giftcard/payment')->load($order->getId(), 'order_id');
            if ($historyModel->getId()) {
                $historyModel
                    ->setLastChanged($date)
                    ->setExpectedId($giftcardPaymentModel->getOrderExpected());
            } else {
                $historyModel
                    ->setId(null)
                    ->setOrderId($order->getId())
                    ->setLastChanged($date)
                    ->setExpectedId($giftcardPaymentModel->getOrderExpected());

            }

            $historyModel->save();
        }

        return $this;
    }
}

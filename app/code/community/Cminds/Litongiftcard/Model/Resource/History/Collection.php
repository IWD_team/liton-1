<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Litongiftcard_Model_Resource_History_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('cminds_litongiftcard/history');
    }

    public function getOrdersToSendCron()
    {
        $collection = $this;
        $now = new DateTime();

        $orderIds = array();
        foreach ($collection as $order) {
            $date = $order->getLastChanged();
            $dateLastChanged = new DateTime($date);
            $expectedOrder = $order->getExpectedId();
            $orderId = $order->getOrderId();
            $interval = $now->diff($dateLastChanged)->days;

            if ($interval == 0) continue;

            if ($expectedOrder == 1 && $interval == 14) {
                $orderIds[] = $orderId;
            }
            if ($expectedOrder == 2 && $interval == 30) {
                $orderIds[] = $orderId;
            }
            if ($expectedOrder == 3 && $interval == 61) {
                $orderIds[] = $orderId;
            }
            if ($expectedOrder == 4 && $interval == 61) {
                $orderIds[] = $orderId;
            }
        }
        return $orderIds;
    }
}

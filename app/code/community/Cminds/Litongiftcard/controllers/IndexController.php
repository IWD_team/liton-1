<?php
require_once(Mage::getModuleDir('controllers','Cminds_GiftCard').DS.'IndexController.php');

class Cminds_Litongiftcard_IndexController extends Cminds_GiftCard_IndexController
{

    /**
     * Apply card to cart
     * @return object
     */
    public function applyCardAction()
    {
        $data = $this->getRequest()->getParams();
        
        $quote = $this->_getQuote();
        $amount = (float) $data['amount'];
        $_SESSION['cno'] = $data['cardNum'];
        
        
        $reloaded = Mage::getSingleton('core/session')->getGiftCardNumber();

        Mage::getSingleton('core/session')->unsetGiftCardNumber();

        if ($amount > $quote->getGrandTotal()) {
            $amount = $quote->getGrandTotal();
            $_SESSION['cn_amt'] = $amount;
        }
        if ($amount < 0.01) {
            echo "invalid amount";
            return true;
        }
        //check for valid card
        $giftcard = Mage::getModel('giftcard/giftcard')
            ->getGiftCardByNumber($data['cardNum']);

        if (!$giftcard->orderIsPayed($giftcard->getNumber())) {
            echo "Giftcard not Payed";
            return true;
        }


        if (!$giftcard->checkRefillPayments()) {
            echo "Refill not Payed";
            return true;
        }


        if (!$giftcard->getId()) {
            echo "Invalid Card";
            return true;
        }
        if ($giftcard->getBal() < $amount) {
            echo "Card Balance Too Low.";
            return true;
        }

        //check to see if this card is the same as the reloaded card
        if ($reloaded == $data['cardNum']) {
            $reloaded = '';
            echo "Card Type";
            return true;
        }

        //check to see if this card is already applied
        $cardPay = Mage::getModel('giftcard/payment')->getCollection()
                ->addFieldToFilter('quote_id', $quote->getId())
                ->addFieldToFilter('giftcard_id', $giftcard->getId())
                ->getFirstItem();
        if ($cardPay->getId()) {
            echo "This card is already applied to your order";
            return true;
        }

        //apply new card
        //echo ($giftcard->getBal() - $amount);die;
        $giftcard->setBal($giftcard->getBal() - $amount);
        $giftcard->save();
        $newCard = Mage::getModel('giftcard/payment');
        $newCard->setJobName($data['gcjob']);
        $newCard->setSalesPersonRequesting($data['gc_sales_person_requesting']);
        $newCard->setOrderExpected($data['gc_when_order_expected']);
        $newCard->setGiftcardId($giftcard->getId());
        $newCard->setQuoteId($quote->getId());
        $newCard->setAmount($amount);
        $newCard->setCreatedAt(now());
        $newCard->save();
        $quote->collectTotals();
        $quote->save();

        echo $this->getAjaxHtml();
    }

}

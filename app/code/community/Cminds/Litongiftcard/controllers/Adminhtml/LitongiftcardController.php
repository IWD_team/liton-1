<?php

/**
 * Class Cminds_Litongiftcard_Adminhtml_LitongiftcardController
 */
class Cminds_Litongiftcard_Adminhtml_LitongiftcardController extends Mage_Adminhtml_Controller_action
{
    /**
     * Loads the layout
     */
    public function indexAction()
    {
        $this->_title($this->__('Manage Giftcard Orders'));
        $this->loadLayout();
        $this->_setActiveMenu('sales/cminds_litongiftcard');
        $this->_addContent($this->getLayout()->createBlock('cminds_litongiftcard/adminhtml_giftcardorders'));
        $this->renderLayout();
    }
	
    protected function _isAllowed()
    {
        return true;
    }
}
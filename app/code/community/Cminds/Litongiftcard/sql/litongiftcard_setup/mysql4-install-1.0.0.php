<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('giftcard/payment'),
        'job_name',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 225,
            'nullable' => true,
            'comment' => 'Job Name'
        )
    );
$installer->getConnection()
    ->addColumn(
        $installer->getTable('giftcard/payment'),
        'sales_person_requesting',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 225,
            'nullable' => true,
            'comment' => 'Sales person requesting'
        )
    );
$installer->getConnection()
    ->addColumn(
        $installer->getTable('giftcard/payment'),
        'order_expected',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'length' => 1,
            'nullable' => true,
            'default' => null,
            'comment' => 'When order is expected?'
        )
    );


$installer->endSetup();

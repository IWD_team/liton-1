<?php
$installer = $this;

$statusTable = $installer->getTable('sales/order_status');
$statusStateTable = $installer->getTable('sales/order_status_state');

$installer->getConnection()->insertArray(
    $statusTable,
    array(
        'status',
        'label'
    ),
    array(
        array(
            'status' => 'sampleorder_new',
            'label' => Mage::helper('cminds_litongiftcard')->__('Sample Order')
        ),
        array(
            'status' => 'sampleorder_order_received',
            'label' => Mage::helper('cminds_litongiftcard')->__('Order Received')
        ),
        array(
            'status' => 'sampleorder_pending_order',
            'label' => Mage::helper('cminds_litongiftcard')->__('Pending Order')
        ),
        array(
            'status' => 'sampleorder_not_accepted',
            'label' => Mage::helper('cminds_litongiftcard')->__('Not Accepted')
        )
    )
);

$installer->getConnection()->insertArray(
    $statusStateTable,
    array(
        'status',
        'state',
        'is_default'
    ),
    array(
        array(
            'status' => 'sampleorder_new',
            'state' => 'sample_ordered',
            'is_default' => 1
        ),
        array(
            'status' => 'sampleorder_order_received',
            'state' => 'sample_ordered',
            'is_default' => 0
        ),
        array(
            'status' => 'sampleorder_pending_order',
            'state' => 'sample_ordered',
            'is_default' => 0
        ),
        array(
            'status' => 'sampleorder_not_accepted',
            'state' => 'sample_ordered',
            'is_default' => 0
        )
    )
);
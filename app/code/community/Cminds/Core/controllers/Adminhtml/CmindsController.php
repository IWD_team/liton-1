<?php
class Cminds_Core_Adminhtml_CmindsController extends Mage_Adminhtml_Controller_Action {
	public function deactivateLicenseAction() {
		$id = $this->getRequest()->getParam('id', null);

		$id = str_replace('_is_approved', '', $id);
		$id = str_replace('row_cmindsConf_', '', $id);

		if($id) {
			Mage::getModel('cminds/deactivate')->run($id);
			$jsonData = json_encode(array('success' => true));
			$this->getResponse()->setHeader('Content-type', 'application/json');
			$this->getResponse()->setBody($jsonData);
		} else {
			$jsonData = json_encode(array('success' => false));
			$this->getResponse()->setHeader('Content-type', 'application/json');
			$this->getResponse()->setBody($jsonData);
		}
	}

	protected function _isAllowed()
	{
		return true;
	}
}
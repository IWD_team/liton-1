<?php

class Cminds_Positions_Model_Mysql4_Report_Customer_Totals_Collection extends Mage_Reports_Model_Resource_Customer_Totals_Collection
{
    /**
     * Add Order count field to select
     *
     * @return Mage_Reports_Model_Resource_Order_Collection
     */
    public function addOrdersCount()
    {
        $this->addFieldToFilter('state', array('neq' => Mage_Sales_Model_Order::STATE_CANCELED));
        $this->getSelect()
            ->columns(array('orders_count' => 'COUNT(main_table.entity_id)'));

        $customersIds = Mage::helper('cminds_positions')->getReportCustomersBySalesrep();
        if(empty($customersIds)) {
            $customersIds[] = 0;
        }
        $this->getSelect()
            ->where('customer_id IN ('. implode(', ',$customersIds).')');

        $this->getSelect()
            ->columns(array('orders_sum_invoiced_amount' => 'SUM(main_table.base_total_invoiced)'));
        return $this;
    }
	

    /**
     * Set store filter collection
     *
     * @param array $storeIds
     * @return Mage_Reports_Model_Resource_Customer_Totals_Collection
     */
    public function setStoreIds($storeIds)
    {
        if ($storeIds) {
			$storeIds[] = 0;
            $this->addAttributeToFilter('store_id', array('in' => (array)$storeIds));
            $this->addSumAvgTotals(1)
                ->orderByTotalAmount();
        } else {
            $this->addSumAvgTotals()
                ->orderByTotalAmount();
        }

        return $this;
    }

}
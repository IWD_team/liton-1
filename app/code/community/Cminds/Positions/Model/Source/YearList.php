<?php
class Cminds_Positions_Model_Source_YearList {

    public static function toOptionArray() {

        $startYear = 2014;

        $endYear = date('Y');

        $result = array();
        $result[] = array('value' => 0, 'label' => 'None');
        for($i = $startYear; $i<=intval($endYear); $i++){
            $result[] = array('value' => $i, 'label' => $i);
        }

        return $result;
    }
}

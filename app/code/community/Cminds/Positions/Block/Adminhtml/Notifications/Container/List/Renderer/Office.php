<?php

class Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Office
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $officeName = '';

        if($row->getSalesrepRepId()) {
            $salesrepId = $row->getSalesrepRepId();
            $salesrep = Mage::getModel('admin/user')->load($salesrepId);

            if ($salesrep->getId()) {
                $positionId = $salesrep->getSalesrepPositionId();

                $position = Mage::getModel('cminds_positions/position')->load($positionId);

                if($position->getId()){
                    $officeName = $position->getName();
                }
            }
        }
        return $officeName;
    }
}
<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_Offices extends Mage_Core_Block_Template
{

    public function __construct()
    {
        $this->setTemplate('cminds_positions/sales/grid/offices.phtml');
        parent::__construct();
    }

    public function getMonthsValues()
    {
        $firstYear = 2012;
        $currentYear = date('Y');
        $currentDate = date('n-Y');
        $months = array();

        $month = array(
            1 => $this->__('January'),
            2 => $this->__('Ferubary'),
            3 => $this->__('March'),
            4 => $this->__('April'),
            5 => $this->__('May'),
            6 => $this->__('June'),
            7 => $this->__('July'),
            8 => $this->__('August'),
            9 => $this->__('September'),
            10 => $this->__('October'),
            11 => $this->__('November'),
            12 => $this->__('December'),
        );
        $k = 0;
        for ($firstYear; $firstYear <= $currentYear; $firstYear++) {
            for ($i = 1; $i <= 12; $i++) {
                $date = $i . '-' . $firstYear;
                if ((date('n') >= $i && $firstYear == date('Y')) || $firstYear < date('Y')) {
                    $months[$k]['value'] = '01-' . $i . '-' . $firstYear;
                    $months[$k]['label']= $month[$i] . ' ' . $firstYear;
                    $k++;
                } else {
                    break;
                }
            }
        }

        return array_reverse($months);
    }

    public function getChildren($adminsPosition){
        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('parent_id', array('eq' => $adminsPosition));

        $adminDataArray = array();

        $hasChildren = true;
        $childrenArray = array();

        if(!$positionsCollection->getSize()){
            $hasChildren = false;
        } else {
            foreach($positionsCollection as $child){
                $childrenArray[] = $child->getId();
            }
        }

        $adminDataArray['children'] = $childrenArray;

        $adminDataArray['has_children'] = $hasChildren;

        $adminDataArray['children_position_type_id'] = $positionsCollection->getFirstItem()->getTypeId();

        if($hasChildren) {
            $positionsCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id',
                    array('in' => $adminDataArray['children']));
        }
        $isNational = false;

        if($positionsCollection->getSize()){
            $isNational = true;
        }

        $adminDataArray['is_national'] = $isNational;


        return $adminDataArray;
    }

    public function getOptionPositions($positionData){

        $result = array();
        if($positionData['has_children']){
            $filteredPositionTypes = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('id', array('in' => $positionData['children']));
            $filteredPositionTypes->getSelect()
                ->order('name DESC');
            foreach($filteredPositionTypes as $position){
                $result[] = array('value' => $position->getId(), 'label' => $position->getName());
            }
        } else {
            $result[] = array('value' => '', 'label' => $this->__('You have no available values.'));
        }

        return $result;
    }

    public function getPositionTypeData($id){
        return Mage::getModel('cminds_positions/positiontype')->load($id);
    }

    public function getCurrentUser(){
        $currentAdmin = Mage::getSingleton('admin/session')->getUser();

        return $currentAdmin;
    }

    public function getPositionData(){
        $adminUserPositionId = $this->getCurrentUser()->getSalesrepPositionId();
        $positionData = $this->getChildren($adminUserPositionId);

        return $positionData;
    }
}
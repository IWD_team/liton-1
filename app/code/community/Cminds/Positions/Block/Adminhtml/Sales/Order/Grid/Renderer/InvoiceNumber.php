<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_InvoiceNumber extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $invoiceNumber = '';
        $invoiceModel = Mage::getModel('sales/order')->load($row->getId());
        if($invoiceModel->getId()){
            $invoiceNumber = $invoiceModel->getPoNumber();
        }
        return $invoiceNumber;
    }
}
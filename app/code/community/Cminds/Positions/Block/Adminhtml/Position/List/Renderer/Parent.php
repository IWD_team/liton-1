<?php

class Cminds_Positions_Block_Adminhtml_Position_List_Renderer_Parent
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $parentId = $row->getData('parent_id');

        if($parentId == 0) {
            return Mage::helper('cminds_positions')->__("No Parent Position");
        } else {
            $parent = Mage::getModel('cminds_positions/position')->load($parentId);

            if($parent->getId()) {
                return $parent->getName();
            }
        }
    }
}
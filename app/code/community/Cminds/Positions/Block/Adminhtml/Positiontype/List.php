<?php
class Cminds_Positions_Block_Adminhtml_Positiontype_List extends Mage_Adminhtml_Block_Widget_Grid_Container {

  public function __construct() {
    $this->_controller = 'adminhtml_positiontype_list';
    $this->_blockGroup = 'cminds_positions';
    $this->_headerText = Mage::helper('cminds_positions')->__('Position Types');
    parent::__construct();
  }
}

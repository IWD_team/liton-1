<?php
class Cminds_Positions_Block_Adminhtml_Positiontype_List_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('salesrep_positiontype_list_grid');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('cminds_positions/positiontype')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('cminds_positions')->__('ID'),
            'width'     => '50px',
            'index'     => 'id',
            'type'  => 'number',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('cminds_positions')->__('Name'),
            'index'     => 'name',
            'filter_condition_callback' => array($this, 'filterConditionCallback')
        ));
        $this->addColumn('parent_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Parent Position Name'),
            'index'     => 'parent_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Positiontype_List_Renderer_Parent',
            'filter_condition_callback' => array($this, 'filterConditionCallback')
        ));

        $yesnoOptions = array('0' => 'No','1' => 'Yes');

        $this->addColumn('can_change_goals', array(
            'header'    => Mage::helper('cminds_positions')->__('Can Change Goals'),
            'index'     => 'can_change_goals',
            'type'      => 'options',
            'options'   => $yesnoOptions,
            'filter_condition_callback' => array($this, 'filterConditionCallback')
        ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('cminds_positions')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('cminds_positions')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            ));
        return parent::_prepareColumns();
    }

    protected function filterConditionCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            if($column->getId() != 'can_change_goals') {
                return $this;
            }
        }
        if (empty($value) && $column->getId() != 'can_change_goals') {
                return $this;
        } else {
            if($column->getId() == 'name') {
                $filteredCollection = Mage::getModel('cminds_positions/positiontype')
                    ->getCollection()
                    ->addFieldToFilter(
                        'name', array('like' => '%' . $value . '%')
                    );

                $nameIdsArray = array();
                foreach($filteredCollection as $position){
                    $nameIdsArray[] = $position->getId();
                }
                $collection
                    ->addFieldToFilter(
                        'id', array('in' => $nameIdsArray)
                    );
            }

            if($column->getId() == 'parent_id') {
                $filteredCollection = Mage::getModel('cminds_positions/positiontype')
                    ->getCollection()
                    ->addFieldToFilter(
                        'name', array('like' => '%' . $value . '%')
                    );

                $parentIdsArray = array();
                foreach($filteredCollection as $position){
                    $parentIdsArray[] = $position->getId();
                }
                $collection
                    ->addFieldToFilter(
                        'parent_id', array('in' => $parentIdsArray)
                    );
            }

            if($column->getId() == 'can_change_goals') {
                    $collection
                        ->addFieldToFilter(
                            'can_change_goals', array('eq' => $value)
                        );
            }
        }
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/indexgrid');
    }
}

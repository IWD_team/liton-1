<?php

class Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Target
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return Mage::helper('core')->currency($row->getData('period_target'), true, false);
    }
}
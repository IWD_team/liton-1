<?php
class Cminds_Positions_Block_Adminhtml_Goals_Container_List extends Mage_Adminhtml_Block_Widget_Grid_Container {

  public function __construct() {
    $this->_controller = 'adminhtml_goals_container_list';
    $this->_blockGroup = 'cminds_positions';
    $this->_headerText = Mage::helper('cminds_positions')->__('Settings - Goals');
    parent::__construct();
    $this->removeButton('add');
  }
}

<?php
class Cminds_Positions_Block_Adminhtml_Report_Customer_Totals_Renderer_YearlyGoal extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row)
    {
        $customerId = $row->getCustomerId();
        $currentYear = date('Y');
        $goalsCollection = Mage::getModel('cminds_positions/customergoal')->getCollection();

        $goalsCollection
            ->addFieldToFilter('customer_id', array('eq' => $customerId))
            ->addFieldToFilter('period_type', array('eq' => 2))
            ->addFieldToFilter('period_year', array('eq' => $currentYear));

        $yearlyGoal = 0;
        if($goal = $goalsCollection->getFirstItem()->getData()){
            $yearlyGoal = $goal['period_target'];
        }

        return Mage::helper('core')->currency($yearlyGoal, true, false);
    }
}
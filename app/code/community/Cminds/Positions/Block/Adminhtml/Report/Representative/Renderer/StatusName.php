<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_StatusName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
	    $order = Mage::getModel('sales/order')->load($row->getOrderId());

	           $allItems = $order->getAllVisibleItems();

	           $isFullyShipped = true;
	           foreach($allItems as $item){
	               if($item->getQtyOrdered() != $item->getQtyShipped()){
	                   $isFullyShipped = false;
	               }
	           }

	           if($isFullyShipped){
	               $status = 'Shipped';
	           } else {
	               $status = 'Partially Shipped';
	           }

	           if(!$order->hasShipments()){
	               $status = 'Not Shipped';
	           }

	           return $status;
    }
}

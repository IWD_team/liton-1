<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_StatusComm extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {

        $status = $row->getData($this->getColumn()->getIndex());

        if (strtolower($status) == 'unpaid') {
            $name = '<span style="color:red;text-transform:uppercase;">UNPAID</span>';
        }
        if (strtolower($status) == 'paid') {
            $name = '<span style="color:green;text-transform:uppercase; font-weight:bold;">PAID</span>';
        }
        if (strtolower($status) == 'ineligible') {
            $name = '<span style="color:magenta;text-transform:uppercase;">INELIGIBLE</span>';
        }
        if (strtolower($status) == 'canceled') {
            $name = '<span style="color:grey;text-transform:uppercase;">CANCELED</span>';
        }
        return '<span>' . $name . '</span>';
    }
}

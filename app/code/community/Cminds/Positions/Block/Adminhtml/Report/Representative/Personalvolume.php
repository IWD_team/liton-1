<?php
class Cminds_Positions_Block_Adminhtml_Report_Representative_Personalvolume extends Cminds_Positions_Block_Adminhtml_Report_Representative_Abstract {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('date_placed');
        $this->setId('sales_region_orders_'. rand(0,100));
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $filterFrom = $this->getFilter('from');
        $filterTo = $this->getFilter('to');

        $customFilters = $this->getFilters();

        $repIds = implode(",", $this->getMatchingRepIds());

        $query = "SELECT `main_table`.*, `order`.`created_at` AS `date_placed`, CONCAT(order.customer_firstname, ' ', order.customer_lastname) AS `customer_name`,
               `order`.`status`, `order`.`increment_id`, `order`.`subtotal`, `order`.`base_currency_code` AS `currency`, `order`.`grand_total` AS `sale_amount` FROM `salesrep` AS `main_table`
               LEFT JOIN `sales_flat_order` AS `order` ON order.entity_id = main_table.order_id
               WHERE (main_table.rep_commission_status != 'ineligible') AND (`main_table`.`rep_id` IN(".$repIds."))";

        if(isset($customFilters['filter_from']) && isset($customFilters['filter_to'])){

            $fromTimestamp = strtotime($customFilters['filter_from']);
            $toTimestamp = strtotime($customFilters['filter_to']);

            $customFilterFrom = date('d/m/Y', $fromTimestamp);
            $customFilterTo = date('d/m/Y', $toTimestamp);

            $dateFrom = new Zend_Date($customFilterFrom, 'd/m/Y');
            $dateTo = new Zend_Date($customFilterTo, 'd/m/Y');

            $query .= " AND (order.created_at >= '".$dateFrom->toString('Y-m-d 00:00:00')."') AND (order.created_at <= '".$dateTo->toString('Y-m-d 23:59:59')."')";
        } elseif($filterFrom && $filterTo) {
            $dateFrom = new Zend_Date($filterFrom, 'd/m/Y');
            $dateTo = new Zend_Date($filterTo, 'd/m/Y');

            $query .= " AND (order.created_at >= '".$dateFrom->toString('Y-m-d 00:00:00')."') AND (order.created_at <= '".$dateTo->toString('Y-m-d 23:59:59')."')";
        }

        $query .= " ORDER BY `order_id` DESC LIMIT 10";

        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');

        $queryData = $readConnection->fetchAll($query);

        $collection = new Varien_Data_Collection();

        foreach($queryData as $row){
            $item = new Varien_Object();

            $item->addData($row);
            $collection->addItem($item);
        }
        $this->setCollection($collection);
    }
    protected function _prepareColumns() {
        $baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('salesrep')->__('Order #'),
            'width'     => '5%',
            'index'     => 'increment_id',
            'type'  => 'number',
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_OrderLink',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('salesrep')->__('Date Placed'),
            'index'     => 'date_placed',
            'width'     =>  '30%',
            'sortable'  =>  true,
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_DatePlaced',
        ));
        $this->addColumn('customer_name', array(
            'header'    => Mage::helper('salesrep')->__('Customer Name'),
            'index'     => 'increment_id',
            'width'     =>  '40%',
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_Customer',
        ));
        $this->addColumn('Status', array(
            'header'    => Mage::helper('salesrep')->__('Status'),
            'index'     => 'status',
            'column_css_class'     => 'shipping-status',
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_StatusName',
        ));
        $this->addColumn('subtotal', array(
            'header'    => Mage::helper('salesrep')->__('Sale Amount'),
            'index'     => 'subtotal',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
        ));
        $this->addColumn('action',
            array(
                'header'    => Mage::helper('catalog')->__('Action'),
                'width'     => '100px',
                'type'      => 'action',
                'getter'     => 'getOrderId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('catalog')->__('View Order'),
                        'url'     => array(
                            'base'=>'adminhtml/sales_order/view',
                            'params'=>array('store'=>$this->getRequest()->getParam('store'))
                        ),
                        'field'   => 'order_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            ));
        return parent::_prepareColumns();
    }

    public function getMatchingRepIds(){
        $adminUserCollection = Mage::getModel('admin/user')
            ->getCollection()
            ->addFieldToFilter('salesrep_position_id', array('eq' => $this->getData('position_id')));

        $salesrepArray = array();
        foreach($adminUserCollection as $user){
            $salesrepArray[] = $user->getId();
        }

        return $salesrepArray;
    }

    public function getOrderId(){

    }
}

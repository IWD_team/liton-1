<?php
class Cminds_Positions_Block_Adminhtml_Report_Product_Sold_Renderer_Category extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row)
    {
        $productId = $row->getData('product_id');

        $product = Mage::getModel('catalog/product')->load($productId);

        $categoriesIds = $product->getCategoryIds();

        $categoriesString = '';

        foreach($categoriesIds as $categoryId){
            $category = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($categoryId);
            $categoriesString .= $category->getName() . ', ';
        }

        return $categoriesString;
    }
}
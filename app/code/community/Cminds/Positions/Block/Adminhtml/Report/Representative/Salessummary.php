<?php
class Cminds_Positions_Block_Adminhtml_Report_Representative_Salessummary extends Cminds_Positions_Block_Adminhtml_Report_Representative_Abstract {

    private $_filters = array();

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('salesrep_salessummary_grid');
        $this->setDefaultDir('asc');
        $this->setFilters();
        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = new Varien_Data_Collection();

        $adminRegions = $this->getRegions();

        foreach($adminRegions as $region) {
            $regionSalesreps = $this->getRepIdsByRegion($region);

            $dates = $this->getPeriodArray();
            $datesCount = count($dates);
            $startDate = $dates[0]['start_date'];
            $endDate = $dates[$datesCount - 1]['end_date'];


            if ($startDate && $endDate) {
                $dateFrom = new Zend_Date($startDate, 'd-m-Y');

                $dateTo = new Zend_Date($endDate, 'd-m-Y');
            }
            $explodedRegionSalesreps = implode(",", $regionSalesreps);
			$explodedRegionSalesreps = '\''.implode("','", $regionSalesreps).'\'';
            $year = date('Y');

            $resource = Mage::getSingleton('core/resource');

            $readConnection = $resource->getConnection('core_read');

            $query = "SELECT `main_table`.*, `salesrep`.`rep_id` FROM `sales_flat_order` AS `main_table`
                     LEFT JOIN `salesrep` ON salesrep.order_id = main_table.entity_id
                     WHERE (`created_at` >= '".$dateFrom->toString('Y-m-d 00:00:00')."'
                     AND `created_at` <= '".$dateTo->toString('Y-m-d 23:59:59')."')
                     AND (main_table.state != 'canceled') AND (`rep_id` IN(".$explodedRegionSalesreps."))";

            $commissions = $readConnection->fetchAll($query);

            $periodPV = 0;

            foreach ($commissions AS $c) {
                $periodPV += $c['subtotal'];
            }


            $commissions = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToFilter('created_at', array(
                    'from' => date('Y-01-01 00:00:00'),
                    'to' => date('Y-m-d H:i:s')
                ));
            $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                array('rep_id'));
            $commissions->getSelect()->where('main_table.state != ?',
                'canceled');
            $commissions->addFieldToFilter('rep_id', array('in' => $regionSalesreps));

            $resource = Mage::getSingleton('core/resource');

            $readConnection = $resource->getConnection('core_read');

            $query = "SELECT `main_table`.*, `salesrep`.`rep_id` FROM `sales_flat_order` AS `main_table`
                     LEFT JOIN `salesrep` ON salesrep.order_id = main_table.entity_id
                     WHERE (`created_at` >= '".date('Y-01-01 00:00:00')."' AND `created_at` <= '".date('Y-m-d H:i:s')."')
                     AND (main_table.state != 'canceled') AND (`rep_id` IN(".$explodedRegionSalesreps."))";

            $commissions = $readConnection->fetchAll($query);

            $ytdPV = 0;

            foreach ($commissions AS $c) {
                $ytdPV += $c['subtotal'];
            }

            $commissions = Mage::getModel('sales/order')->getCollection();
            $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                array('rep_id'));
            $commissions->getSelect()->where('main_table.state != ?',
                'canceled');
            $commissions->addFieldToFilter('rep_id', array('in' => $regionSalesreps));

            $resource = Mage::getSingleton('core/resource');

            $readConnection = $resource->getConnection('core_read');

            $query = "SELECT `main_table`.*, `salesrep`.`rep_id` FROM `sales_flat_order` AS `main_table`
                   LEFT JOIN `salesrep` ON salesrep.order_id = main_table.entity_id WHERE (main_table.state != 'canceled') AND (`rep_id` IN(".$explodedRegionSalesreps."))";

            $commissions = $readConnection->fetchAll($query);

            $lifetimePV = 0;

            foreach ($commissions AS $c) {
                $lifetimePV += $c['subtotal'];
            }

            $position = Mage::getModel('cminds_positions/position')
                ->load($region);

            $goal = $this->getGoal($region, $year, $ytdPV);

            $s = new Varien_Object();
            $s->setTitle($position->getName());
            $s->setPeriod($periodPV);
            $s->setYtd($ytdPV);
            $s->setLifetime($lifetimePV);
            $s->setGoal($goal);
            $collection->addItem($s);
        }

        $this->setCollection($collection);
    }

    protected function _prepareColumns()
    {
      $baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
        $this->addColumn('title', array(
            'header'    => Mage::helper('salesrep')->__('SALES SUMMARY'),
            'width'     => '20%',
            'column_css_class' => 'main_header',
            'index'     => 'title',
        ));
        $this->addColumn('ytd', array(
            'header'    => Mage::helper('salesrep')->__('YTD'),
            'index'     => 'ytd',
            'class'     => 'ytd-sales',
            'width'     => '20%',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_SummaryYtd'
        ));
//        $this->addColumn('lifetime', array(
//            'header'    => Mage::helper('salesrep')->__('Lifetime'),
//            'index'     => 'lifetime',
//            'class'     => 'lifetime-sales',
//            'width'     => '20%',
//            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_SummaryLifetime'
//
//        ));
        $this->addColumn('period', array(
            'header'    => Mage::helper('salesrep')->__('Period'),
            'index'     => 'period',
            'class'     => 'period-sales',
            'width'     => '20%',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_SummaryPeriod'
        ));
        $this->addColumn('goal', array(
            'header'    => Mage::helper('salesrep')->__('Goal'),
            'index'     => 'goal',
            'width'     => '20%',
        ));
        return parent::_prepareColumns();
    }

    public function isTopLevel(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());

        if($positionModel->getId()){
            if($positionModel->getParentId() == 0 || $positionModel->getParentId() == null){
                return true;
            }
        }
        return false;
    }

    public function isRegional(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());


        if($positionModel->getId()){
            $childrenCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $currenUser->getSalesrepPositionId()));
            if($positionModel->getParentId() != 0 && $positionModel->getParentId() != null && $childrenCollection->getSize()){
                return true;
            }
        }
        return false;
    }

    public function getRegions(){

        $currentUser = Mage::getSingleton('admin/session')->getUser();

        $filters = $this->getFilters();

        $regionsArray = array();
        if($this->isTopLevel()) {
            $positionCollection = Mage::getModel('cminds_positions/position')
                ->getCollection();

            $positionCollection->getSelect()
                ->order('name DESC');
            if(isset($filters['region_id']) && isset($filters['office_data'])) {
                if ($filters['region_id'] != 0) {
                    $positionCollection
                        ->addFieldToFilter(
                            'parent_id',
                            $filters['region_id']
                        );
                }

                if ($filters['office_data'] != 0) {
                    $positionCollection
                        ->addFieldToFilter(
                            'id',
                            $filters['office_data']
                        );
                }
            } else {
                $positionCollection
                    ->addFieldToFilter('parent_id',
                        $currentUser->getSalesrepPositionId());
            }
            foreach ($positionCollection as $position) {
                $regionsArray[] = $position->getId();
            }
        } elseif($this->isRegional()){
            $positionCollection = Mage::getModel('cminds_positions/position')
                ->getCollection();

            $positionCollection->getSelect()
                ->order('name DESC');
            if(isset($filters['office_data']) && $filters['office_data'] != 0) {
                $positionCollection
                    ->addFieldToFilter('id',
                        $filters['office_data']);
            } else {
                $positionCollection
                    ->addFieldToFilter('parent_id',
                        $currentUser->getSalesrepPositionId());
            }
            foreach ($positionCollection as $position) {
                $regionsArray[] = $position->getId();
            }

        } else {
            $regionsArray[] = $currentUser->getSalesrepPositionId();
        }
        return $regionsArray;
    }

    public function getRepIdsByRegion($regionId){
        $filters = $this->getFilters();
        $positionsUnderRegion = array();
        if($this->isTopLevel()) {
            if(!isset($filters['region_id'])) {
                $positionCollection = Mage::getModel('cminds_positions/position')
                    ->getCollection()
                    ->addFieldToFilter('parent_id', $regionId);


                foreach ($positionCollection as $position) {
                    $positionsUnderRegion[] = $position->getId();
                }
            } else {
                $positionsUnderRegion[] = $regionId;
            }
        } elseif($this->isRegional()){
            $positionsUnderRegion[] = $regionId;
        } else {
            $positionsUnderRegion[] = $regionId;
        }

        $adminUserCollection = Mage::getModel('admin/user')
            ->getCollection()
            ->addFieldToFilter('salesrep_position_id', array('in' => $positionsUnderRegion));

        $salesrepArray = array();

        foreach($adminUserCollection as $admin){
            $salesrepArray[] = $admin->getId();
        }

        return $salesrepArray;
    }

    public function getGoal($regionId, $year, $ytdPV){
        $filters = $this->getFilters();
        $goalCollection = Mage::getModel('cminds_positions/customergoal')
            ->getCollection()
            ->addFieldToFilter('period_type', array('eq' => 2))
            ->addFieldToFilter('period_year', array('eq' => $year));
        if($this->isTopLevel()){
            if(isset($filters['region_id'])){
                $goalCollection
                    ->addFieldToFilter('type_id', array('eq' => 2))
                    ->addFieldToFilter('office_id', array('eq' => $regionId));
            } else {
                $goalCollection
                    ->addFieldToFilter('type_id', array('eq' => 3))
                    ->addFieldToFilter('region_id', array('eq' => $regionId));
            }
        } else {
            $goalCollection
                ->addFieldToFilter('type_id', array('eq' => 2))
                ->addFieldToFilter('office_id', array('eq' => $regionId));
        }

        if($goalCollection->getFirstItem()->getId()) {
            $goal = ($ytdPV/$goalCollection->getFirstItem()->getPeriodTarget())*100;
        } else {
            $goal = 0;
        }
        return round($goal,2) . "%";
    }

    public function setFilters()
    {
        $filter = Mage::app()->getRequest()->getParam('filter', null);
        if (is_null($filter)) {
            $filter = $this->_defaultFilter;
        }

        if (is_string($filter)) {
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);

            $this->setFilter('from', $data['from']);

            if(isset($data['office_data'])){
                if(is_array($data['office_data'])){
                    if(isset($data['region_id']) && $data['region_id'] != 0){
                        $this->setFilter('office_data', $data['office_data'][$data['region_id']]);
                        $this->setFilter('region_id', $data['region_id']);
                    }
                } else {
                    $this->setFilter('office_data', $data['office_data']);
                }
            }
            if(isset($data['filter_from']) && isset($data['filter_to'])) {
                $this->setFilter('filter_from', $data['filter_from']);
                $this->setFilter('filter_to', $data['filter_to']);
            }

        } else {
            $this->setFilter('from', date('Y-m-d'));
        }


        return $this;
    }

    public function getFilters(){
        return $this->_filters;
    }


    public function getFilter($name)
    {

        if (isset($this->_filters[$name])) {
            return $this->_filters[$name];
        } else {
            return ($this->getRequest()->getParam($name)) ? htmlspecialchars($this->getRequest()->getParam($name)) : '';
        }
    }

    public function setFilter($name, $value)
    {
        if ($name) {
            $this->_filters[$name] = $value;
        }
    }

    public function getPeriodArray(){
        $filters = $this->getFilters();
        $periodMonthArray = array();

        if(isset($filters['filter_from']) && isset($filters['filter_to'])) {
            $start = $month = strtotime($filters['filter_from']);
            $end = strtotime($filters['filter_to']);
            while ($month <= $end) {
                $dateArray = array();
                $dateArray['start_date'] = date('d-m-Y', $month);
                $dateArray['end_date'] = date('t-m-Y', $month);
                $nextMonth = strtotime("+1 month", $month);
                if ($nextMonth >= $end) {
                    $dateArray['end_date'] = date('d-m-Y', $end);
                }
                $month = strtotime(date("01-m-Y", $nextMonth));
                $periodMonthArray[] = $dateArray;
            }
        } else {
            if(isset($filters['from'])) {
                $start = strtotime($filters['from']);
                $dateArray = array();
                $dateArray['start_date'] = date('01-m-Y', $start);
                $dateArray['end_date'] = date('t-m-Y', $start);
                $periodMonthArray[] = $dateArray;
            }
        }
        return $periodMonthArray;
    }
}

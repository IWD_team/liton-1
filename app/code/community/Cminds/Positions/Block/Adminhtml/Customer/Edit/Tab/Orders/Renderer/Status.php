<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $orderModel = Mage::getModel('sales/order')->load($row->getId());
        if($orderModel->getId()){
            $hasShipments = $orderModel->hasShipments();
            if($hasShipments) {
                return 'Shipped';
            } else {
                return 'Not Shipped';
            }
        }
    }
}
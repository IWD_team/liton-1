<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit extends Mage_Adminhtml_Block_Customer_Edit
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->canSeeElement()){
            $this->_removeButton('order');
            $this->_removeButton('save');
            $this->_removeButton('reset');
            $this->_removeButton('delete');
            $this->_removeButton('save_and_continue');
        }
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if(!$this->canSeeElement()){
            $this->_removeButton('save_and_continue');
        }
        return $this;
    }

    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }
	

    public function getHeaderText()
    {
        if (Mage::registry('current_customer')->getId()) {
			if(Mage::registry('current_customer')->getImportCustomId()){
				return $this->escapeHtml(Mage::registry('current_customer')->getName() . ' #' .Mage::registry('current_customer')->getImportCustomId());
			} else {
				return $this->escapeHtml(Mage::registry('current_customer')->getName());	
			}
        }
        else {
            return Mage::helper('customer')->__('New Customer');
        }
    }
}

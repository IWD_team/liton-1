<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Ordered extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $totalOrdered = 0;
        $orderModel = Mage::getModel('sales/order')->load($row->getId());
        if($orderModel->getId()){
            if($orderModel->getBaseGrandTotal()){
                $totalOrdered = $orderModel->getBaseGrandTotal();
            }
        }
        return Mage::helper('core')->currency($totalOrdered, true, false);
    }
}
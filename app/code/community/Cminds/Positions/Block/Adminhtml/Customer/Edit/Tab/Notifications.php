<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Notifications extends Mage_Adminhtml_Block_Template
                                                                  implements Mage_Adminhtml_Block_Widget_Tab_Interface {

  public function __construct() {
    parent::__construct();
    $this->setTemplate('cminds_positions/customer/edit/tab/notifications.phtml');
  }


  public function getAdmins() {
    return LucidPath_SalesRepDeluxe_Model_Source_UsersList::toOptionArray();
  }

  /**
  * Prepare label for tab
  *
  * @return string
  */
  public function getTabLabel() {
    return $this->__('Notifications');
  }

  /**
  * Prepare title for tab
  *
  * @return string
  */
  public function getTabTitle() {
    return $this->__('Notifications');
  }

  /**
  * Returns status flag about this tab can be shown or not
  *
  * @return true
  */
  public function canShowTab() {
      return true;
  }

  /**
  * Returns status flag about this tab hidden or not
  *
  * @return true
  */
  public function isHidden() {
    return false;
  }

}

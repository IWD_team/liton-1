<?php

class Cminds_Positions_Adminhtml_NotificationsController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction(){
        $this->_title($this->__('Goals Management'));

        $params = $this->getRequest()->getParams();

        Mage::register('notification_filter_params', $params);

        $this->loadLayout();
        $this->_setActiveMenu('system');
        $this->getLayout()->getBlock('salesrep_notifications_grid')
            ->setCustomers($this->getRequest()->getPost('customers', null));
        $this->renderLayout();
    }

    public function notificationgridAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()
                ->getBlock('salesrep_notifications_grid')
                ->toHtml()
        );
    }

    public function editAction()
    {
        $goal_id = $this->getRequest()->getParam('id', null);
        $model = Mage::getModel('cminds_positions/customergoal')->load($goal_id);

        if ($model) {
            Mage::register('goal_data', $model);
        }

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('cminds_positions/adminhtml_goals_container_edit'));
        $this->renderLayout();
    }

    public function deleteAction() {
        if ($goalId = $this->getRequest()->getParam('id', false)) {
            $field = Mage::getModel('cminds_positions/customergoal');
            $field->load($goalId);

            if (!$field->getId()) {
                $this->_getSession()->addError(
                    $this->__('This goal no longer exists.')
                );
            }

            try {
                $field->delete();

                $this->_getSession()->addSuccess(
                    $this->__('Goal has bees deleted successfuly.')
                );
                return $this->_redirect(
                    '*/*/index'
                );
            } catch(Exception $e) {
                $this->_getSession()->addError(
                    $this->__('Can not delete this goal.')
                );
                return $this->_redirect(
                    '*/*/index'
                );
            }
        }

        return $this->_redirect(
            '*/*/index'
        );
    }

    public function saveAction() {
        $params = $this->getRequest()->getParams();

        try {
            $goalModel = Mage::getModel('cminds_positions/customergoal');

            switch($params['type_id']){
                case 0:
                    $this->_getSession()->addError($this->__('Please select goal type'));

                    return $this->_redirect(
                        '*/*/index'
                    );
                    break;
                case 1:
                    unset($params['region_id']);
                    unset($params['office_id']);

                    if(isset($params['customer_id'])){
                        $customer = Mage::getModel('customer/customer')->load($params['customer_id']);

                        if($customer->getId()){
                            $salesrepId = $customer->getSalesrepRepId();
                            $salesrep = Mage::getModel('admin/user')->load($salesrepId);

                            if($salesrep->getId()){
                                $salesrepPositionId = $salesrep->getSalesrepPositionId();

                                $position = Mage::getModel('cminds_positions/position')->load($salesrepPositionId);

                                if($position->getId()){
                                    $params['office_id'] = $position->getId();

                                    if($position->getParentId() != 0 || $position->getParentId() != null){
                                        $params['region_id'] = $position->getParentId();
                                    }
                                }
                            }
                        }

                    } else {
                        $this->_getSession()->addError($this->__('Please select customer'));

                        return $this->_redirect(
                            '*/*/index'
                        );
                    }

                    break;
                case 2:
                    unset($params['customer_id']);

                    $selectedRegionId = $params['region_id'];

                    if(isset($params['office_id'][$selectedRegionId]) && $params['office_id'][$selectedRegionId] != 0) {
                        $regionOffice = $params['office_id'][$selectedRegionId];

                        $params['office_id'] = $regionOffice;
                    } else {
                        $this->_getSession()->addError($this->__('Please select office'));

                        return $this->_redirect(
                            '*/*/index'
                        );
                    }
                    break;
                case 3:
                    unset($params['customer_id']);
                    unset($params['office_id']);
                    break;
            }

            if(isset($params['id'])){
                $goalModel->load($params['id']);
            } else {
                $goalModel
                    ->setId(null);
            }

            $goalModel
                ->addData($params)
                ->save();

            $this->_getSession()->addSuccess($this->__('The goal has been saved.'));

            return $this->_redirect(
                '*/*/index'
            );
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            return $this->_redirect(
                '*/*/index'
            );
        }
    }

    public function saveNotificationsAction(){

        $params = $this->getRequest()->getParams();
        $notifiedCustomerArray = array();

        $result = array('success' => false);

        if(isset($params['serialized_notifications'])){
            parse_str($params['serialized_notifications'], $notifiedCustomerArray);
            $arrayKeys = array_keys($notifiedCustomerArray);
            $customerCollection = Mage::getModel('customer/customer')->getCollection();
            foreach($customerCollection as $customer){
                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                $table = Mage::getSingleton('core/resource')->getTableName('customer_entity');
                if(in_array($customer->getId(), $arrayKeys)) {

                    $write->query("UPDATE {$table} SET salesrep_notify_when_order_placed = " . 1 . " WHERE entity_id = " . $customer->getId() . ";");
                    $write->query("UPDATE {$table} SET salesrep_notify_when_order_shipped = " . 1 . " WHERE entity_id = " . $customer->getId() . ";");
                } else {
                    $write->query("UPDATE {$table} SET salesrep_notify_when_order_placed = " . 0 . " WHERE entity_id = " . $customer->getId() . ";");
                    $write->query("UPDATE {$table} SET salesrep_notify_when_order_shipped = " . 0 . " WHERE entity_id = " . $customer->getId() . ";");
                }
            }
            $result = array('success' => true);
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
    }
	
    protected function _isAllowed()
       {
           return true;
       }
}
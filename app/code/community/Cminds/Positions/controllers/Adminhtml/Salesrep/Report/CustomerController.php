<?php
class Cminds_Positions_Adminhtml_Salesrep_Report_CustomerController extends Mage_Adminhtml_Controller_Report_Abstract {

    public function indexAction(){
        $this->_title($this->__('Reports'))
            ->_title($this->__('Products'))
            ->_title($this->__('Products Ordered'));

        $this->_initAction()
            ->_setActiveMenu('report/product/sold')
            ->_addBreadcrumb(Mage::helper('reports')->__('Products Ordered'), Mage::helper('reports')->__('Products Ordered'))
            ->_addContent($this->getLayout()->createBlock('cminds_positions/adminhtml_report_customer_totals'))
            ->renderLayout();
    }

    protected function _isAllowed()
    {
        return true;
    }

    public function filteredCustomersAction(){
        $params = $this->getRequest()->getParams();

        $officeId = $params['office_id'];

        $helper = Mage::helper('cminds_positions');
        $adminIds = $helper->getAdminIdsByOfficeId($officeId);
        $customersCollection = $helper->getCustomersIdsByAssignedRepIds($adminIds);

        $result = '';
        $result .= '<option value="">Please select</option>';
        foreach($customersCollection as $customer){
            $customerData = Mage::getModel('customer/customer')->load($customer->getId());
            $result .= '<option value="' . $customer->getId() . '">' . $customerData->getName() . '</option>';
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
    }
}

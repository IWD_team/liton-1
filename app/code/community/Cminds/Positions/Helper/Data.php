<?php
class Cminds_Positions_Helper_Data extends Mage_Checkout_Helper_Data {

    protected $_module_name = 'Cminds_Positions';
    protected $_subordinateIds = null;

    /**
     * Return shipping status list for order grid filter
     *
     * @return array
     */
    public function getCommissionStatusList() {
        $result = array();

        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $table = Mage::getSingleton('core/resource')->getTableName('salesrep/salesrep');

        $res = $read->fetchAll("SELECT DISTINCT(commission_status) FROM {$table} WHERE commission_status IS NOT NULL ORDER BY commission_status;");

        foreach ($res as $item) {
            $result[$item['commission_status']] = $item['commission_status'];
        }

        return $result;
    }

    /**
     * Return admins list for salesrep dropdown
     *
     * @return array
     */
    public function getAdminsList() {
        $selected_admins = explode(',', Mage::getStoreConfig('salesrep/step_setup/users'));
        $all_admins    = Mage::getResourceModel('admin/user_collection')->setOrder('firstname', 'asc')->load();

        $result   = array();
        $result[] = array('value' => "0", 'label' => "No Personal Stylist");
        foreach ($all_admins as $admin) {
//            if (in_array($admin->getId(), $selected_admins)) {
            $result[] = array('value' => $admin->getId(), 'label' => $admin->getFirstname() .' '. $admin->getLastname());
//            }
        }
        return $result;
    }
    public function getAdminsListForFilter() {
        $all_admins    = Mage::getResourceModel('admin/user_collection')->setOrder('firstname', 'asc')->load();

        $result   = array();
        foreach ($all_admins as $admin) {
            $result[$admin->getId()] = $admin->getFirstname() .' '. $admin->getLastname();
        }
        return $result;
    }

    /**
     * Return status list for orders
     *
     * @return array
     */
    public function getStatusList() {
        $result = array();
        $result[] = array('value' => 'Unpaid', 'label' => 'Unpaid');
        $result[] = array('value' => 'Paid', 'label' => 'Paid');
        $result[] = array('value' => 'Ineligible', 'label' => 'Ineligible');
        $result[] = array('value' => 'Canceled', 'label' => 'Canceled');

        return $result;
    }

    /**
     * Return status list for order order grid filter
     *
     * @return array
     */
    public function getStatusListFilter() {
        $result = array('Unpaid' => 'Unpaid', 'Paid' => 'Paid', 'Ineligible' => 'Ineligible', 'Canceled' => 'Canceled');

        return $result;
    }
    public function setCommissionEarned($order_id, $rep_id=null, $manager_id=null, $is_new = true) {
        $order    = Mage::getModel('sales/order')->load($order_id);
        $salesrep = Mage::getModel('salesrep/salesrep')->loadByOrder($order);

        if ($rep_id) {
            $rep_model = Mage::getModel('admin/user')->load($rep_id);

            if (!$rep_model->getId()) {
                return;
            }

            $salesrep->setRepId($rep_model->getId());
            $salesrep->setRepName($rep_model->getFirstname() ." ". $rep_model->getLastname());

            $rep_commission_earned = 0;

            if($is_new) {
                $commission = $this->calculateForCurrentMonth($rep_model->getId(), $order->getGrandTotal());
            } else {
                $commission = $this->calculateForCurrentMonth($rep_model->getId());
            }

            if (!$commission) {
                $commission = floatval(Mage::getStoreConfig('salesrep/setup/default_rep_commission')) / 100;
            }

            $orderItems = $order->getAllItems();

            $orderSubtotal = 0;

            foreach($orderItems as $item){

                $product = $item->getProduct();

                if ($product->getSalesrepIsExcluded() == '1') {
                    continue;
                }

                $qty   = $item->getQtyOrdered();
                $price = $item->getBasePrice();

                $orderSubtotal += $price * $qty;
            }
            $rep_commission_earned = $orderSubtotal * $commission;

            $salesrep->setRepCommissionEarned(round($rep_commission_earned, 2));
//            $salesrep->setRepPercentage($commission);
            $salesrep->save();

            if(!Mage::getStoreConfig('salesrep/setup/fix_commissions_daily')) {
                $this->_refreshPastOrderCommissions($rep_id, $commission);
            }

            $this->_applyManagersCommission($rep_model, $order, $rep_commission_earned, $is_new);
        } else if ($manager_id != null) {
            $rep_model = Mage::getModel('admin/user')->load($rep_id);

            $rep_commission_earned = 0;
            $items = $order->getAllItems();
            foreach ($items as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());

                $qty   = $item->getQtyOrdered();
                $price = $item->getPrice();

                $commission = $product->getSalesrepRepCommissionRate();

                if (!$commission) {
                    if ($rep_model->getSalesrepRepCommissionRate()) {
                        $commission = $rep_model->getSalesrepRepCommissionRate();
                    } else {
                        $commission = floatval(Mage::getStoreConfig('salesrep/setup/default_rep_commission')) / 100;
                    }
                }
                $rep_commission_earned += $price*$qty*$commission;
            }

            $this->_applyManagersCommission($salesrep, $order, $rep_commission_earned, $is_new);
        } else {
            $salesrep->setRepCommissionEarned(null);
            $salesrep->setRepPercentage(null);
            $salesrep->setRepCommissionStatus(null);
        }

        $salesrep->setRepCommissionStatus(Mage::getStoreConfig('salesrep/setup/default_status'));
        $salesrep->save();

    }

    private function _applyManagersCommission($rep_model, $order, $salesCommission, $is_new) {
        $manager_model = Mage::getModel('admin/user')->load($rep_model->getSalesrepManagerId());

        if ($manager_model->getId()) {
            $s = Mage::getModel('salesrep/salesrep')
                ->setOrderId($order->getId())
                ->setRepId($manager_model->getId())
                ->setRepName($manager_model->getFirstname() .' '. $manager_model->getLastname())
                ->setRepCommissionStatus(Mage::getStoreConfig('salesrep/setup/default_status'));

            $commission = 0;
            if ($is_new) {
                $commission = $this->calculateForCurrentMonth($manager_model->getId(), $order->getGrandTotal());
            } else {
                $commission = $this->calculateForCurrentMonth($rep_model->getId());
            }

            if ($commission) {
                $manager_commission_rate = floatval($commission) / 100;
            } else {
                $manager_commission_rate = floatval($manager_model->getSalesrepManagerCommissionRate());
            }

            if ($manager_commission_rate > 0) {
                $manager_commission = round($salesCommission * $manager_commission_rate, 2);

                $s->setRepCommissionEarned($manager_commission);
                $s->setIsDownline(1);
                $s->save();
            } else {
                $manager_commission = 0;
            }

            if($manager_model->getSalesrepManagerId()) {
                $this->_applyManagersCommission($manager_model, $order, $manager_commission, $is_new);
            }
        } else {
            return;
        }
    }

    public function _getCommissionForOrder($order) {
        $commission = Mage::getModel('salesrep/pricerange')
            ->getCollection()
            ->addFieldToFilter('from_value', array(
                    'from_value' => $order->getGrandTotal(),
                )
            )
            ->addFieldToFilter('from_value',array(
                    'to_value' => $order->getGrandTotal(),
                )
            )->getFirstItem();

        if($commission->getId()) {
            return floatval($commission->getPercentageRate()) / 100;
        }
        return 0;
    }

    public function getCommissionForAmount($amount, $RepId, $isFirstGen) {
        $admin = Mage::getModel('admin/user')->load($RepId);

        $commissions = Mage::getModel('cminds_positions/pricerange')
            ->getCollection()
            ->addFieldToFilter('from_value', array(
                    'to' => $amount,
                )
            )
            ->addFieldToFilter('to_value',array(
                    'from' => $amount,
                )
            )
            ->addFieldToFilter('position_id',array(
                'eq' => $admin->getSalesrepPositionId()
            ));

        $commission = $commissions->getFirstItem();
        if ($commission->getId()) {
            $positionsModel = Mage::getModel('cminds_positions/position')->load($admin->getSalesrepPositionId());

            $minUserCommission = floatval($positionsModel->getMinimumCommission());
            $priceRangeValue = floatval($commission->getPercentageRate());

            if ($minUserCommission > $priceRangeValue) {
                $comm = $minUserCommission / 100;
            } else {
                $comm = $priceRangeValue / 100;
            }
        } else {
            $positionsModel = Mage::getModel('cminds_positions/position')->load($admin->getSalesrepPositionId());

            $minUserCommission = floatval($positionsModel->getMinimumCommission());

            $comm = $minUserCommission / 100;
        }

        if($isFirstGen){
            $comm = Mage::getModel('cminds_positions/position')->load($admin->getSalesrepPositionId())->getCommissionFirstGeneration() / 100;
        }

        return $comm;
    }

    public function getSubordinateIds($id=null){
        if (!$this->_subordinateIds) {
            $this->_subordinateIds = array();

            $admin_user_collection = Mage::getResourceModel('admin/user_collection');
            $admin_user_collection->addFieldToFilter('salesrep_manager_id', $id);

            foreach ($admin_user_collection as $admin_user) {
                $this->_subordinateIds[] = $admin_user->getId();
            }

            $this->_subordinateIds[] = $id;
        }

        return $this->_subordinateIds;
    }

    public function getPercent($val, $for_save=false) {
        if ($for_save) {
            if ($val > 100) {
                $val = 100;
            }
            return $val/100;
        }

        return $val ? number_format($val*100, 2) : '';
    }

    public function isAdmin() {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        }

        if (Mage::getDesign()->getArea() == 'adminhtml') {
            return true;
        }

        return false;
    }

    public function isStoreAdmin(){
        $adminUser = Mage::getSingleton('admin/session')->getUser();
        if($adminUser->getIsSalesrep()){
            return false;
        }
        return true;
    }

    public function isTopLevel(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());

        if($positionModel->getId()){
            if($positionModel->getParentId() == 0 || $positionModel->getParentId() == null){
                return true;
            }
        }
        return false;
    }

    public function isRegional(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());


        if($positionModel->getId()){
            $childrenCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $currenUser->getSalesrepPositionId()));
            if($positionModel->getParentId() != 0 && $positionModel->getParentId() != null && $childrenCollection->getSize()){
                return true;
            }
        }
        return false;
    }

    /**
     * Check user permission on resource
     *
     * @param   string $user
     * @param   string $resource
     * @return  boolean
     */
    public function isAllowed($user, $resource) {
        $acl = Mage::getResourceModel('admin/acl')->loadAcl();

        if (!preg_match('/^admin/', $resource)) {
            $resource = 'admin/'.$resource;
        }

        try {
            return $acl->isAllowed($user->getAclRole(), $resource);
        } catch (Exception $e) {
            try {
                if (!$acl->has($resource)) {
                    return $acl->isAllowed($user->getAclRole(), null);
                }
            } catch (Exception $e) { }
        }
        return false;
    }

    public function isModuleInstalled() {
        $s =  Mage::getConfig()->getModuleConfig($this->_module_name);
        return ($s->active);
    }

    public function isModuleActive() {
        return Mage::getConfig()->getModuleConfig($this->_module_name)->is('active', 'true');
    }

    public function isModuleEnabled($module_name = NULL) {
        return $this->isModuleInstalled() && $this->isModuleActive() && Mage::getStoreConfig('salesrep/module_status/enabled');
    }

    public function isFrontendStepEnabled() {
        return Mage::getStoreConfig('salesrep/step_setup/step_enabled');
    }

    public function showFrontendStep() {
        if ($this->isModuleEnabled() && $this->isFrontendStepEnabled()) {

            $_show_rep_step = true;

            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $_customer = Mage::getSingleton('customer/session')->getCustomer();

                if ($_customer->getSalesrepRepId()) {
                    $_show_rep_step = false;
                }
            }

            if ($_show_rep_step) {
                return true;
            }
        }

        return false;
    }

    public function calculateForCurrentMonth($RepId, $additionalSubtotal = 0) {
        $date = new DateTime();
        return $this->calculateMonthlyRepTotal($RepId, $date, $additionalSubtotal);
    }

    public function calculateMonthlyRepTotal($RepId, DateTime $date, $additionalSubtotal, $isDownline = false) {
        $subtotal = 0.0;

        $collection = $this->_getCurrentMonthOrders($RepId, $date);

        $orderSubtotal = 0;
        foreach ($collection['collection'] AS $order) {
            $orderItems = $order->getAllItems();


            foreach($orderItems as $item){

                $product = $item->getProduct();

                if($product->getSalesrepIsExcluded() == '1'){
                    continue;
                }

                $qty   = $item->getQtyOrdered();
                $price = $item->getBasePrice();

                $orderSubtotal += $price * $qty;
            }


            $subtotal = $orderSubtotal + $subtotal;
        }

        $subtotal = $subtotal + $additionalSubtotal;

        $minimumOrdersAmount = Mage::getStoreConfig('salesrep/setup/minimum_orders_amount');

        $isFirstGen = false;
        if ($collection['is_manager'] && ($subtotal >= $minimumOrdersAmount) && $isDownline) {
            $isFirstGen = true;
        }
        return $this->getCommissionForAmount($subtotal, $RepId, $isFirstGen);
    }

    private function _refreshPastOrderCommissions($RepId, $commission) {
        $date = new DateTime();

        if(!$commission) return;

        $collection = $this->_getCurrentMonthOrders($RepId, $date);

        foreach($collection['collection'] AS $order) {
            $salesrep = Mage::getModel('salesrep/salesrep')->load($order->getSalesrepId());


            if (!$salesrep->getId()) {
                continue;
            }

            $rep_commission_earned = 0;

            if (!$commission) {
                $commission = floatval(Mage::getStoreConfig('salesrep/setup/default_rep_commission')) / 100;
            }

            $rep_commission_earned += $order->getGrandTotal() * $commission;
            $salesrep->setCommissionEarned(round($rep_commission_earned, 2));
            $salesrep->setRepPercentage($commission);
            $salesrep->save();
        }
    }

    private function _getCurrentMonthOrders($RepId, DateTime $date) {
        $collection = Mage::getModel('sales/order')->getCollection();
        $collection->getSelect()
            ->joinLeft(
                array(
                    'salesrep'=> 'salesrep'),
                'main_table.entity_id = salesrep.order_id',
                array(
                    'rep_id',
                    'salesrep_id',
                )
            )
            ->where('main_table.created_at >= ?', $date->format('Y-m-01 00:00:00'))
            ->where('main_table.created_at <= ?', $date->format('Y-m-t 23:59:59'))
            ->where('main_table.state != ?', 'canceled');

        $isManager = false;
        $subordinatesArray = $this->getAllSubordinates($RepId);

        $canDisplayDownline = $this->canDisplayDownline($subordinatesArray, $date, $RepId);
        if($canDisplayDownline){
            $subordinatesArray[] = $RepId;
            $collection->addAttributeToFilter('salesrep.rep_id', array('in' => implode(",", $subordinatesArray)));
            $collection->addAttributeToFilter('salesrep.is_downline', array('eq' => 1));
            $isManager = true;
        } else {
//            $collection->addAttributeToFilter('salesrep.rep_id', $RepId);
//            $collection->addAttributeToFilter('salesrep.is_downline', array('eq' => 0));
            $collection->getSelect()
                ->where('salesrep.rep_id = ?', $RepId);
        }


        return array('collection' => $collection, 'is_manager' => $isManager);
    }

    public function setCommissionForStoreManager($order){
        $adminUserCollection = Mage::getModel('admin/user')->getCollection();
        $adminUserCollection
            ->getSelect()
            ->where('salesrep_is_store_manager = ?', 1);

        foreach($adminUserCollection as $admin){
            $salesrepModel = Mage::getModel('salesrep/salesrep');

            $orderSubtotal = $order->getBaseSubtotal();

            $storeManagerCommission = $this->getCommissionForAmount($orderSubtotal, $admin->getId(), false);

            if($storeManagerCommission !== null){
                $commission = $storeManagerCommission;
            } else {
                $commission = floatval(Mage::getStoreConfig('salesrep/setup/default_rep_commission')) / 100;
            }

            $commissionEarned = $orderSubtotal * $commission;

            $salesrepModel
                ->setOrderId($order->getId())
                ->setAdminId($admin->getId())
                ->setAdminName($admin->getFirstname() . ' ' . $admin->getLastname())
                ->setCommissionEarned($commissionEarned)
                ->save();
        }

    }

    public function getAllSubordinates($positionId = null, $startFromSelected = false)
    {
        if(!$positionId && !$startFromSelected){
            $positionId = Mage::getSingleton('admin/session')->getUser()->getSalesrepPositionId();
        }

        $subordinates = array();

        $this->getSubordinatePositions($positionId, $subordinates);

        if($startFromSelected && empty($subordinates)){
            $subordinates[] = $positionId;
        }
        return $subordinates;
    }

    protected function getSubordinatePositions($positionId, &$subordinates){
        $adminUserCollection = Mage::getModel('cminds_positions/position')->getCollection()
            ->addFieldToFilter('parent_id', $positionId);
		
		$adminUserCollection->getSelect()
		            ->order('name DESC');
        foreach($adminUserCollection as $subordinateAdmin){
            $subordinates[] = (int) $subordinateAdmin->getId();
            $this->getSubordinatePositions($subordinateAdmin->getId(), $subordinates);
        }

    }

    public function getAvailableRepsForOrderGrid(){
        $availablePositions = $this->getAllSubordinates();

        $adminUser = Mage::getSingleton('admin/session')->getUser();

        $adminUserCollection = Mage::getModel('admin/user')->getCollection();

        $repsToFilterArray = array();
        if(!empty($availablePositions)){
            $matchingReps = $adminUserCollection
                ->addFieldToFilter(
                    'salesrep_position_id',
                    array('in' => $availablePositions)
                );

            foreach($matchingReps as $rep){
                $repsToFilterArray[] = $rep->getId();
            }
        }

        $repsToFilterArray[] = $adminUser->getId();

        return $repsToFilterArray;
    }

    public function getPositionTypes() {
        return Mage::getModel("cminds_positions/positiontype")->getCollection();
    }

    public function getPosition($typeId) {
        return Mage::getModel("cminds_positions/position")
            ->getCollection()
            ->addFieldToFilter('type_id', array('eq' => $typeId));
    }

    public function getAdminChild(){
        $adminUser = Mage::getSingleton('admin/session')->getUser();
    }

    public function getCurrentAdmin(){
        $adminUser = Mage::getSingleton('admin/session')->getUser();

        return $adminUser;
    }

    public function getCurrentAdminRegions(){
        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('parent_id', $this->getCurrentAdmin()->getSalesrepPositionId());

        $regionArray = array();

        foreach($positionsCollection as $position){
            $regionArray[] = $position->getId();
        }
        return $regionArray;
    }

    public function getCurrentAdminOffices(){
        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection();

        if($this->isTopLevel()) {
            $positionsCollection
                ->addFieldToFilter('parent_id',
                    array('in' => $this->getCurrentAdminRegions()));
        } elseif($this->isRegional()){
            $positionsCollection
                ->addFieldToFilter('parent_id',
                    array('in' => $this->getCurrentAdmin()->getSalesrepPositionId()));
        }


        $officesArray = array();

        foreach($positionsCollection as $position){
            $officesArray[] = $position->getId();
        }
        return $officesArray;
    }

    public function getFilterSalesreps(){
        $region = Mage::registry('report_region');
        $office = Mage::registry('region_office');
        $customer = Mage::registry('region_customer');

        $salesrepArray = array();

        if($customer){
            $orderCollection = Mage::getModel('sales/order')->getCollection();

            $orderCollection
                ->addFieldToFilter('customer_id', array('eq' => $customer));

            $orderIdsArray = array();

            foreach($orderCollection as $order){
                $orderIdsArray[] = $order->getId();
            }

            $salesrepCollection = Mage::getModel('salesrep/salesrep')->getCollection();

            $salesrepCollection
                ->addFieldToFilter('order_id', array('in' => $orderIdsArray));

            $salesrepArray = array();

            foreach($salesrepCollection as $salesrep){
                $salesrepArray[] = $salesrep->getRepId();
            }
        } elseif($office){
            $salesrepArray = array();

            $adminCollection = Mage::getModel('admin/user')
                ->getCollection()
                ->addFieldToFilter('salesrep_position_id', array('eq', $office));

            foreach($adminCollection as $admin){
                $salesrepArray[] = $admin->getId();
            }
        } elseif($region){
            $positionIdsArray = $this->getAllSubordinates($region);
//            $positionsCollection = Mage::getModel('cminds_positions/positiontype')->getCollection();
//
//            $positionsCollection
//                ->addFieldToFilter('parent_id', array('eq' => $region));
//
//            $positionIdsArray = array();
//
//            foreach($positionsCollection as $position){
//                $positionIdsArray[] = $position->getId();
//            }

            $salesrepArray = array();

            $adminCollection = Mage::getModel('admin/user')
                ->getCollection()
                ->addFieldToFilter('salesrep_position_id', array('in', $positionIdsArray));

            foreach($adminCollection as $admin){
                $salesrepArray[] = $admin->getId();
            }
        } else {
            $officeId = $this->getCurrentAdmin()->getSalesrepPositionId();
            $adminIds = $this->getAdminIdsByOfficeId($officeId);
            $customersCollection = $this->getCustomersIdsByAssignedRepIds($adminIds);

            $salesrepCustomersArray = array();

            foreach($customersCollection as $customer){
                $salesrepCustomersArray[] = $customer->getId();
            }

            $orderCollection = Mage::getModel('sales/order')->getCollection();

            $orderCollection
                ->addFieldToFilter('customer_id', array('in' => $salesrepCustomersArray));

            $orderIdsArray = array();

            foreach($orderCollection as $order){
                $orderIdsArray[] = $order->getId();
            }

            $salesrepCollection = Mage::getModel('salesrep/salesrep')->getCollection();

            $salesrepCollection
                ->addFieldToFilter('order_id', array('in' => $orderIdsArray));

            $salesrepArray = array();

            foreach($salesrepCollection as $salesrep){
                $salesrepArray[] = $salesrep->getRepId();
            }
        }
        // if(empty($salesrepArray)){
//             $salesrepArray[] = '0';
//         }
        return $salesrepArray;
    }

    public function getAdminIdsByOfficeId($officeId){

        $adminIds = array();
        $adminCollection = Mage::getModel('admin/user')
            ->getCollection()
            ->addFieldToFilter('salesrep_position_id', array('eq' => $officeId));

        foreach ($adminCollection as $admin) {
            $adminIds[] = $admin->getUserId();
        }
        return $adminIds;
    }

    public function getCustomersIdsByAssignedRepIds(array $adminIds){

        $customersCollection = Mage::getModel('customer/customer')
            ->getCollection();

        $customersCollection
            ->getSelect()
            ->where('salesrep_rep_id IN ('. implode(', ',$adminIds).')');

        return $customersCollection;
    }

    public function getReportCustomersBySalesrep(){
        $salesrep = $this->getFilterSalesreps();

        $salesrepCollection = Mage::getModel('salesrep/salesrep')->getCollection();

        $salesrepCollection->addFieldToFilter('rep_id', array('in' => $salesrep));

        $orderIds = array();

        foreach($salesrepCollection as $rep){
            $orderIds[] = $rep->getOrderId();
        }

        $orderCollection = Mage::getModel('sales/order')->getCollection();

        $orderCollection->addFieldToFilter('entity_id', array('in' => $orderIds));

        $customersArray = array();

        foreach($orderCollection as $order){
            if($customerId = $order->getCustomerId()){
                $customersArray[] = $customerId;
            }
        }

        return $customersArray;
    }
}
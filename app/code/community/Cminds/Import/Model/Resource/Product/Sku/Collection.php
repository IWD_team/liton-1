<?php
class Cminds_Import_Model_Resource_Product_Sku_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    protected function _construct()
    {
        $this->_init('import/product_sku');
    }
}
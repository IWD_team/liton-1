<?php
/**
 * CreativeMindsSolutions
 */
// fixes an issue after 1.6.1 where a file was not declared abstract 
if (version_compare(Mage::getVersion(), '1.6.0', '>')===true) {
class Cminds_Import_Model_Resource_Order_Item extends Mage_Sales_Model_Resource_Abstract
{
    protected function _construct()
    {
        $this->_init('import/order_item', 'item_id');
    }
    
}
}else{
class Cminds_Import_Model_Resource_Order_Item extends Mage_Sales_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('import/order_item', 'item_id');
    }
}
}

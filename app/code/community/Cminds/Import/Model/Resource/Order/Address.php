<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Resource_Order_Address extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
    {    
        $this->_init('import/order_address', 'entity_id');
    }
}
<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Resource_Order_Address extends Mage_Eav_Model_Entity_Abstract
{
	public function __construct()
    {
    	if(version_compare('1.4.1.0', Mage::getVersion(), '<=')) {
    		$this->_init('import/order_address', 'entity_id');
    	} else {
    		$resource = Mage::getSingleton('core/resource');
       	    $this->setType('order_address')->setConnection(
            	$resource->getConnection('sales_read'),
            	$resource->getConnection('sales_write')
        	);
    	}
        
    }
}
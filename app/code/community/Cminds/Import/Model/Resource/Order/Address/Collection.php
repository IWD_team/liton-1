<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Resource_Order_Address_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('import/order_address');
    }

    public function setOrderFilter($orderId)
    {
        $this->addFieldToFilter('parent_id', $orderId);
        return $this;
    }

}
<?php

class Cminds_Import_Model_Products extends Mage_Core_Model_Abstract
{
    protected $resource;
    protected $writeConnection;
    protected $readConnection;

    protected $insertSql = [];
    protected $insertSqlBatchSize = 200;

    public function __construct()
    {
        $this->resource = Mage::getSingleton('core/resource');
        $this->writeConnection = $this->resource->getConnection('core_write');
        $this->readConnection = $this->resource->getConnection('core_read');
    }

    public function run()
    {
        $executionStart = microtime(true);

        Mage::log(
            'products sku generation start',
            null,
            'cminds_products_skus.log'
        );

        try {
            set_time_limit(0);
            ini_set('memory_limit', '-1');

            echo 'Starting generation process.' . "\n\n";

            $productsTable = $this->resource
                ->getTableName('catalog_product_entity');

            $q = 'SELECT entity_id, sku FROM ' . $productsTable
                . ' ORDER BY entity_id DESC';
            $products = $this->readConnection->fetchAll($q);
            $productsCount = count($products);

            echo $productsCount . ' products to process.' . "\n\n";

            $i = 1;
            foreach ($products as $product) {
                echo '[' . $i . '/' . $productsCount
                    . '] product with id "' . $product['entity_id']
                    . '" is processing.' . "\n";
                $i++;

                $this->processProduct($product['entity_id'], $product['sku']);
            }
        } catch (Exception $e) {
            Mage::log(
                '[' . $e->getFile() . ':' . $e->getLine() . '] ' . $e->getMessage(),
                null,
                'cminds_products_skus.log'
            );
        }

        $executionEnd = microtime(true);
        $executionTime = $executionEnd - $executionStart;

        echo "\n" . 'Generation process has been finished.' . "\n";
        echo 'Finished in: '
            . $this->getFormattedTimeString($executionTime) . "\n\n";

        return $this;
    }

    protected function processProduct($productId, $productSku)
    {
        $q = 'SELECT v.option_id, v.sku FROM catalog_product_option_type_value AS v '
            . ' INNER JOIN catalog_product_option AS o'
            . ' ON v.option_id = o.option_id'
            . ' WHERE o.product_id = ' . $productId . ' AND v.sku != ""'
            . ' ORDER BY o.sort_order ASC, v.sort_order ASC;';
        $productOptions = $this->readConnection->fetchAll($q);
        $productOptionsCount = count($productOptions);

        echo "\t" . $productOptionsCount . ' options found.' . "\n";
        echo "\t" . 'Generating skus.' . "\n";

        $optionsSkus = [];
        foreach ($productOptions as $option) {
            $optionsSkus[$option['option_id']][] = $option['sku'];
        }
        unset(
            $productOptions,
            $option
        );
        $optionsSkus = array_values($optionsSkus);

        $collect2 = [];
        $collect = $this->combinations($optionsSkus);
        foreach ($collect as $c) {
            $collect2[] = $this->combinations2($c);
        }

        $collectToSave = [];
        $collectToSaveString = [];
        foreach ($collect2 as $c2) {
            foreach ($c2 as $index => $value) {
                if (!empty($value)) {
                    if (!in_array(json_encode($value), $collectToSaveString)) {
                        $collectToSave[] = $value;
                        $collectToSaveString[] = json_encode($value);
                    }
                }
            }
        }

        echo "\t" . 'Processing generated skus.' . "\n";

        if (count($collectToSave) > 0) {
            foreach ($collectToSave as $o) {
                if (is_array($o)) {
                    $o2 = implode($o, '');
                    $o = implode($o, ',');
                } else {
                    $o2 = $o;
                }

                $generatedSku = $productSku . $o2;

                $this->insertSku($productId, $generatedSku, $o);
                $this->executeBulkInsertion();
            }
        } else {
            foreach ($optionsSkus as $option_id => $skuu) {
                foreach ($skuu as $s) {
                    $generatedSku = $productSku . $s;

                    $this->insertSku($productId, $generatedSku, $s);
                    $this->executeBulkInsertion();
                }
            }
        }

        $this->executeBulkInsertion(true);
    }

    protected function insertSku($productId, $sku, $optionSkus)
    {
        $q = sprintf(
            'INSERT INTO products_skus (product_id, sku, options_skus)'
            . ' VALUES("%s", "%s", "%s");',
            $productId,
            $sku,
            $optionSkus
        );
        $this->insertSql[$sku] = $q;

        return true;
    }

    protected function executeBulkInsertion($force = false)
    {
        $insertsCount = count($this->insertSql);
        if ($insertsCount === 0) {
            return true;
        }

        if ($force === false && $insertsCount < $this->insertSqlBatchSize) {
            return true;
        }

        $skus = array_keys($this->insertSql);

        $q = sprintf(
            'SELECT sku FROM products_skus WHERE sku in (%s);',
            '"' . implode('","', $skus) . '"'
        );
        $existingSkus = $this->readConnection->fetchCol($q);

        if (count($existingSkus) > 0) {
            $existingSkus = array_flip($existingSkus);
            $this->insertSql = array_diff_key($this->insertSql, $existingSkus);

            $insertsCount = count($this->insertSql);
            if ($insertsCount === 0) {
                return true;
            }
        }

        $q = implode('', $this->insertSql);
        
        try {
            $this->writeConnection->query($q);
        } catch (Exception $e) {
            echo "\t" . 'Error during bulk insertion.' . "\n";
            Mage::log(
                '[' . $e->getFile() . ':' . $e->getLine() . '] ' . $e->getMessage(),
                null,
                'cminds_products_skus.log'
            );
            Mage::log(
                $q,
                null,
                'cminds_products_skus.log'
            );

            $this->insertSql = [];

            return false;
        }

        /*$skusOutput = implode("\n\t", $skus);
        echo "\t" . $skusOutput . "\n";*/
        echo "\t" . 'Inserting ' . $insertsCount . ' skus.' . "\n";

        $this->insertSql = [];

        return true;
    }

    protected function combinations($arrays, $i = 0)
    {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i === (count($arrays) - 1)) {
            return $arrays[$i];
        }

        $tmp = $this->combinations($arrays, $i + 1);

        $result = array();

        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t)
                    ? array_merge(array($v), $t)
                    : array($v, $t);
            }
        }

        return $result;
    }

    protected function combinations2($array)
    {
        if (is_array($array) === false) {
            return $array;
        }

        $num = count($array);
        $total = pow(2, $num);

        $return = [];
        for ($i = 0; $i < $total; $i++) {
            $returnTmp = [];
            for ($j = 0; $j < $num; $j++) {
                if (pow(2, $j) & $i) {
                    if (!empty($array[$j])) {
                        $returnTmp[] = $array[$j];
                    }
                }
            }
            $return[] = $returnTmp;
        }

        return $return;
    }

    protected function getFormattedTimeString($seconds)
    {
        return sprintf(
            '%02d%s%02d%s%02d',
            floor($seconds / 3600),
            ':',
            ($seconds / 60) % 60,
            ':',
            $seconds % 60
        );
    }
}

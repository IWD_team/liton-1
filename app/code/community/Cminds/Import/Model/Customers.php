<?php
class Cminds_Import_Model_Customers extends Mage_Core_Model_Abstract
{

    /**
     * Path to csv.
     */
    const CSV_URL = '/csv/customers.csv';

    /**
     * Array with customer data from csv.
     *
     * @var array
     */
    protected $customersArr = array();

    /**
     * Create customers.
     *
     * @return $this
     */
    public function createCustomers()
    {
        try {
            set_time_limit(0);
            ini_set('memory_limit', '-1');

            echo 'Starting customers import.' . "\n\n";

            $i = 1;
            $this->customersArr = $this->prepareCsvArr();
            $customersCount = count($this->customersArr);

            foreach ($this->customersArr as $customerArr) {
                try {
                    $customerModel = self::createCustomer($customerArr);
                    self::createCustomerBillingAddress($customerModel, $customerArr);
                    self::createCustomerShippingAddress($customerModel, $customerArr);

                    echo 'Customer processed [' . $i . '/' . $customersCount . ']' . "\n";
                    $i++;
                } catch (Exception $e) {
                    Mage::log(
                        '['.$e->getFile().':'.$e->getLine().'] '
                        . '(Customer ID: '.$customerArr['id'].' Email: '
                        . Mage::registry('last_email') . ') '
                        . $e->getMessage(),
                        null,
                        'customers_import.log'
                    );
                }
            }

            echo 'Customers import has been finished.' . "\n\n";
        } catch (Exception $e) {
            Mage::log(
                '['.$e->getFile().':'.$e->getLine().'] '
                . $e->getMessage(),
                null,
                'customers_import.log'
            );
        }
        unlink(Mage::getBaseDir().self::CSV_URL);

        return $this;
    }

    /**
     * Method create new customer
     *
     * @param array $customerArr customer data to save.
     *
     * @return Mage_Customer_Model_Customer
     */
    public static function createCustomer($customerArr)
    {
        $update = 1;
        $websiteId = Mage::app()->getWebsite()->getId();
        $store = Mage::app()->getStore();
        $password = self::randomStr(8);

        $customerModel = Mage::getModel("customer/customer")
            ->setWebsiteId($websiteId)
            ->setStore($store)
            ->getCollection()
            ->addFieldToFilter('import_custom_id', $customerArr['id'])
            ->load();

        $customerData = $customerModel->getData();

        if (!isset($customerData[0]['entity_id']) && !empty($customerArr['email'])) {
            $customerModel = Mage::getModel("customer/customer");
            $customerModel->setWebsiteId($websiteId);
            $customerModel->setStore($store);
            $customerModel->loadByEmail($customerArr['email']);
            if ($customerModel->getEntityId()) {
                $customerData[0] = $customerModel->getData();
            }
        }

        if (!isset($customerData[0]['entity_id'])) {
            $update = 0;
            $customerModel = Mage::getModel("customer/customer")
                ->setWebsiteId($websiteId)
                ->setStore($store);
        } else {
            $customerModel = Mage::getModel("customer/customer")
                ->load($customerData[0]['entity_id']);
        }

        $email = self::generateEmailForCustomer($customerArr, $websiteId, $store, $customerModel);

        Mage::unregister('last_email');
        Mage::register('last_email', $email);

        if ($update == 0) {
            $customerModel
                ->setFirstname($customerArr['firstname'])
                ->setLastname('-')
                ->setPrefix($customerArr['prefix'])
                ->setMiddlename($customerArr['middlename'])
                ->setSuffix($customerArr['suffix'])
                ->setEmail($email)
                ->setImportCustomId($customerArr['id'])
                ->setPassword($password);
        } else {
            $customerModel
                ->setFirstname($customerArr['firstname'])
                ->setLastname('-')
                ->setPrefix($customerArr['prefix'])
                ->setMiddlename($customerArr['middlename'])
                ->setSuffix($customerArr['suffix']);
        }

        if (empty($customerArr['group_id'])) {
            $customerModel->setGroupId(1);
        } else {
            $customerModel->setGroupId($customerArr['group_id']);
        }

        $customerModel->save();

        return $customerModel;
    }

    /**
     * Generate email for new customer.
     *
     * @param $customerArr
     *
     * @return string
     *
     * @throws Exception
     */
    public static function generateEmailForCustomer(
        $customerArr,
        $websiteId,
        $store,
        $customerModel = null
    ) {
        if (empty($customerArr['email'])) {
            if (!empty($customerArr['billing_fax'])) {
                $email = self::cleanFaxString($customerArr['billing_fax'])
                    . '@metrofax.com';
            } elseif (!empty($customerArr['billing_telephone'])) {
                $email = self::cleanFaxString($customerArr['billing_telephone'])
                    . '@metrofax.com';
            } elseif (!empty($customerArr['firstname'])) {
                $email = self::cleanFirstNameString($customerArr['firstname'])
                    . '@mail.com';
            } else {
                throw new Exception(
                    'Customer with id ' .$customerArr['id'].' dosent have email and fax.'
                );
            }

            if ($customerModel !== null && !$customerModel->getEntityId()) {
                $customerModel = Mage::getModel('customer/customer')
                    ->setWebsiteId($websiteId)
                    ->setStore($store)
                    ->loadByEmail($email);

                $i = 0;
                $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                    'i', 'j','k','l','m','n','o','p','r','s','t','w', 'x');
                while ($customerModel->getEntityId()) {
                    $emailExplode = explode('@', $customerModel->getEmail());
                    $email = $emailExplode[0] . $alphabet[$i] . '@' . $emailExplode[1];
                    $i++;

                    $customerModel = Mage::getModel('customer/customer')
                        ->setWebsiteId($websiteId)
                        ->setStore($store)
                        ->loadByEmail($email);
                }
            }
        } else {
            $email = $customerArr['email'];
        }

        return $email;
    }

    /**
     * Return clean fax string.
     *
     * @param $string
     *
     * @return string
     */
    protected static function cleanFaxString($string)
    {
        return preg_replace('/[^0-9\-]/', '', $string);
    }

    /**
     * Return clean firstname string.
     *
     * @param $string
     *
     * @return string
     */
    protected static function cleanFirstNameString($string)
    {
        $string = str_replace(' ', '', $string);
        $string = strtolower($string);

        return preg_replace('/[^a-z0-9\-]/', '', $string);
    }

    /**
     * Create billing address.
     *
     * @param $customerModel
     * @param $customerArr
     *
     * @return Mage_Customer_Model_Address
     */
    public static function createCustomerBillingAddress($customerModel, $customerArr)
    {

        $addressId = $customerModel->getDefaultBilling();

        if (!$addressId) {
            $addressModel = Mage::getModel("customer/address")
                ->setIsDefaultBilling('1');
        } else {
            $addressModel = Mage::getModel("customer/address")->load($addressId);
            if (!empty($customerArr['billing_country']) &&
                !empty($customerArr['billing_company']) &&
                !empty($customerArr['billing_city']) &&
                !empty($customerArr['billing_postalcode']) &&
                !empty($customerArr['billing_telephone']) &&
                !empty($customerArr['billing_fax']) &&
                !empty($customerArr['billing_street1'])
            ) {
                $addressModel = Mage::getModel("customer/address")
                    ->setIsDefaultBilling('1');
            }
        }

        if (empty($customerArr['billing_country']) || $customerArr['billing_country'] == 'USA') {
            $customerArr['billing_country'] = 'US';
        }

        if (empty($customerArr['billing_firstname'])) {
            $firstname = $customerModel->getFirstname();
        } else {
            $firstname = $customerArr['billing_firstname'];
        }

        $addressModel->setCustomerId($customerModel->getId())
            ->setFirstname($firstname)
            ->setLastname('-')
            ->setCountryId($customerArr['billing_country'])
            ->setMiddlename($customerArr['billing_middlename'])
            ->setSuffix($customerArr['billing_suffix'])
            ->setCompany($customerArr['billing_company'])
            ->setCity($customerArr['billing_city'])
            ->setPostcode($customerArr['billing_postalcode'])
            ->setTelephone($customerArr['billing_telephone'])
            ->setFax($customerArr['billing_fax'])
            ->setPrefix($customerArr['billing_prefix'])
            ->setStreet($customerArr['billing_street1'].' '.$customerArr['billing_street2']);

        if (isset($customerArr['billing_region']) and
            !empty($customerArr['billing_region'])
        ) {
            if ($customerArr['billing_country'] == 'US') {
                $regionModel = Mage::getModel('directory/region')
                    ->load($customerArr['billing_region'], 'code');
                $addressModel->setRegionId($regionModel->getRegionId());
            } else {
                $addressModel->setRegion($customerArr['billing_region']);
            }

        }

        $addressModel->save();

        return $addressModel;
    }

    /**
     * Create shipping address.
     *
     * @param $customerModel
     * @param $customerArr
     *
     * @return Mage_Customer_Model_Address
     */
    public static function createCustomerShippingAddress($customerModel, $customerArr)
    {

        $addressId = $customerModel->getDefaultShipping();

        if (!$addressId) {
            $addressModel = Mage::getModel("customer/address")
                ->setIsDefaultShipping('1');
        } else {
            $addressModel = Mage::getModel("customer/address")->load($addressId);
            if (!empty($customerArr['shipping_country']) &&
                !empty($customerArr['shipping_company']) &&
                !empty($customerArr['shipping_city']) &&
                !empty($customerArr['shipping_postalcode']) &&
                !empty($customerArr['shipping_telephone']) &&
                !empty($customerArr['shipping_fax']) &&
                !empty($customerArr['shipping_street1'])
            ) {
                $addressModel = Mage::getModel("customer/address")
                    ->setIsDefaultShipping('1');
            }
        }

        if (empty($customerArr['shipping_country']) || $customerArr['shipping_country'] == 'USA') {
            $customerArr['billing_coshipping_countryuntry'] = 'US';
        }

        if (empty($customerArr['billing_firstname'])) {
            $firstname = $customerModel->getFirstname();
        } else {
            $firstname = $customerArr['billing_firstname'];
        }

        $addressModel->setCustomerId($customerModel->getId())
            ->setFirstname($firstname)
            ->setLastname('-')
            ->setCountryId($customerArr['shipping_country'])
            ->setMiddlename($customerArr['shipping_middlename'])
            ->setSuffix($customerArr['shipping_suffix'])
            ->setCompany($customerArr['shipping_company'])
            ->setCity($customerArr['shipping_city'])
            ->setPostcode($customerArr['shipping_postalcode'])
            ->setTelephone($customerArr['shipping_telephone'])
            ->setFax($customerArr['shipping_fax'])
            ->setPrefix($customerArr['shipping_prefix'])
            ->setStreet($customerArr['shipping_street1'].' '.$customerArr['shipping_street2']);

        if (isset($customerArr['shipping_region']) &&
            !empty($customerArr['shipping_region'])
        ) {
            if ($customerArr['shipping_country'] == 'US') {
                $regionModel = Mage::getModel('directory/region')
                    ->load($customerArr['shipping_region'], 'code');
                $addressModel->setRegionId($regionModel->getRegionId());
            } else {
                $addressModel->setRegion($customerArr['shipping_region']);
            }
        }
        $addressModel->save();

        return $addressModel;
    }

    /**
     * Generate random string.
     *
     * @param int $length
     *
     * @return string
     */
    protected static function randomStr($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Prepare array with csv from records
     *
     * @return array
     */
    protected function prepareCsvArr()
    {
        $headers = array();
        $csvArr  = array();
        $csv = file_get_contents(Mage::getBaseDir().self::CSV_URL);

        if ($csv != false) {
            $lines = explode("\n", $csv);
            foreach ($lines as $line) {
                $row = str_getcsv($line, '|');
                if (count($headers) == 0) {
                    $headers = $row;
                } elseif (is_array($row)) {
                    array_splice($row, count($headers));
                    $row_trim = array();
                    foreach ($row as $r) {
                        $row_trim[] = trim($r);
                    }
                    if (count($headers) >  0 && count($row_trim) > 0 && count($headers) == count($row_trim)) {
                        $csvArr[] = array_combine($headers, $row_trim);
                    }
                }
            }
        } else {
            Mage::log('Cant load customers csv file.', null, 'customers_import.log');
        }



        return $csvArr;
    }
}

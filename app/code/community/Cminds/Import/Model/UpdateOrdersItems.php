<?php 
class Cminds_Import_Model_OrdersInvoices extends Mage_Core_Model_Abstract
{

    const CSV_URL = './csv/ordersInvoices2.csv';

    /**
     * Magento website id.
     *
     * @var int
     */
    protected $websiteId;

    /**
     * Magento store id
     *
     * @var int
     */
    protected $store;

    /**
     * Quote
     *
     * @var
     */
    protected $quote;

    /**
     * Array with customer data from csv.
     *
     * @var array
     */
    protected $ordersArr = [];

    /**
     * Array with products skus dosent found.
     *
     * @var array
     */

    protected $skusNotFound = [];

    /**
     * Array with customer data from csv.
     *
     * @var array
     */
    protected $orders = [];

    /**
     * Product instance.
     *
     * @var array
     */
    protected $product = null;

    /**
     * Product option instance.
     *
     * @var array
     */
    protected $productOption = [];

    /**
     * Array with invoices data from csv.
     *
     * @var array
     */
    protected $invoicesArr = [];

    /**
     * Array with shipments data from csv.
     *
     * @var array
     */
    protected $shipmentsArr = [];

    /**
     * Missing salesreps.
     *
     * @var array
     */
    protected $missingSealereps = [];

    protected $csv = false;

    public function createOrders()
    {
        try {
            set_time_limit(0);
            ini_set("memory_limit", "-1");
            $this->websiteId = Mage::app()->getWebsite()->getId();
            $this->store =  Mage::app()->getStore();
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

            $i = 1;
            while ($this->ordersArr = $this->prepareCsvArr($i, 300)) {
                foreach ($this->ordersArr as $orderArr) {
                    if (!isset($orderArr['id'])) {
                        continue;
                    }
                    $i++;
                    try {
                      //  $orderArr['product_sku'] = 'SKU';
                       // $this->searchProductBySku($orderArr['product_sku']);
                        if (empty($orderArr['product_qty'])) {
                            $orderArr['product_qty'] = 1;
                        }
                        //$order = $this->checkOrderExist($orderArr);
                        /*if ($order->getId()) {
                            $this->orders[] = $order->getId();
                            if (!empty($orderArr['product_sku'])) {
                                if (empty($orderArr['shipping_tracking_number']) &&
                                    empty($orderArr['invoice_id'])
                                ) {
                                    $this->assignNextProduct($orderArr, $order);
                                }
                                if (!empty($orderArr['shipping_tracking_number']) ||
                                    !empty($orderArr['invoice_id'])
                                ) {
                                    $product = $this->product;
                                    $item = $this->getItemByProductIdOrderId($product, $order);
                                    $itemData = $item->getData();
                                    if (!empty($itemData[0]['qty_ordered']) &&
                                        ($orderArr['product_qty'] > $itemData[0]['qty_ordered'])
                                    ) {
                                        $orderItem = Mage::getModel('sales/order_item')
                                            ->load($itemData[0]['item_id']);
                                        $qty =
                                            (int) $orderArr['product_qty'] - (int) $itemData[0]['qty_ordered'];
                                        $this->updatedItemQty($qty, $orderItem);
                                    } elseif (isset($itemData[0]['item_id']) or
                                        empty($itemData[0]['item_id'])
                                    ) {
                                        $this->assignNextProduct($orderArr, $order);
                                    }
                                }
                            }
                        } else {
                            $admin = Mage::getModel("admin/user")
                                ->getCollection()
                                ->addFieldToFilter('import_id', $orderArr['order_salesrep'])
                                ->load();
                            if (count($admin) == 0) {
                                $this->missingSealereps[] = $orderArr['order_salesrep'];
                            }
                            $adminData = $admin->getData();
                            Mage::app()->getFrontController()->getRequest()
                                ->setPost('salesrep_rep_id', $adminData[0]['user_id']);

                            $this->createQuote($orderArr);

                            $service = Mage::getModel('sales/service_quote', $this->quote);
                            $service->submitAll();

                            $order = $service->getOrder();
                            $order->setData('import_id', $orderArr['id']);
                            $order->setData('po_number', $orderArr['po_number']);
                            $order->setData('email_follower', $orderArr['email_follower']);
                            $order->setData('shipping_time', $orderArr['shipping_time']);
                            $order->save();

                            $this->orders[] = $order->getId();
                        }*/
                        if (!empty($orderArr['shipping_tracking_number'])) {
                            $this->shipmentsArr[$orderArr['shipping_tracking_number']][] = $orderArr;
                        }
                        if (!empty($orderArr['invoice_id'])) {
                            $this->invoicesArr[$orderArr['invoice_id']][] = $orderArr;
                        }
                       // $order->collectTotals()->save();
                       // $this->addNoteToOrder($order, $orderArr);
                    } catch (Exception $e) {
                        Mage::log(
                            '['.$e->getFile().':'.$e->getLine().'] (Order id: '.$orderArr['id'].') '.$e->getMessage(),
                            null,
                            'cminds_warnings.log'
                        );
                    }
                }
            }
            $this->createShippments();
            $this->createInvoices();
            $this->updateOrdersStatus();
            //$this->sendMail();
        } catch (Exception $e) {
            Mage::log('['.$e->getFile().':'.$e->getLine().'] '.$e->getMessage(), null, 'cminds_exceptions.log');
        }

        //unlink(self::CSV_URL);
    }

    protected function searchProductBySku($sku) {
        $this->product = Mage::getModel('catalog/product')
            ->loadByAttribute('sku',$sku);
        if(!$this->product) {
            $product_skus = Mage::getModel('import/product_sku')->getCollection();
            $product_skus->getSelect()->where('sku = ?',[$sku]);
            $product_skus_data = $product_skus->getData();
            if(isset($product_skus_data[0]['product_id'])) {
                $this->productOption = explode(',',$product_skus_data[0]['options_skus']);

                $this->product = Mage::getModel('catalog/product')
                    ->load($product_skus_data[0]['product_id']);
            } else {
                $sku_default = Mage::getStoreConfig('order_import/general/default_product_sku');
                $this->product = Mage::getModel('catalog/product')
                    ->loadByAttribute('sku',$sku_default);

                if(!empty($sku))
                    $this->skusNotFound[] = $sku;
            }
        }

        return $sku;
    }

    public function sendMail(){

        if(count($this->skusNotFound) > 0) {
            //$name = Mage::getStoreConfig('trans_email/ident_general/name');
            //$email = Mage::getStoreConfig('trans_email/ident_general/email');
            $name = 'Rafal';
            $email = 'rafal.andryanczyk@gmail.com';
            $body = '';
            if (count($this->skusNotFound) > 0) {
                $body .= 'SKUS not found: <br/><br/>'.implode('<br/>',$this->skusNotFound).'<br/><br/>';
            }
            if (count($this->missingSealereps) > 0) {
                $body .= 'Salesreps not found: <br/><br/>'.implode('<br/>',$this->missingSealereps);
            }

            $mail = Mage::getModel('core/email');
            $mail->setToName($name);
            $mail->setToEmail($email);
            $mail->setBody($body);
            $mail->setSubject('Liton order report');
            $mail->setFromEmail($email);
            $mail->setFromName($name);
            $mail->setType('html');

            try {
                $mail->send();
            }
            catch (Exception $e) {
                Mage::log('Cant send mail',null,'cminds_exceptions.log');
            }

            if (count($this->skusNotFound) > 0) {
                Mage::log("SKUS not found: \n" . implode("\n", $this->skusNotFound), null, 'cminds_import.log');
            }
            if (count($this->missingSealereps) > 0) {
                Mage::log('Salesreps not found: <br/><br/>' . implode('<br/>', $this->missingSealereps), null, 'cminds_import.log');
            }

        }


    }

    protected function searchProductBySkuStatic($sku) {
        $product = Mage::getModel('catalog/product')
            ->loadByAttribute('sku',$sku);

        if(!$product) {
            $product_skus = Mage::getModel('import/product_sku')->getCollection();
            $product_skus->getSelect()->where('sku = ?',[$sku]);
            $product_skus_data = $product_skus->getData();

            if(isset($product_skus_data[0]['product_id'])) {
                $product = Mage::getModel('catalog/product')
                    ->load($product_skus_data[0]['product_id']);
            } else {
                $sku = Mage::getStoreConfig('order_import/general/default_product_sku');
                $product = Mage::getModel('catalog/product')
                    ->loadByAttribute('sku',$sku);
            }
        }

        return $product;


    }

    protected function updateOrdersStatus() {
        foreach($this->orders as $orderId) {
            $order = Mage::getModel('sales/order')
                ->load($orderId);

            $fullyShipped = true;

            foreach ($order->getAllItems() as $item) {
                if (($item->getQtyToShip()>0 && !$item->getIsVirtual()
                    && !$item->getLockedDoShip()) || $item->getQtyToInvoiced() > 0)
                {
                    $fullyShipped = false;
                }
            }

            if($fullyShipped) {
                $state = Mage_Sales_Model_Order::STATE_COMPLETE;

                if($state) {
                    $status = $order->getConfig()->getStateDefaultStatus($state);
                    $order->setStatus($status);
                }
            }


            $order->save();
        }
    }

    protected function addNoteToOrder($order,$orderArr) {

        $note = '';

        if(!empty($orderArr['order_note_reporter'])) {
            $note .= 'Reporter: ' .$orderArr['order_note_reporter'].' ';
        }
        if(!empty($orderArr['order_note_created_at'])) {
            $note .= 'Created At: ' .$orderArr['order_note_created_at'].' ';
        }
        if(!empty($orderArr['order_note'])) {
            $note .= $orderArr['order_note'].' ';
        }
        if(!empty($note)) {
            $order->addStatusHistoryComment($note);
        }
        $order->save();
    }


    protected function createShippments() {

       // $transaction = Mage::getModel('core/resource_transaction');

        foreach($this->shipmentsArr AS $tracking_number => $items) {
            try {

                $order = Mage::getModel('import/order')->load($items[0]['id'], 'import_id');
                $qty_arr = [];

                if (empty($order->getId()) || $order->getId() < 15113) {
                    continue;
                }

                foreach($items as $item) {

                    $product =  $this->searchProductBySkuStatic($item['product_sku']);

                    $itemCollection = Mage::getModel('sales/order_item')->getCollection();
                    $itemCollection->getSelect()->where(
                        'product_id = ?',
                        $product->getId())->where('order_id = ?',$order->getId()
                    );

                    $itemData = $itemCollection->getData();
                    if(
                        $item['product_qty'] == $itemData[0]['qty_shipped'] and
                        $itemData[0]['qty_ordered'] == $item['product_qty']
                    ) {
                        continue;
                    } elseif($item['product_qty'] > $itemData[0]['qty_shipped']) {
                        $qty_arr[$itemData[0]['item_id']] =
                            $item['product_qty'] - $itemData[0]['qty_shipped'];
                    } else {
                        $qty_arr[$itemData[0]['item_id']] =
                            (int) $item['product_qty'];
                    }
                }
                if(count($qty_arr) > 0) {
                    $shipment = $order->prepareShipment($qty_arr);
                    $shipment->register();

                    foreach($shipment->getAllItems() AS $item) {

                        $orderItem = Mage::getModel('sales/order_item')
                            ->load($item->getOrderItemId());
                        $orderItem->setQtyShipped($item->getQty() + $orderItem->getQtyShipped());
                        $orderItem->save();
                    }
                    $shipment->save();
                  //  $transaction->addObject($shipment);
                    $sh = Mage::getModel('sales/order_shipment_track')
                        ->setShipment($shipment)
                        ->setData('title', 'Title')
                        ->setData('number', $tracking_number)
                        ->setData('order_id', $shipment->getData('order_id'));

                   // $transaction->addObject($sh);
                    $sh->save();
                }

                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $status = $order->getConfig()->getStateDefaultStatus($state);
                $order->setStatus($status);
                $order->save();
            } catch (Exception $e) {
                Mage::log(
                    '['.$e->getFile().':'.$e->getLine().'] (Order id: '.$items[0]['id'].') '.$e->getMessage(),
                    null,
                    'cminds_shippings_warnings.log'
                );
            }
        }

       // $transaction->save();

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function createInvoices() {

       // $transaction = Mage::getModel('core/resource_transaction');

        foreach($this->invoicesArr AS $invoice_id => $items) {
            try {
                $order = Mage::getModel('import/order')->load($items[0]['id'], 'import_id');
                $qty_arr = [];

               if (empty($order->getId()) || $order->getId() < 15113) {
                   continue;
               }

                foreach($items as $item) {

                    $product =  $this->searchProductBySkuStatic($item['product_sku']);

                    if(empty($product)) {
                        throw new Exception(
                            'Product with SKU dosent exists.'
                        );
                    }

                    $itemCollection = Mage::getModel('sales/order_item')->getCollection();
                    $itemCollection->getSelect()
                        ->where('product_id = ?',$product->getId())
                        ->where('order_id = ?',$order->getId());

                    $itemData = $itemCollection->getData();
                    if(
                        $item['product_qty'] == $itemData[0]['qty_invoiced'] and
                        $itemData[0]['qty_ordered'] == $item['product_qty']
                    ) {
                        continue;
                    } elseif($item['product_qty'] > $itemData[0]['qty_invoiced']) {
                        $qty_arr[$itemData[0]['item_id']] =
                            $item['product_qty'] - $itemData[0]['qty_invoiced'];
                    } else {
                        $qty_arr[$itemData[0]['item_id']] =
                            (int) $item['product_qty'];
                    }

                }

                if(count($qty_arr) > 0) {
                    $invoice = Mage::getModel('import/invoice', $order)
                        ->prepareInvoice($qty_arr);

                    $invoice->register()
                        ->save()
                        ->sendEmail(false)
                        ->setEmailSent(false);

                    /* foreach($invoice->getAllItems() AS $item) {
                         $orderItem = Mage::getModel('sales/order_item')
                             ->load($item->getOrderItemId());
                         $orderItem
                             ->setQtyInvoiced($item->getQty() + $orderItem->getQtyInvoiced());
                         $transaction->addObject($orderItem);
                     }*/

               //     $transaction->addObject($invoice);
                }

                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $status = $order->getConfig()->getStateDefaultStatus($state);
                $order->setStatus($status);
                $order->save();
            //    $transaction->addObject($order);
            } catch (Exception $e) {
                Mage::log(
                    '['.$e->getFile().':'.$e->getLine().'] (Order id: '.$items[0]['id'].') '.$e->getMessage(),
                    null,
                    'cminds_invoices_warnings.log'
                );
            }
        }

       // $transaction->save();

        return true;
    }

    protected function checkOrderExist($orderArr) {
        $order = Mage::getModel('import/order')->load($orderArr['id'], 'import_id');
        return $order;
    }

    protected function createQuote($orderArr) {
        $this->quote = Mage::getModel('sales/quote')->setStoreId($this->store->getId());

        $customer = [];

        if(!empty($orderArr['customer_email']) or !empty($orderArr['customer_id'])) {
            $customer = $this->assignCustomer($orderArr);
        }

        $this->assignProduct($orderArr);
        $this->createBillingAddress($orderArr,$customer);
        $this->createShippingAddress($orderArr,$customer);

        $this->quote->getPayment()
            ->importData(array('method' => 'purchaseorder','po_number' => $orderArr['po_number']));

        $this->quote->collectTotals()->save();

    }

    protected function updatedItemQty($qty,$orderItem)
    {
        $qty = $qty + $orderItem->getQtyOrdered();
        $orderItem->setQtyOrdered($qty);
        $orderItem->save();
    }

    protected function getItemByProductIdOrderId($product,$order) {
        $itemCollection = Mage::getModel('sales/order_item')->getCollection();
        $itemCollection->getSelect()->where('product_id = ?',$product->getId())
            ->where('order_id = ?',$order->getId());

        return $itemCollection;
    }

    protected function assignNextProduct($orderArr,$order) {
        $product =  $this->product;

        if(!$product) {
            throw new Exception(
                'Product with SKU ' .$orderArr['product_sku'].' dosent exists.'
            );
        }

        $productData = $product->getData();

        $product = Mage::getModel('catalog/product')
            ->load($productData['entity_id']);

        $itemCollection = $this->getItemByProductIdOrderId($product,$order);

        $itemData = $itemCollection->getData();

        if(isset($itemData[0]['item_id']) and !empty($itemData[0]['item_id'])) {
            $orderItem = Mage::getModel('sales/order_item')
               ->load($itemData[0]['item_id']);
            $this->updatedItemQty($orderArr['product_qty'],$orderItem);
        } else {
            $qty = $orderArr['product_qty'];
            $quote = Mage::getModel('sales/quote')
                ->getCollection()
                ->addFieldToFilter("entity_id", $order->getQuoteId())
                ->getFirstItem();

            if (!$quote->getId()) {
                $quote = Mage::getModel('sales/convert_order')
                    ->toQuote($order)
                    ->setIsActive(false)
                    ->setReservedOrderId($order->getIncrementId())
                    ->save();
            }

            $requestTax = Mage::getSingleton('tax/calculation')
                ->getRateRequest()
                ->setProductClassId($product->getTaxClassId());

            $taxRate = Mage::getSingleton('tax/calculation')
                ->getRate($requestTax);

            $totalAmountInclTax = $product->getPrice() + ($product->getPrice()/100) * $taxRate;

            $quoteItem = Mage::getModel('sales/quote_item')
                ->setProduct($product)
                ->setPrice($product->getPrice())
                ->setOriginalPrice($product->getPrice())
                ->setSubTotal($product->getPrice() * $qty)
                ->setRowTotal($product->getPrice() * $qty)
                ->setRowTotalInclTax($totalAmountInclTax * $qty)
                ->setBaseRowTotalInclTax($totalAmountInclTax * $qty)
                ->setQuote($quote)
                ->setQty($qty);

            $option_tmp = null;

            if(!empty($orderArr['discounts_amount'])) {
                $quoteItem
                    ->setDiscountAmount($orderArr['discounts_amount']);
            }

            if(
                !empty($orderArr['product_price']) and
                $product->getPrice() != $orderArr['product_price']
            ) {
                $quoteItem
                    ->setCustomPrice($orderArr['product_price'])
                    ->setOriginalCustomPrice($orderArr['product_price']);
            }

            $quoteItem
                ->save();

            $orderItem = Mage::getModel('sales/convert_quote')
                ->itemToOrderItem($quoteItem)
                ->setOrderID($order->getId());

            if(count($this->productOption) > 0) {
                $option_arr = array(
                    'info_buyRequest' =>
                        array ('options' => array(

                        )
                    ),
                    array ('options' => array(

                        )
                    )
                );
                foreach($this->productOption as $productOption) {
                    foreach ($product->getOptions() as $option) {
                        foreach ($option->getValues() as $value) {
                            if ($value->getSku() == $productOption) {
                                $price =
                                    Mage::helper('core')->currency($qty * $value->getPrice(), true, false);
                                $optionData = $option->getData();
                                $option_arr['info_buyRequest']['options'][$value->getOptionId()]
                                    = $value->getOptionTypeId();
                                $option_arr['options'][] =
                                    array (
                                        'label' =>  $optionData['default_title'],
                                        'value' =>  $qty.' x '.$value->getTitle().' - '.$price,
                                        'option_id' => $optionData['option_id'],
                                        'option_type' => $optionData['type'],
                                        'option_value' => $value->getOptionTypeId(),
                                        'custom_view' => false,
                                    );
                            }
                        }
                    }
                }
                $orderItem->setProductOptions($option_arr);
            }

            $orderItem
                ->save($order->getId());
        }

        $order->collectTotals()->save();

        return $this;
    }

    protected function assignCustomer($orderArr) {

        if(!empty($orderArr['customer_id'])) {
            $customer = Mage::getModel('customer/customer')
                ->getCollection()
                ->addFieldToFilter('import_custom_id',$orderArr['customer_id'])
                ->load();
            $customerData = $customer->getData();
            if (isset($customerData[0]['entity_id']) && !empty($customerData[0]['entity_id'])) {
                $customer = Mage::getModel('customer/customer')
                    ->load($customerData[0]['entity_id']);
            } else {
                $customer = $this->createCustomer($orderArr);
            }
        }

        if(!empty($customer)) {
            $this->quote->setCustomer($customer);
        }

        return $customer;
    }

    /**
     * Method create new customer.
     *
     * @param $orderArr
     *
     * @return Mage_Customer_Model_Customer
     *
     * @throws Exception
     */
    protected function createCustomer($orderArr) {
        $customerArr = $this->prepareCustomerArray($orderArr);
        $customerModel = Cminds_Import_Model_Customers::createCustomer($customerArr);
        Cminds_Import_Model_Customers::createCustomerBillingAddress($customerModel, $customerArr);
        Cminds_Import_Model_Customers::createCustomerShippingAddress($customerModel, $customerArr);

        return $customerModel;
    }

    /**
     * Prepare array which will use in create customer methods.
     *
     * @param $orderArr
     *
     * @return array
     */
    protected function prepareCustomerArray($orderArr) {
        $customerArr = [];
        $customerArr['id'] = $orderArr['customer_id'];
        $customerArr['firstname'] = $orderArr['customer_type'];

        $customerArr['billing_country'] = $orderArr['billing_address_country'];
        $customerArr['billing_city'] = $orderArr['billing_address_city'];
        $customerArr['billing_postalcode'] = $orderArr['billing_address_postalcode'];
        $customerArr['billing_telephone'] = $orderArr['billing_address_phone'];
        $customerArr['billing_street1'] = $orderArr['billing_address_street'];
        $customerArr['billing_street2'] = '';
        $customerArr['billing_region'] = $orderArr['billing_address_state'];

        $customerArr['shipping_country'] = $orderArr['shipping_address_country'];
        $customerArr['shipping_city'] = $orderArr['shipping_address_city'];
        $customerArr['shipping_postalcode'] = $orderArr['shipping_address_postalcode'];
        $customerArr['shipping_telephone'] = $orderArr['shipping_address_phone'];
        $customerArr['shipping_street1'] = $orderArr['shipping_address_street'];
        $customerArr['shipping_street2'] = '';
        $customerArr['shipping_region'] = $orderArr['shipping_address_state'];

        return $customerArr;
    }

    /**
     * Generate email for new customer.
     *
     * @param $orderArr
     *
     * @return string
     */
    protected function generateEmail($orderArr) {
        return 'customer_'.$orderArr['customer_id'].'@mail.com';
    }

    protected function assignProduct($orderArr) {
        $product =  $this->product;

        if(!$product) {
            throw new Exception(
                'Product with SKU ' .$orderArr['product_sku'].' dosent exists.'
            );
        }

        $productData = $product->getData();

        $productById = Mage::getModel('catalog/product')
            ->load($productData['entity_id']);

        if(count($this->productOption) > 0) {
            $option_arr = array(
                'options' => array(

                )
            );
            foreach($this->productOption as $productOption) {
                foreach ($productById->getOptions() as $option) {
                    foreach ($option->getValues() as $value) {
                        if ($value->getSku() == $productOption) {
                            $option_arr['options'][$value->getOptionId()] = $value->getOptionTypeId();
                        }

                    }
                }
            }

            $request = new Varien_Object();
            $request->setData($option_arr);

            $quoteItem = $this->quote->addProduct($productById,$request);

        } else {
            $quoteItem = $this->quote->addProduct($productById,$orderArr['product_qty']);
        }

        if(is_string($quoteItem)) {
            throw new Exception(
                'Product with SKU ' .$orderArr['product_sku'].': '.$quoteItem
            );
        }

        $quoteItem->setQty($orderArr['product_qty']);

        if(!empty($orderArr['discounts_amount'])) {
            $quoteItem
                ->setDiscountAmount($orderArr['discounts_amount']);
        }

        if(
            !empty($orderArr['product_price']) and
            $product->getPrice() != $orderArr['product_price']
        ) {
            $quoteItem
                ->setCustomPrice($orderArr['product_price'])
                ->setOriginalCustomPrice($orderArr['product_price']);
        }

        return $this;
    }

    protected function createBillingAddress(&$orderArr,$customer) {
        $billingAddress  = $this->quote->getBillingAddress();
        $customer = Mage::getModel('customer/customer')->load($customer->getId());
        $customerBillingAddress = [];

        if(count($customer) > 0 and !empty($customer) and $customer->getPrimaryBillingAddress()) {
            $customerBillingAddress = $customer->getPrimaryBillingAddress()->getData();
        }

        if(count($customerBillingAddress) > 0) {
            if(
                empty($orderArr['billing_address_first_name']) and
                isset($customerBillingAddress['firstname'])
            ) {
                $orderArr['billing_address_first_name'] =
                    $customerBillingAddress['firstname'];
            }
            if(
                empty($orderArr['billing_address_last_name']) and
                isset($customerBillingAddress['lastname'])
            ) {
                $orderArr['billing_address_last_name'] =
                    $customerBillingAddress['lastname'];
            }
            if(
                empty($orderArr['billing_address_country']) and
                isset($customerBillingAddress['country_id'])
            ) {
                $orderArr['billing_address_country'] =
                    $customerBillingAddress['country_id'];
            }
            if(
                empty($orderArr['billing_address_city']) and
                isset($customerBillingAddress['city'])
            ) {
                $orderArr['billing_address_city'] =
                    $customerBillingAddress['city'];
            }
            if(
                empty($orderArr['billing_address_postalcode']) and
                isset($customerBillingAddress['postcode'])
            ) {
                $orderArr['billing_address_postalcode'] =
                    $customerBillingAddress['postcode'];
            }
            if(
                empty($orderArr['billing_address_phone']) and
                isset($customerBillingAddress['telephone'])
            ) {
                $orderArr['billing_address_phone'] =
                    $customerBillingAddress['telephone'];
            }
            if(
                empty($orderArr['billing_address_street']) and
                isset($customerBillingAddress['street'])
            ) {
                $orderArr['billing_address_street'] =
                    $customerBillingAddress['street'];
            }
            if(
                empty($orderArr['billing_address_state']) and
                isset($customerBillingAddress['region'])
            ) {
                $orderArr['billing_address_state'] =
                    $customerBillingAddress['region'];
            }
        }

        if(empty($orderArr['billing_address_first_name'])) {
            $orderArr['billing_address_first_name'] = 'Dummy First Name';
        }
        if(empty($orderArr['billing_address_last_name'])) {
            $orderArr['billing_address_last_name'] = 'Dummy Last Name';
        }
        if(empty($orderArr['billing_address_country'])) {
           $orderArr['billing_address_country'] = 'USA';
        }
        if(empty($orderArr['billing_address_city'])) {
            $orderArr['billing_address_city'] =  'Dummy City';
        }
        if(empty($orderArr['billing_address_phone'])) {
            $orderArr['billing_address_phone'] = '111 222 333';
        }
        if(empty($orderArr['billing_address_street'])) {
            $orderArr['billing_address_street'] =  'Dummy Street';
        }

        $billingAddress
            ->setFirstname($orderArr['billing_address_first_name'])
            ->setLastname($orderArr['billing_address_last_name'])
            ->setCountryId($orderArr['billing_address_country'])
            ->setCity($orderArr['billing_address_city'])
            ->setPostcode($orderArr['billing_address_postalcode'])
            ->setTelephone($orderArr['billing_address_phone'])
            ->setStreet($orderArr['billing_address_street']);

        if(
            isset($orderArr['billing_address_state']) and
            !empty($orderArr['billing_address_state'])
        ) {

            if($orderArr['billing_address_country'] == 'US') {
                $regionModel = Mage::getModel('directory/region')
                    ->load($orderArr['billing_address_state'],'default_name');
                $billingAddress->setRegionId($regionModel->getRegionId());
            } else {
                $billingAddress->setRegion($orderArr['billing_address_state']);
            }

        }
    }

    protected function createShippingAddress($orderArr,$customer) {
        $shippingAddress  = $this->quote->getShippingAddress();

        $customerShippingAddress = [];

        if(count($customer) > 0 and !empty($customer) and $customer->getPrimaryShippingAddress()) {
            $customerShippingAddress = $customer->getPrimaryShippingAddress()->getData();
        }

        if(count($customerShippingAddress) > 0) {
            if(
                empty($orderArr['shipping_address_first_name']) and
                isset($customerShippingAddress['firstname'])
            ) {
                $orderArr['shipping_address_first_name'] =
                    $customerShippingAddress['firstname'];
            }
            if(
                empty($orderArr['shipping_address_last_name']) and
                isset($customerShippingAddress['lastname'])
            ) {
                $orderArr['shipping_address_last_name'] =
                    $customerShippingAddress['lastname'];
            }
            if(
                empty($orderArr['shipping_address_country']) and
                isset($customerShippingAddress['country_id'])
            ) {
                $orderArr['shipping_address_country'] =
                    $customerShippingAddress['country_id'];
            }
            if(
                empty($orderArr['shipping_address_city']) and
                isset($customerShippingAddress['city'])
            ) {
                $orderArr['shipping_address_city'] =
                    $customerShippingAddress['city'];
            }
            if(
                empty($orderArr['shipping_address_postalcode']) and
                isset($customerShippingAddress['postcode'])
            ) {
                $orderArr['shipping_address_postalcode'] =
                    $customerShippingAddress['postcode'];
            }
            if(
                empty($orderArr['shipping_address_phone']) and
                isset($customerShippingAddress['telephone'])
            ) {
                $orderArr['shipping_address_phone'] =
                    $customerShippingAddress['telephone'];
            }
            if(
                empty($orderArr['shipping_address_street']) and
                isset($customerShippingAddress['street'])
            ) {
                $orderArr['shipping_address_street'] =
                    $customerShippingAddress['street'];
            }
            if(
                empty($orderArr['shipping_address_state']) and
                isset($customerShippingAddress['region'])
            ) {
                $orderArr['shipping_address_state'] =
                    $customerShippingAddress['region'];
            }
        }

        if(empty($orderArr['shipping_address_first_name'])) {
            $orderArr['shipping_address_first_name'] =
                $orderArr['billing_address_first_name'];
        }

        if(empty($orderArr['shipping_address_last_name'])) {
            $orderArr['shipping_address_last_name'] =
                $orderArr['billing_address_last_name'];
        }
        if(empty($orderArr['shipping_address_country'])) {
            $orderArr['shipping_address_country'] =
                $orderArr['billing_address_country'];
        }
        if(empty($orderArr['shipping_address_city'])) {
            $orderArr['shipping_address_city'] =
                $orderArr['billing_address_last_name'];
        }
        if(empty($orderArr['shipping_address_postalcode'])) {
            $orderArr['shipping_address_postalcode'] =
                $orderArr['billing_address_city'];
        }
        if(empty($orderArr['shipping_address_phone'])) {
            $orderArr['shipping_address_phone'] =
                $orderArr['billing_address_phone'];
        }
        if(empty($orderArr['shipping_address_street'])) {
            $orderArr['shipping_address_street'] =
                $orderArr['billing_address_street'];
        }
        if(empty($orderArr['shipping_address_state'])) {
            $orderArr['shipping_address_state'] =
                $orderArr['billing_address_state'];
        }

        $shippingAddress
            ->setFirstname($orderArr['shipping_address_first_name'])
            ->setLastname($orderArr['shipping_address_last_name'])
            ->setCountryId($orderArr['shipping_address_country'])
            ->setCity($orderArr['shipping_address_city'])
            ->setPostcode($orderArr['shipping_address_postalcode'])
            ->setTelephone($orderArr['shipping_address_phone'])
            ->setStreet($orderArr['shipping_address_street']);

        if(
            isset($orderArr['shipping_address_state']) and
            !empty($orderArr['shipping_address_state'])
        ) {

            if($orderArr['shipping_address_country'] == 'US') {
                $regionModel = Mage::getModel('directory/region')
                    ->load($orderArr['shipping_address_state'],'default_name');
                $shippingAddress->setRegionId($regionModel->getRegionId());
            } else {
                $shippingAddress->setRegion($orderArr['shipping_address_state']);
            }

            if(empty($orderArr['shipping_method'])) $shippingMethod = 'flatrate_flatrate';
            else $shippingMethod = $orderArr['shipping_method'];

            // Collect Rates and Set Shipping & Payment Method
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod($shippingMethod);
        }
    }

    /**
     * Prepare array with csv from records
     *
     * @return array
     */
    protected function prepareCsvArr($offset = 1, $limit = 300) {
        $headers = [];
        $csvArr  = [];

        if (!$this->csv) {
            $this->csv = file_get_contents(self::CSV_URL);
        }

        $lines = explode("\n", $this->csv);
        $row = str_getcsv($lines[0],'|');
        $headers = $row;
        $lines_slice = array_slice($lines,$offset,$limit);

        if(count($lines_slice) == 0) {
            return false;
        }

        unset($lines);

        foreach ($lines_slice as $line) {
            $row = str_getcsv($line,'|');
            if(is_array($row)) {
                array_splice($row, count($headers));
                $row_trim = [];
                foreach($row as $r) {
                    $row_trim[] = trim($r);
                }
                if ((count($headers) >  0 && count($row_trim) > 0) && count($headers) == count($row_trim)) {
                    $csvArr[] = array_combine($headers, $row_trim);
                }
            }
        }

        unset($lines_slice);

        return $csvArr;
    }

}
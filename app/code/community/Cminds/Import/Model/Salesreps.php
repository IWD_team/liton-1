<?php 
class Cminds_Import_Model_Salesreps extends Mage_Core_Model_Abstract
{

    const CSV_URL = '/csv/salesreps.csv';


    /**
     * Array with salesreps data from csv.
     *
     * @var array
     */
    protected $salesrepsArr = array();

    /**
     * Magento website id.
     *
     * @var int
     */
    protected $websiteId;

    /**
     * Magento store id
     *
     * @var int
     */
    protected $store;

    protected $levels =  array(
            0 => 1,
            1 => 2,
            2 => 3
    );

    /**
     * Create customers
     *
     * @return $this
     */
    public function createSalesreps()
    {
        set_time_limit(0);
        try {
            $this->websiteId = Mage::app()->getWebsite()->getId();
            $this->store     = Mage::app()->getStore();
            $this->salesrepsArr = $this->prepareCsvArr();
            $salesrepsCount = count($this->salesrepsArr);

            echo 'Starting sales representatives import.' . "\n\n";
            echo '1. Importing sales representatives.' . "\n";

            $i = 1;
            foreach ($this->salesrepsArr as $salesrepsArr) {
                try {
                    if ((int)$salesrepsArr['level'] !== 4) {
                        $this->createSalesrep($salesrepsArr);
                    }

                    echo 'Sales representative processed [' . $i . '/'
                        . $salesrepsCount . ']' . "\n";
                    $i++;
                } catch (Exception $e) {
                    Mage::log('['.$e->getFile().':'.$e->getLine().'] '.$salesrepsArr['unique_id'].' '.$e->getMessage(),null,'cminds_salesreps.log');
                }
            }

            echo '2. Updating sales representatives.' . "\n";
            $i = 1;
            foreach ($this->salesrepsArr as $salesrepsArr) {
                try {
                    if ($salesrepsArr['level'] != 4) {
                        $this->updatePositions($salesrepsArr);
                    } else {
                        $this->assignUser($salesrepsArr);
                    }

                    echo 'Sales representative processed [' . $i . '/'
                        . $salesrepsCount . ']' . "\n";
                    $i++;
                } catch (Exception $e) {
                    Mage::log('['.$e->getFile().':'.$e->getLine().'] '.$salesrepsArr['unique_id'].' '.$e->getMessage(),null,'cminds_salesreps_positions.log');
                }
            }

            echo 'Sales representatives import has been finished.' . "\n\n";
        } catch (Exception $e) {
            Mage::log('['.$e->getFile().':'.$e->getLine().'] '.$e->getMessage(),null,'cminds_salesreps_exceptions.log');
        }

        unlink(Mage::getBaseDir().self::CSV_URL);

        return $this;
    }

    /**
     * @param array $salesrepArr salesrep data to save
     *
     * @return false|Mage_Core_Model_Abstract
     * @throws Exception
     */
    protected function assignUser($salesrepArr) {

        $salesrepModelParent = Mage::getModel("admin/user")
            ->getCollection()
            ->addFieldToFilter('import_id',$salesrepArr['parent_id'])
            ->load();

        $customerModel = Mage::getModel("customer/customer")
            ->setWebsiteId($this->websiteId)
            ->setStore($this->store)
            ->getCollection()
            ->addFieldToFilter('import_custom_id',$salesrepArr['unique_id'])
            ->load();

        $salesrepParentData = $salesrepModelParent->getData();
        $customerData       = $customerModel->getData();

        if(!empty($salesrepParentData[0]['user_id']) and !empty($customerData[0]['entity_id'])) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $table = Mage::getSingleton('core/resource')->getTableName('customer/entity');

            $write->query("UPDATE {$table} SET salesrep_rep_id = '". intval($salesrepParentData[0]['user_id']) ."' WHERE entity_id = ". $customerData[0]['entity_id'] .";");
        } else {
            return false;
        }

        return $customerModel;
    }

    /**
     * @param array $salesrepArr salesrep data to save
     *
     * @return false|Mage_Core_Model_Abstract
     * @throws Exception
     */
    protected function updatePositions($salesrepArr) {

        if (!empty($salesrepArr['parent_id']) && !empty($salesrepArr['unique_id'])) {
            $salesrepModelParent = Mage::getModel("admin/user")
                ->getCollection()
                ->addFieldToFilter('import_id',$salesrepArr['parent_id'])
                ->load();

            $salesrepModel = Mage::getModel("admin/user")
                ->getCollection()
                ->addFieldToFilter('import_id',$salesrepArr['unique_id'])
                ->load();

            $salesrepParentData = $salesrepModelParent->getData();
            $salesrepData       = $salesrepModel->getData();

            if(!empty($salesrepParentData[0]['user_id']) && !empty($salesrepData[0]['salesrep_position_id'])) {
                $positionModel = Mage::getModel("cminds_positions/position")
                    ->load($salesrepData[0]['salesrep_position_id']);
                $positionModel->setParentId($salesrepParentData[0]['salesrep_position_id'])
                    ->save();
            } else {
                return false;
            }

            return $salesrepModel;
        }

    }

    /**
     * @param array $salesrepArr salesrep data to save
     *
     * @return false|Mage_Core_Model_Abstract
     * @throws Exception
     */
    protected function createSalesrep($salesrepArr) {

        $password = 'Password1234!';

        $this->generateEmail($salesrepArr);

        $salesrepModel = Mage::getModel("admin/user")
            ->getCollection()
            ->addFieldToFilter('import_id',$salesrepArr['unique_id'])
            ->load();

        $salesrepData = $salesrepModel->getData();
        $names = explode(' ',$salesrepArr['sales_office_name']);

        if(empty($salesrepData[0]['user_id'])) {
            $positionModel = Mage::getModel("cminds_positions/position")
                ->setName($salesrepArr['sales_office_name'])
                ->setTypeId($this->levels[$salesrepArr['level']])
                ->save();

            $dataToSave = array(
                'username'              => $salesrepArr['email'],
                'firstname'             => $names[0],
                'lastname'              => $names[1],
                'email'                 => $salesrepArr['email'],
                'password'              => $password,
                'is_active'             => 1,
                'is_salesrep'           => 1,
                'parent_import_id'      => $salesrepArr['parent_id'],
                'import_id'             => $salesrepArr['unique_id'],
                'salesrep_position_id'  => $positionModel->getId()
                );

            $salesrepModel = Mage::getModel("admin/user")->setData($dataToSave)
                ->save();

            $salesrepModel->setRoleIds(array(1))
                ->setRoleUserId($salesrepModel->getUserId())
                ->saveRelations();
        } else {
           return false;
        }

        return $salesrepModel;
    }

    protected function generateEmail(&$salesrepArr) {

        if(empty($salesrepArr['email'])) {
            $salesrepArr['email'] = 'salesrep_'.$salesrepArr['unique_id'].'@mail.com';
        }

    }

    function random_str($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Prepare array with csv from records
     *
     * @return array
     */
    protected function prepareCsvArr() {
        $headers = array();
        $headers_new =  array();
        $csvArr  = array();

        $csv = file_get_contents(Mage::getBaseDir().self::CSV_URL);

        if ($csv != false) {

            $lines = explode("\n", $csv);

            foreach ($lines as $line) {
                $row = str_getcsv($line, '|');
                if (count($headers) == 0) {
                    $headers = $row;
                    foreach ($headers as $key => $header) {
                        $headers_new[] = str_replace(' ', '_', strtolower($header));
                    }
                } elseif (is_array($row)) {
                    array_splice($row, count($headers_new));
                    $row_trim = array();
                    foreach ($row as $r) {
                        $row_trim[] = trim($r);
                    }
                    $csvArr[] = array_combine($headers_new, $row_trim);
                }
            }

        } else {
            Mage::log('Cant load salesreps csv file.', null, 'customers_import.log');
        }

        return $csvArr;
    }

}
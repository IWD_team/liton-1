<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Order_Item_Option extends Mage_Core_Model_Abstract
{
    /**
     *
     * @var type $_item Item
     * @var type $_product Product 
     */
    protected $_item;
    protected $_product;

    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('sales/order_item_option');
    }
    /**
     * Sets the item Id
     * @param object $item Item Object
     * @return Cminds_Import_Model_Order_Item_Option
     */
    public function setItem($item)
    {
        $this->setItemId($item->getId());
        $this->_item = $item;
        return $this;
    }
    /**
     * Gets the item object
     * @return object $_item 
     */
    public function getItem()
    {
        return $this->_item;
    }
    /**
     * Sets the product by ID
     * @param object $product Product Object
     * @return Cminds_Import_Model_Order_Item_Option
     */
    public function setProduct($product)
    {
        $this->setProductId($product->getId());
        $this->_product = $product;
        return $this;
    }
    /**
     * Gets the product object
     * @return type $_product
     */
    public function getProduct()
    {
        return $this->_product;
    }
    /**
     * Before saving, it sets the item id
     * @return object
     */
    protected function _beforeSave()
    {
        if ($this->getItem()) {
            $this->setItemId($this->getItem()->getId());
        }
        return parent::_beforeSave();
    }
    /**
     * Sets the items to null
     * @return Cminds_Import_Model_Order_Item_Option
     */
    public function __clone()
    {
        $this->setId(null);
        $this->_item    = null;
        return $this;
    }
}
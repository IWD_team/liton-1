<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Order_Address_Total_Grand extends Cminds_Import_Model_Order_Address_Total_Abstract
{
    /**
     * Fetches the grand total
     * @param Cminds_Import_Model_Order_Address $address
     * @return Cminds_Import_Model_Order_Address_Total_Grand
     */
    public function fetch(Cminds_Import_Model_Order_Address $address)
    {
        $address->addTotal(array(
            'code'=>$this->getCode(),
            'title'=>Mage::helper('sales')->__('Grand Total'),
            'value'=>$address->getGrandTotal(),
            'area'=>'footer',
        ));
        return $this;
    }
}
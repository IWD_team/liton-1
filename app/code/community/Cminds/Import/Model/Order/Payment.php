<?php
/**
 * CreativeMindsSolutions
 */
/**
 * Order payment information
 */
class Cminds_Import_Model_Order_Payment extends Mage_Payment_Model_Info
{
    protected $_eventPrefix = 'sales_order_payment';
    protected $_eventObject = 'payment';

    protected $_order;

    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('import/order_payment');
    }

    /**
     * Declare quote model instance
     *
     * @param   Mage_Sales_Model_Quote $quote
     * @return  Mage_Sales_Model_Quote_Payment
     */
    public function setOrder(Cminds_Import_Model_Order $order)
    {
        $this->_order = $order;
        $this->setOrderId($order->getId());
        return $this;
    }

    /**
     * Retrieve quote model instance
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Import data
     *
     * @param array $data
     * @throws Mage_Core_Exception
     * @return Mage_Sales_Model_Quote_Payment
     */
    public function importData(array $data)
    {
        $data = new Varien_Object($data);
        Mage::dispatchEvent(
            $this->_eventPrefix . '_import_data_before',
            array(
                $this->_eventObject=>$this,
                'input'=>$data,
            )
        );

        $this->setMethod($data->getMethod());

        $method = $this->getMethodInstance();

        if (!$method->isAvailable($this->getOrder())) {
            Mage::throwException(Mage::helper('sales')->__('Requested Payment Method is not available'));
        }

        $method->assignData($data);
        /**
         * validating the payment data
         */
        $method->validate();
        return $this;
    }

    /**
     * Prepare object for save
     *
     * @return Mage_Sales_Model_Quote_Payment
     */
    protected function _beforeSave()
    {
        try {
            $method = $this->getMethodInstance();
        } catch (Mage_Core_Exception $e) {
            return parent::_beforeSave();
        }
        $method->prepareSave();
        if ($this->getOrder()) {
            $this->setQuoteId($this->getOrder()->getId());
        }
        return parent::_beforeSave();
    }
    /**
     * Gets the redirect URL for checkout
     * @return type 
     */
    public function getCheckoutRedirectUrl()
    {
        $method = $this->getMethodInstance();

        return $method ? $method->getCheckoutRedirectUrl() : false;
    }
    /**
     * Gets redirect URL for orders after they are placed
     * @return type 
     */
    public function getOrderPlaceRedirectUrl()
    {
        $method = $this->getMethodInstance();

        return $method ? $method->getOrderPlaceRedirectUrl() : false;
    }
}
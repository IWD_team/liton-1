<?php

class Cminds_Import_TestController extends Mage_Core_Controller_Front_Action
{
    public function testCustomersAction()
    {
        Mage::getModel('import/customers')->createCustomers();
    }

    public function testOrdersAction()
    {
        Mage::getModel('import/orders')->createOrders();
    }

    public function testProductsAction()
    {
        Mage::getModel('import/products')->run();
    }
}

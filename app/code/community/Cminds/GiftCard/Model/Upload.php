<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_Upload extends Mage_Core_Model_Config_Data
{

    public function uploadAndImport(Varien_Object $object)
    {

        $base_path = Mage::getBaseDir('base') . DS . 'media/';
        if ($_FILES['groups']["tmp_name"]['giftcard']['fields']['import']['value'] != '') {
            move_uploaded_file($_FILES['groups']["tmp_name"]['giftcard']['fields']['import']['value'], $base_path . $_FILES['groups']["name"]['giftcard']['fields']['import']['value']);
            $fl = $base_path . 'giftcard.csv';
            $file = fopen($fl, "r");
            while (!feof($file)) {
                $dt[] = fgetcsv($file);
            }

            for ($i = 1; $i < (count($dt)-1); $i++) {
                $model = Mage::getModel('giftcard/giftcard');
                $model->setNumber($dt[$i][1]);
                $model->setOrderId($dt[$i][5]);
                $model->setBal($dt[$i][2]);
                $model->setShipped($dt[$i][4]);
                $model->setCreatedAt($dt[$i][6]);
                $model->save();
            }

            fclose($file);
        }
        $amt = explode(',', $_REQUEST['groups']['giftcard']['fields']['amounts']['value']);
        //echo $amt[0]."---".$amt[1];die;

        if ($_REQUEST['groups']['giftcard']['fields']['ship_card_sku']['value'] != '') {
            for ($i = 0; $i < count($amt); $i++) {
                $product = new Mage_Catalog_Model_Product();

                $sku = $_REQUEST['groups']['giftcard']['fields']['ship_card_sku']['value'] . '_' . $amt[$i];
                if (!Mage::getModel('catalog/product')->getIdBySku($sku)) {
                    $product->setSku($sku);
                    $product->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId());
                    $product->setTypeId('simple');
                    $product->setName($sku);
                    $product->setCategoryIds(array(7)); # some cat id's, my is 7
                    $product->setWebsiteIDs(array(1)); # Website id, my is 1 (default frontend)
                    $product->setDescription('Gift card Full description here');
                    $product->setShortDescription('Gift Card Short description here');
                    $product->setPrice($amt[$i]); # Set some price
                    $product->setWeight(4.0000);
                    $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
                    $product->setStatus(1);
                    $product->setTaxClassId(0); # My default tax class
                    $product->setStockData(array(
                        'is_in_stock' => 1,
                        'qty' => 9999
                    ));

                    $product->save();
                }
            }
        }

        if ($_REQUEST['groups']['giftcard']['fields']['virtual_card_sku']['value'] != '') {
            for ($i = 0; $i < count($amt); $i++) {
                $product = new Mage_Catalog_Model_Product();

                $sku = $_REQUEST['groups']['giftcard']['fields']['virtual_card_sku']['value'] . '_' . $amt[$i];
                if (!Mage::getModel('catalog/product')->getIdBySku($sku)) {
                    $product->setSku($sku);
                    $product->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId());
                    $product->setTypeId('virtual');
                    $product->setName($sku);
                    $product->setWebsiteIDs(array(1)); # Website id, my is 1 (default frontend)
                    $product->setDescription('Gift card Full description here');
                    $product->setShortDescription('Gift Card Short description here');
                    $product->setPrice($amt[$i]); # Set some price
                    $product->setWeight(4.0000);
                    $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
                    $product->setStatus(1);
                    $product->setTaxClassId(0); # My default tax class
                    $product->setStockData(array(
                        'is_in_stock' => 1,
                        'qty' => 9999
                    ));

                    $product->save();
                }
            }
        }
        
        if ($_REQUEST['groups']['giftcard']['fields']['add_card_sku']['value'] != '') {
            for ($i = 0; $i < count($amt); $i++) {
                $product = new Mage_Catalog_Model_Product();

                $sku = $_REQUEST['groups']['giftcard']['fields']['add_card_sku']['value'] . '_' . $amt[$i];
                if (!Mage::getModel('catalog/product')->getIdBySku($sku)) {
                    $product->setSku($sku);
                    $product->setAttributeSetId(Mage::getModel('catalog/product')->getDefaultAttributeSetId());
                    $product->setTypeId('simple');
                    $product->setName($sku);
                    $product->setWebsiteIDs(array(1)); # Website id, my is 1 (default frontend)
                    $product->setDescription('Gift card Full description here');
                    $product->setShortDescription('Gift Card Short description here');
                    $product->setPrice($amt[$i]); # Set some price
                    $product->setWeight(4.0000);
                    $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
                    $product->setStatus(1);
                    $product->setTaxClassId(0); # My default tax class
                    $product->setStockData(array(
                        'is_in_stock' => 1,
                        'qty' => 9999
                    ));

                    $product->save();
                }
            }
        }
        return;
    }
}

<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_Quote_Address_Total_Subtotal extends Mage_Sales_Model_Quote_Address_Total_Subtotal
{
    /**
     * Initializes the items
     * @param array $address Address information
     * @param Mage_Sales_Model_Quote_Address_Item $item
     * @return boolean
     */
    protected function _initItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $quoteItem = $item->getAddress()->getQuote()->getItemById($item->getQuoteItemId());
        } else {
            $quoteItem = $item;
        }
        $product = $quoteItem->getProduct();
        if (!$product->hasCustomerGroupId()) {
            $product->setCustomerGroupId($quoteItem->getQuote()->getCustomerGroupId());
        }

        /**
         * Quote super mode flag meen whot we work with quote without restriction
         */
        if ($item->getQuote()->getIsSuperMode()) {
            if (!$product) {
                return false;
            }
        } else {
            if (!$product || !$product->isVisibleInCatalog()) {
                return false;
            }
        }

        if ($quoteItem->getParentItem() && $quoteItem->isChildrenCalculated()) {
            $finalPrice = $quoteItem->getParentItem()->getProduct()->getPriceModel()->getChildFinalPrice(
                $quoteItem->getParentItem()->getProduct(),
                $quoteItem->getParentItem()->getQty(),
                $quoteItem->getProduct(),
                $quoteItem->getQty()
            );
            if (!$item->getIsGiftcard()) {
                $item->setPrice($finalPrice)
                    ->setBaseOriginalPrice($finalPrice);
            }
            $item->calcRowTotal();
        } elseif (!$quoteItem->getParentItem()) {
            $finalPrice = $product->getFinalPrice($quoteItem->getQty());
            if (!$item->getIsGiftcard()) {
                $item->setPrice($finalPrice)
                    ->setBaseOriginalPrice($finalPrice);
            }
            $item->calcRowTotal();
            if (version_compare('1.4.0', Mage::getVersion(), '<=')) {
                $this->_addAmount($item->getRowTotal());
                $this->_addBaseAmount($item->getBaseRowTotal());
            } else {
                $address->setSubtotal($address->getSubtotal() + $item->getRowTotal());
                $address->setBaseSubtotal($address->getBaseSubtotal() + $item->getBaseRowTotal());
            }
            $address->setTotalQty($address->getTotalQty() + $item->getQty());
        }

        return true;
    }
}

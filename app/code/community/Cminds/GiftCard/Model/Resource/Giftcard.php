<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_Resource_Giftcard extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('giftcard/giftcard', 'giftcard_id');
    }
}

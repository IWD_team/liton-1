<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_Resource_Payment extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('giftcard/payment', 'payment_id');
    }
}

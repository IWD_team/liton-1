<?php

/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_Csv extends Mage_Core_Model_Config_Data
{
    /**
     * Collect quote totals
     * @return Cminds_GiftCard_Model_Quote
     */
    public function _afterSave()
    {
     
        $q= new Cminds_GiftCard_Model_Upload;
        $q->uploadAndImport($this);
    }
}

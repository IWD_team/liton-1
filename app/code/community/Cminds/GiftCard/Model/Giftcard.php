<?php

class Cminds_GiftCard_Model_Giftcard extends Mage_Core_Model_Abstract
{
    /**
     * Event prefix.
     *
     * @var string
     */
    protected $_eventPrefix = 'cminds_giftcard_giftcard';

    public function _construct()
    {
        parent::_construct();
        $this->_init('giftcard/giftcard');
    }

    /**
     * Retrieve gift card model by gift card number.
     *
     * @param int $cardNumber Gift Card number.
     *
     * @return Cminds_GiftCard_Model_Giftcard
     */
    public function getGiftCardByNumber($cardNumber)
    {
        return $this->getCollection()
            ->addFieldToFilter('number', $cardNumber)
            ->getFirstItem();
    }

    /**
     * Authorize transaction to gift card
     *
     * @param object $payment Payment object
     * @param int    $amount  Amount to Authorize
     *
     * @return object
     */
    public function auth($payment, $amount)
    {
        $gcInfo = unserialize($payment->getAdditionalData());
        $cardNum = $gcInfo['gc_number'];

        $card = $this->getGiftCardByNumber($cardNum);
        if ($amount <= $card->getBal()) {
            return $this->capturePayment($cardNum, $amount);
        } else {
            Mage::throwException(
                Mage::helper('giftcard')->__(
                    'Payment authorization transaction has been declined.'
                )
            );
        }
    }

    /**
     * Capture authorized payment of Gift Cards
     *
     * @param int $cardNum Card number to Capture
     * @param int $amount  Amount to capture from card
     *
     * @return Cminds_GiftCard_Model_Giftcard
     */
    public function capturePayment($cardNum, $amount)
    {
        $card = $this->getGiftCardByNumber($cardNum);
        $card->setBal($card->getBal() - $amount);
        $card->save();

        return $this;
    }

    /**
     * Add amount to balance
     *
     * @param int $cardNum Giftcard number
     * @param int $pin     Pin of Gift Card
     * @param int $amount  Amount to add back to balance
     *
     * @return Cminds_GiftCard_Model_Giftcard
     */
    public function addToBalance($cardNum, $pin, $amount)
    {
        $card = $this->getGiftCardByNumber($cardNum, $pin);
        $card->setBal($card->getBal() + $amount);
        $card->save();

        return $this;
    }

    /**
     * Refund the gift card
     *
     * @param object $payment Payment Object
     * @param int    $amount  Amount to refund
     *
     * @return Cminds_GiftCard_Model_Giftcard
     */
    public function refund($payment, $amount = null)
    {
        $gcInfo = unserialize($payment->getAdditionalData());
        $cardNum = $gcInfo['gc_number'];
        $card = $this->getGiftCardByNumber($cardNum);
        $card->setBal($card->getBal() + $amount);
        $card->save();

        return $this;
    }

    /**
     * Remove pending authos on all gift cards
     * If they are past the time limit
     */
    public function removeAuths()
    {
        $mdl = Mage::getModel('giftcard/payment')->getCollection();
        foreach ($mdl as $payment) {
            if ($payment->getOrderId() === null && $payment->getQuoteId()) {
                $curtime = time();
                $paytime = strtotime($payment->getCreatedAt());
                $pastLimit = $curtime - 3600;
                if ($paytime < $pastLimit) {
                    //add funds back on card
                    $giftcard = Mage::getModel('giftcard/giftcard')->load($payment->getGiftcardId());
                    $giftcard->setBal($giftcard->getBal() + $payment->getAmount());
                    $giftcard->save();

                    $payment->delete();
                }
            }
        }
    }

    public function goOnDelivery()
    {
        $quote_coll = Mage::getModel('sales/quote_item')->getCollection()
            ->addFieldToFilter('delivery_date', array('lteq' => date("m/d/y")));

        foreach ($quote_coll->getData() as $quote_data) {
            if ($quote_data['giftcard_delivered'] == 1) {
                continue;
            }
            if ($this->orderIsPayed($quote_data['giftcard_num'])) {
                $sent = Mage::getModel('core/email_template')
                    ->sendTransactional(
                        Mage::getStoreConfig('sales/giftcard/trans_id'),
                        array(
                            'email' => Mage::getStoreConfig('sales/giftcard/send_email'),
                            'name' => Mage::getStoreConfig('sales/giftcard/send_email_name'),
                        ),
                        $quote_data['giftcard_email'],
                        $quote_data['giftcard_email'],
                        array(
                            'number' => $quote_data['giftcard_num'],
                            'amount' => Mage::helper('core')->currency(
                                $quote_data['price_incl_tax'],
                                true,
                                false
                            ),
                            'msg' => $quote_data['giftcard_msg'],
                        )
                    );

                Mage::app()->setCurrentStore(Mage::app()->getDefaultStoreView()->getCode());
                $quote = Mage::getModel('sales/quote')->load($quote_data['quote_id']);
                $quoteItem = $quote->getItemById($quote_data['item_id']);
                $quoteItem->setGiftcardDelivered(1);
                $quoteItem->save();
            }
        }
    }

    public function orderIsPayed($giftCardNumber = null)
    {
        if (!$giftCardNumber) {
            return false;
        }

        $giftCard = Mage::getModel('giftcard/giftcard')->load(
            $giftCardNumber,
            'number'
        );
        $orderId = $giftCard->getOrderId();

        if ($orderId === '0') {
            return true;
        }

        if (!$orderId) {
            return false;
        }

        $order = Mage::getModel('sales/order')->load($orderId);
        if (!$order->getData()) {
            return false;
        }

        return $order->getBaseTotalDue() == 0;
    }

    public function checkRefillPayments()
    {
        if (!$this->getRefillOrderIds()) {
            return true;
        }
        $refillArray = explode(',', $this->getRefillOrderIds());
        foreach ($refillArray as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getBaseTotalDue() != 0) {
                return false;
            }
        }

        return true;
    }

    public function getOrdersByGiftcardNumber($number)
    {
        if ($number) {
            $giftCard = $this->getGiftCardByNumber($number);
        } else {
            return false;
        }

        $giftcardPaymentModel = Mage::getModel('giftcard/payment');

        $cardPaymentCollection = $giftcardPaymentModel
            ->getCollection()
            ->addFieldToFilter(
                'giftcard_id',
                array('eq' => $giftCard->getGiftcardId())
            );

        return $cardPaymentCollection;
    }
}

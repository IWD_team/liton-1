<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('sales_flat_quote_item')} ADD `giftcard_delivered` int(1) DEFAULT NULL;

")->endSetup();

<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Onepage_Payment extends Mage_Checkout_Block_Onepage_Payment
{
    /**
     * Gets they payment methods
     * @return object $html
     */
    protected function _toHtml()
    {
        $this->setTemplate('cminds_giftcard/checkout/onepage/gc-payment.phtml');
        if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
    }
    /**
     * Gets all giftcards for payment
     * @return object $cards
     */
    public function getCards()
    {
        $cards = Mage::getModel('giftcard/payment')->getCollection()
            ->addFieldToFilter('quote_id', $this->getQuote()->getId());
        $cards->getSelect()
            ->join(
                array('gc' => (string)Mage::getConfig()->getTablePrefix().'giftcard_entity'),
                'main_table.giftcard_id = gc.giftcard_id',
                array('gc.number')
            );
            
        return $cards;
    }
}

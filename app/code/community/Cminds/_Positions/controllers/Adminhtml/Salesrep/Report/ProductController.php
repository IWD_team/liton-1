<?php
class Cminds_Positions_Adminhtml_Salesrep_Report_ProductController extends Mage_Adminhtml_Controller_Report_Abstract {

    public function indexAction(){
        $this->_title($this->__('Reports'))
            ->_title($this->__('Products'))
            ->_title($this->__('Products Ordered'));

        $this->_initAction()
            ->_setActiveMenu('report/product/sold')
            ->_addBreadcrumb(Mage::helper('reports')->__('Products Ordered'), Mage::helper('reports')->__('Products Ordered'))
            ->_addContent($this->getLayout()->createBlock('cminds_positions/adminhtml_report_product_sold'))
            ->renderLayout();
    }
}

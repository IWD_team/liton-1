<?php

class Cminds_Positions_Adminhtml_Sales_Order_FilterController
    extends Mage_Adminhtml_Controller_Action
{
    public function filterSalesOrderGridAction(){

        $params = $this->getRequest()->getParams();

        Mage::getSingleton('admin/session')->setSalesrepFilterData($params);

        echo json_encode(array('success'=>true));
    }

    protected function _isAllowed()
    {
        return true;
    }

}
<?php

class Cminds_Positions_Adminhtml_PositiontypeController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction(){
        $this->_title($this->__('Representative Positions'));
        $this->loadLayout();
        $this->_setActiveMenu('customer');
        $this->_addContent(
            $this->getLayout()->createBlock(
                'cminds_positions/adminhtml_positiontype_list'
            )
        );
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $position_id = $this->getRequest()->getParam('id', null);
        $model = Mage::getModel('cminds_positions/positiontype')->load($position_id);

        if ($model) {
            Mage::register('positiontype_data', $model);
        }

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('cminds_positions/adminhtml_positiontype_edit'));
        $this->renderLayout();
    }

    public function saveAction() {
        if ($positionId = $this->getRequest()->getParam('id', false)) {
            $position = Mage::getModel('cminds_positions/positiontype')
                ->load($positionId);

            if (!$position->getId()) {
                $this->_getSession()->addError(
                    $this->__('This position no longer exists.')
                );

                return $this->_redirect(
                    '*/*/index'
                );
            }
        } else {
            $position = false;
        }

        if ($postData = $this->getRequest()->getPost()) {
            try {
                if(!$position) {
                    $position = Mage::getModel('cminds_positions/positiontype');
                    unset($postData['id']);
                }
                $position->addData($postData);
                $position->save();

                $this->_getSession()->addSuccess($this->__('The position has been saved.'));

                return $this->_redirect(
                    '*/*/index'
                );
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }

    }

    public function deleteAction() {
        if ($fieldId = $this->getRequest()->getParam('id', false)) {
            $field = Mage::getModel('cminds_positions/positiontype');
            $field->load($fieldId);

            if (!$field->getId()) {
                $this->_getSession()->addError(
                    $this->__('This position no longer exists.')
                );
            }

            try {
                $field->delete();
            } catch(Exception $e) {
                $this->_getSession()->addError(
                    $this->__('Can not delete this position.')
                );
            }
        }

        return $this->_redirect(
            '*/*/index'
        );
    }
}
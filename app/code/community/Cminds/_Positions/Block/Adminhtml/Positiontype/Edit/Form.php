<?php

class Cminds_Positions_Block_Adminhtml_Positiontype_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        if (Mage::registry('positiontype_data')){
            $data = Mage::registry('positiontype_data')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));

        $form->setUseContainer(true);

        $this->setForm($form);

        $fieldset = $form->addFieldset('positiontype_form', array(
            'legend' =>Mage::helper('cminds_positions')->__('Position Info')
        ));

        $fieldset->addField('id', 'hidden', array(
            'name'      => 'id',
        ));

        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('cminds_positions')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
        ));


        $collection = Mage::getModel('cminds_positions/positiontype')->getCollection();

        $parents = array();

        $parents[0] = Mage::helper('cminds_positions')->__('No Parent Type');

        foreach($collection As $row) {
            if(isset($data['id']) && $row->getId() == $data['id']) continue;
            $parents[$row->getId()] = $row->getName();
        }

        $fieldset->addField('parent_id', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Parent Position'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'parent_id',
            'values'    => $parents
        ));

        $fieldset->addField('can_change_goals', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Can Change Goals'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'can_change_goals',
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
        ));
        $form->setValues($data);

        return parent::_prepareForm();
    }
}
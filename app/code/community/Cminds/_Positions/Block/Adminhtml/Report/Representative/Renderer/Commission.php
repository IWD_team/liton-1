<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_Commission extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        if ($row->getRepId()) {
            if ($row->getRepPercentage() != null) {
                return $row->getRepPercentage() * 100 . '%';
            } else {
                return '0%';
            }
        }
    }
}

<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_StatusName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $status = $row->getData($this->getColumn()->getIndex());
        return ucwords($status);
    }
}

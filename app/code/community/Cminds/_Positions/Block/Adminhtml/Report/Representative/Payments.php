<?php
class Cminds_Positions_Block_Adminhtml_Report_Representative_Payments extends Cminds_Positions_Block_Adminhtml_Report_Representative_Abstract {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('salesrep_payments_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = new Varien_Data_Collection();

        $adminUser = Mage::getModel('admin/user')->load($this->getUserId());


        $s = new Varien_Object();
        $s->setTitle("Grand Total");
        $commissions = Mage::getModel('salesrep/salesrep')->getCollection();
        $commissions
            ->getSelect()
            ->joinLeft(array('s' => 'sales_flat_order'), 'main_table.order_id = s.entity_id', 'created_at')
            ->where('main_table.rep_id = ?', $this->getUserId())
            ->where('main_table.rep_commission_status != ?', 'ineligible')
            ->where('main_table.rep_commission_status != ?', 'canceled');

        $filterFrom = $this->getFilter('from');
        $filterTo = $this->getFilter('to');

        if($filterFrom && $filterTo) {
            $date = new Zend_Date($filterFrom, 'd/m/Y');
            $commissions->getSelect()->where('s.created_at >= ?', $date->toString('Y-m-d 00:00:00'));
            $date = new Zend_Date($filterTo, 'd/m/Y');
            $commissions->getSelect()->where('s.created_at <= ?', $date->toString('Y-m-d 23:59:59'));
        }


        $date = new DateTime('yesterday');

        $subordinatesArray = Mage::helper('cminds_positions')
            ->getAllSubordinates($this->getUserId());

        $canDisplayDownline = Mage::helper('cminds_positions')
            ->canDisplayDownline($subordinatesArray, $date, $this->getUserId());

        if (!$canDisplayDownline) {
            $commissions->addFieldToFilter('main_table.is_downline', 0);
        }

        $commissionEarned = 0;

        foreach($commissions AS $c) {
            $commissionEarned += $c->getRepCommissionEarned();
        }
        $s->setValue($commissionEarned);
        $s->setType('net');
        $collection->addItem($s);

        $this->setCollection($collection);
    }

    protected function _prepareColumns()
    {
        $baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('salesrep')->__('PAYMENT'),
            'column_css_class' => 'main_header',
            'index'     => 'title',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('salesrep')->__(''),
            'index'     => 'value',
            'type'      => 'currency',
			'width'     => '100px',
            'currency_code'  => $baseCurrencyCode,
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_Total'
        ));
        return parent::_prepareColumns();
    }
}

<?php

class Cminds_Positions_Block_Adminhtml_Notifications_Container_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cminds_positions/notifications/form.phtml');
    }


    public function getRegionOptionArray(){
        $regionArray = Mage::helper('cminds_positions')->getCurrentAdminRegions();

        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('id', array('in' => $regionArray));

        $options = array();

        $options[] = array('value' => 0, 'label' => 'No region');
        foreach($positionsCollection as $position){
            $options[] = array('value' => $position->getId(), 'label' => $position->getName());
        }

        return $options;
    }

    public function getOfficesOptionArray($region){
        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('parent_id', array('eq' => $region));

        $options = array();

        $options[] = array('value' => 0, 'label' => 'No office');
        foreach($positionsCollection as $position){
            $options[] = array('value' => $position->getId(), 'label' => $position->getName());
        }

        return $options;
    }

    public function getCustomersArray(){
        $customersCollection = Mage::getModel('customer/customer')
            ->getCollection();

        $options = array();
        foreach($customersCollection as $customer){
            $options[] = $customer->getId();
        }

        return $options;
    }

    public function getCustomersOptionArray(){
        $options = array();

        $options[] = array('value' => 0, 'label' => 'All Customers','office_id' => '', 'region_id' => '');
        foreach($this->getCustomersArray() as $customer){

            $customerModel = Mage::getModel('customer/customer')->load($customer);
            $office_id = '';
            $region_id = '';
            if($customerModel->getSalesrepRepId()) {
                $salesrepId = $customerModel->getSalesrepRepId();

                $adminModel = Mage::getModel('admin/user')->load($salesrepId);

                $positionId = $adminModel->getSalesrepPositionId();

                $positionModel = Mage::getModel('cminds_positions/position')->load($positionId);

                if($positionModel->getId()){
                    $office_id = $positionModel->getId();
                    $region_id = $positionModel->getParentId();
                }

            }
            $options[] = array(
                'value' => $customerModel->getId(),
                'label' => $customerModel->getName(),
                'office_id' => $office_id,
                'region_id' => $region_id
            );
        }

        return $options;
    }

    public function getFormActionUrl(){
        return $this->getUrl('*/notifications/index');
    }
}
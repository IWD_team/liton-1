<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_Shipment extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $orderModel = Mage::getModel('sales/order')->load($row->getId());
        if($orderModel->getId()){
            $hasShipments = $orderModel->hasShipments();
            if($hasShipments && $hasShipments > 1) {
                return 'Multiple Shipping';
            } elseif($hasShipments == 1){
                $shipmentTrackModel = Mage::getModel('sales/order_shipment_track')->load($orderModel->getId(), 'order_id');

                if($shipmentTrackModel->getId()){
                    $trackName = '';

                    if($shipmentTrackModel->getTitle()){
                        $trackName = $shipmentTrackModel->getTitle();
                    }

                    if($shipmentTrackModel->getTrackNumber()){
                        $trackName .= ' ' . $shipmentTrackModel->getTrackNumber();
                    }
                    return $trackName;
                }
            } else {
                return '';
            }
        }
    }
}
<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals_Renderer_Type extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        if($row->getPeriodType() == 1){
            return $this->__('Month');
        } else {
            return $this->__('Year');
        }
    }
}
<?php
class Cminds_Positions_Block_Adminhtml_Position_List_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('supplier_list_grid');
        $this->setDefaultDir('asc');
//        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('cminds_positions/position')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('cminds_positions')->__('ID'),
            'width'     => '50px',
            'index'     => 'id',
            'type'  => 'number',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('cminds_positions')->__('Name'),
            'index'     => 'name'
        ));
        $this->addColumn('parent_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Parent Position Name'),
            'index'     => 'parent_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Position_List_Renderer_Parent'
        ));
        $this->addColumn('type_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Type Name'),
            'index'     => 'type_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Position_List_Renderer_Parenttype'
        ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('cminds_positions')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('cminds_positions')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}

<?php
class Cminds_Positions_Model_Source_MonthsList {

    public static function toOptionArray() {

        $result = array(
            array('value' => 0, 'label' => 'None'),
            array('value' => 1, 'label' => 'January'),
            array('value' => 2, 'label' => 'February'),
            array('value' => 3, 'label' => 'March'),
            array('value' => 4, 'label' => 'April'),
            array('value' => 5, 'label' => 'May'),
            array('value' => 6, 'label' => 'June'),
            array('value' => 7, 'label' => 'July'),
            array('value' => 8, 'label' => 'August'),
            array('value' => 9, 'label' => 'September'),
            array('value' => 10, 'label' => 'October'),
            array('value' => 11, 'label' => 'November'),
            array('value' => 12, 'label' => 'December')
        );

        return $result;
    }
}

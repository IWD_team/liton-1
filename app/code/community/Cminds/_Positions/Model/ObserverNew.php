<?php

class Cminds_Positions_Model_Observer extends LucidPath_SalesRepDeluxe_Model_Observer
{
    const XML_PATH_EMAIL_TEMPLATE = 'sales_email/order/template';
    const XML_PATH_EMAIL_IDENTITY = 'sales_email/order/identity';
    const XML_PATH_EMAIL_SHIPMENT_TEMPLATE = 'sales_email/shipment/template';
    const XML_PATH_EMAIL_SHIPMENT_IDENTITY = 'sales_email/shipment/identity';

    public function sales_order_place_after(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        $rep_id = "";

        if (Mage::helper('salesrep')->isAdmin()) {
            // backend
            $post = Mage::app()->getFrontController()->getRequest()->getPost();

            if (isset($post) && isset($post['salesrep_rep_id'])) {
                // rep from dropdown
                $rep_id = $post['salesrep_rep_id'];
            } else {
                $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

                if ($customer->getId()) {
                    // rep from customer
                    if (!$rep_id = $customer->getSalesrepRepId()) {
                        // current logged in admin
                        $rep_id = Mage::getSingleton('admin/session')->getUser()->getId();

                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $table = Mage::getSingleton('core/resource')->getTableName('customer/entity');

                        $write->query("UPDATE {$table} SET salesrep_rep_id = '" . $rep_id . "' WHERE entity_id = " . $customer->getId() . ";");
                    }
                }
            }
        } else {
            // frontend
            $rep_id = intval(Mage::getSingleton('core/session')->getRepId());

            if (!$rep_id) {
                $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

                if ($customer->getId()) {
                    // rep from customer
                    $rep_id = $customer->getSalesrepRepId();
                }

            }

            Mage::getSingleton('core/session')->setRepId("");
        }

        Mage::helper('cminds_positions')->setCommissionEarned($order->getId(),
            $rep_id);

//        return $this;
    }

    public function afterOrderPlaced(Varien_Event_Observer $observer){

        /**
         * Send order confirmation email to salesrep assigned to customer.
         */
        $order = $observer->getOrder();
        $storeId = Mage::app()->getStore()->getStoreId();

        if($order->getCustomerId()) {
            $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

            /**
             * Check if mail can be send.
             */
            if($customer->getSalesrepNotifyWhenOrderPlaced()){
                $adminModel = Mage::getModel('admin/user');
                $salesrepEmail = '';
                if($customer->getSalesrepRepId()){
                    $salesrep = $adminModel->load($customer->getSalesrepRepId());
                    if($salesrep->getId()){
                        $salesrepEmail = $salesrep->getEmail();
                    }
                }

                if($salesrepEmail){

                    $order = Mage::getModel('sales/order')->load($order->getId());

                    try {
                        // Retrieve specified view block from appropriate design package (depends on emulated store)
                        $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                            ->setIsSecureMode(true);
                        $paymentBlock->getMethod()->setStore($storeId);
                        $paymentBlockHtml = $paymentBlock->toHtml();
                    } catch (Exception $exception) {
                        // Stop store emulation process
                        throw $exception;
                    }

                    // Stop store emulation process

                    // Retrieve corresponding email template id and customer name
                    $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
                    $customerName = $order->getCustomerName();

                    /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
                    $mailer = Mage::getModel('core/email_template_mailer');
                    /** @var $emailInfo Mage_Core_Model_Email_Info */
                    $emailInfo = Mage::getModel('core/email_info');
                    $emailInfo->addTo($salesrepEmail, $customerName);
                    $mailer->addEmailInfo($emailInfo);

                    $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
                    $mailer->setStoreId($storeId);
                    $mailer->setTemplateId($templateId);
                    $mailer->setTemplateParams(array(
                        'order'        => $order,
                        'billing'      => $order->getBillingAddress(),
                        'payment_html' => $paymentBlockHtml
                    ));

                    $mailer->send();

                }
            }
        }
        return $this;
    }

    public function afterShipmentCreate(Varien_Event_Observer $observer){

        /**
         * Send shipment confirmation email to salesrep assigned to customer.
         */
        $shipment = $observer->getShipment();
        $order = $shipment->getOrder();
        $comment = '';
        $storeId = Mage::app()->getStore()->getStoreId();

        if($order->getCustomerId()) {
            $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

            /**
             * Check if mail can be send.
             */
            if($customer->getSalesrepNotifyWhenOrderShipped()){
                $adminModel = Mage::getModel('admin/user');
                $salesrepEmail = '';
                if($customer->getSalesrepRepId()){
                    $salesrep = $adminModel->load($customer->getSalesrepRepId());
                    if($salesrep->getId()){
                        $salesrepEmail = $salesrep->getEmail();
                    }
                }

                if($salesrepEmail){

                    $order = Mage::getModel('sales/order')->load($order->getId());

                    try {
                        // Retrieve specified view block from appropriate design package (depends on emulated store)
                        $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                            ->setIsSecureMode(true);
                        $paymentBlock->getMethod()->setStore($storeId);
                        $paymentBlockHtml = $paymentBlock->toHtml();
                    } catch (Exception $exception) {
                        // Stop store emulation process
                        throw $exception;
                    }

                    // Stop store emulation process

                    // Retrieve corresponding email template id and customer name
                    $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_SHIPMENT_TEMPLATE, $storeId);
                    $customerName = $order->getCustomerName();

                    /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
                    $mailer = Mage::getModel('core/email_template_mailer');
                    /** @var $emailInfo Mage_Core_Model_Email_Info */
                    $emailInfo = Mage::getModel('core/email_info');
                    $emailInfo->addTo($salesrepEmail, $customerName);
                    $mailer->addEmailInfo($emailInfo);

                    $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_SHIPMENT_IDENTITY, $storeId));
                    $mailer->setStoreId($storeId);
                    $mailer->setTemplateId($templateId);
                    $mailer->setTemplateParams(array(
                            'order'        => $order,
                            'shipment'     => $shipment,
                            'comment'      => $comment,
                            'billing'      => $order->getBillingAddress(),
                            'payment_html' => $paymentBlockHtml
                        )
                    );

                    $mailer->send();

                }
            }
        }
        return $this;
    }
}

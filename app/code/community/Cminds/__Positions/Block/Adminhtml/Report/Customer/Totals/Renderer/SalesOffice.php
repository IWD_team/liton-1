<?php
class Cminds_Positions_Block_Adminhtml_Report_Customer_Totals_Renderer_SalesOffice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row)
    {
        $customerId = $row->getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($customerId);

        $salesrepRepId = $customer->getSalesrepRepId();

        $customerOffice = '';

        if($salesrepRepId){
            $admin = Mage::getModel('admin/user')->load($salesrepRepId);

            $salesrepPosition = $admin->getSalesrepPositionId();

            if($salesrepPosition){
                $positionModel = Mage::getModel('cminds_positions/position')->load($salesrepPosition);

                $customerOffice = $positionModel->getName();
            }
        }
        return $customerOffice;
    }
}
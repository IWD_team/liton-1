<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_Total extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        if ($row->getType() == 'net') {
            return '<span style="color: #1F497D; font-weight: bolder;">' . Mage::helper('core')->currency($row->getValue(),
                true, false) . '</span>';
        } else {
            return Mage::helper('core')->currency($row->getValue(), true,
                false);
        }
    }
}

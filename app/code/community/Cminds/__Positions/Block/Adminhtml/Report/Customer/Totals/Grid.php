<?php
class Cminds_Positions_Block_Adminhtml_Report_Customer_Totals_Grid extends Mage_Adminhtml_Block_Report_Grid
{
    /**
     * Sub report size
     *
     * @var int
     */
    protected $_subReportSize = 0;

    /**
     * Initialize Grid settings
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
        $this->setTemplate('cminds_positions/report/product/grid/form.phtml');
        $this->setUseAjax(false);
        $this->setCountTotals(true);
        $this->setId('gridProductsSold');
    }


    /**
     * Prepare collection object for grid
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        $collection = $this->getCollection()
            ->initReport('cminds_positions/report_customer_totals_collection');

        Mage::register('report_region',$this->getFilter('report_region'));
        Mage::register('region_office',$this->getFilter('report_office'));
        Mage::register('region_customer',$this->getFilter('report_customer'));

        return $collection;
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'    => $this->__('Customer Name'),
            'sortable'  => false,
            'index'     => 'name'
        ));

        $this->addColumn('orders_count', array(
            'header'    => $this->__('Number of Orders'),
            'width'     => '100px',
            'sortable'  => false,
            'index'     => 'orders_count',
            'total'     => 'sum',
            'type'      => 'number'
        ));

        $baseCurrencyCode = $this->getCurrentCurrencyCode();
        $rate = $this->getRate($baseCurrencyCode);

        $this->addColumn('orders_sum_amount', array(
            'header'    => $this->__('Sales'),
            'width'     => '200px',
            'align'     => 'right',
            'sortable'  => false,
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
            'index'     => 'orders_sum_amount',
            'total'     => 'sum',
            'renderer'  => 'adminhtml/report_grid_column_renderer_currency',
            'rate'      => $rate,
        ));

        $this->addColumn('orders_sum_invoiced_amount', array(
            'header'    => $this->__('Invoiced'),
            'width'     => '200px',
            'align'     => 'right',
            'sortable'  => false,
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
            'index'     => 'orders_sum_invoiced_amount',
            'total'     => 'sum',
            'renderer'  => 'adminhtml/report_grid_column_renderer_currency',
            'rate'      => $rate,
        ));

        $this->addColumn('orders_count1', array(
            'header'    => $this->__('Yearly Goal'),
            'width'     => '100px',
            'sortable'  => false,
            'index'     => 'orders_count',
            'total'     => 'sum',
            'type'      => 'number',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Customer_Totals_Renderer_YearlyGoal',
            'rate'      => $rate,
        ));

        $this->addColumn('orders_count2', array(
            'header'    => $this->__('Percentage to Goal'),
            'width'     => '100px',
            'sortable'  => false,
            'index'     => 'orders_count',
            'total'     => 'sum',
            'type'      => 'number',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Customer_Totals_Renderer_GoalPercentage',
            'rate'      => $rate,
        ));

        $this->addColumn('orders_count3', array(
            'header'    => $this->__('Sales Office'),
            'width'     => '100px',
            'sortable'  => false,
            'index'     => 'orders_count',
            'total'     => 'sum',
            'type'      => 'text',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Customer_Totals_Renderer_SalesOffice',
        ));
        $this->addExportType('*/*/exportTotalsCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportTotalsExcel', Mage::helper('reports')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getPositionsHelper(){
        return Mage::helper('cminds_positions');
    }

    protected function getSubordinateOffices(){
        $helper = $this->getPositionsHelper();
        $offices = $helper->getCurrentAdminOffices();

        $positions = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('id', array('in' => $offices));

        $officesArray = array();
        foreach($positions as $position){
            $officesArray[] = array('value' => $position->getId(), 'label' => $position->getName(), 'parent_id' => $position->getParentId());
        }
        return $officesArray;
    }

    public function getSubordinateRegions(){
        $helper = $this->getPositionsHelper();
        $regions = $helper->getCurrentAdminRegions();

        $positions = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('id', array('in' => $regions));

        $regionsArray = array();
        foreach($positions as $position){
            $regionsArray[] = array('value' => $position->getId(), 'label' => $position->getName());
        }
        return $regionsArray;
    }

    public function canDisplayRegions(){
        $canDisplay = false;

        if($this->getPositionsHelper()->isTopLevel()){
            $canDisplay = true;
        }

        return $canDisplay;
    }

    public function canDisplayOffice(){
        $canDisplay = false;

        if($this->getPositionsHelper()->isRegional()){
            $canDisplay = true;
        }

        return $canDisplay;
    }

    public function getFilterCustomersUrl(){
        return $this->getUrl('*/*/filteredCustomers');
    }

    public function getSalesrepCustomers(){
        $helper = $this->getPositionsHelper();
        $officeId = $helper->getCurrentAdmin()->getSalesrepPositionId();
        $adminIds = $helper->getAdminIdsByOfficeId($officeId);
        $customersCollection = $helper->getCustomersIdsByAssignedRepIds($adminIds);

        $result = '';
        $result .= '<option value="">Please select</option>';
        foreach($customersCollection as $customer){
            $customerData = Mage::getModel('customer/customer')->load($customer->getId());
            $result .= '<option value="' . $customer->getId() . '">' . $customerData->getName() . '</option>';
        }

        return $result;
    }
}

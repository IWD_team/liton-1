<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    private $_currentUser = false;
    private $_filters = array();

    protected $salesSummaryCollection = false;

    public function __construct()
    {
        $this->_controller = 'adminhtml_report_representative';
        $this->_blockGroup = 'cminds_positions';
        $this->_headerText = Mage::helper('reports')->__('Welcome ' . $this->getUserName());
        parent::__construct();
        $this->setFilters();
        $this->setTemplate('cminds_positions/report/representative/grid/container.phtml');
        $this->_removeButton('add');
//        $this->addButton('filter_form_submit', array(
//            'label' => Mage::helper('reports')->__('Show Report'),
//            'onclick' => 'filterFormSubmit()'
//        ));
    }

    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/representative', array('_current' => true));
    }

    protected function getUserId()
    {
        $admins = $this->getFilter('order_admins');

        if (isset($admins[0]) && is_array($admins[0])) {
            $p = $admins[0];
        } else {
            if (!is_numeric($admins)) {
                $p = false;
            } else {
                $p = $admins;
            }
        }

        if (!$p) {
            $p = Mage::registry('admin_user')->getId();
        }

        return $p;
    }

    public function getUser()
    {
        if (!$this->_currentUser) {
            $this->_currentUser = Mage::getModel('admin/user')->load($this->getUserId());
        }

        return $this->_currentUser;
    }

    public function getUserName()
    {
        return $this->getUser()->getFirstname() . ' ' . $this->getUser()->getLastname();
    }

    public function getHeaderHtml()
    {
        if (!$this->getIsPdfTemplate()) {
            return '<h3 class="' . $this->getHeaderCssClass() . '">' . $this->getHeaderText() . '</h3>';
        } else {
            return '';
        }
    }

    public function getFilter($name)
    {

        if (isset($this->_filters[$name])) {
            return $this->_filters[$name];
        } else {
            return ($this->getRequest()->getParam($name)) ? htmlspecialchars($this->getRequest()->getParam($name)) : '';
        }
    }

    public function setFilters()
    {
        $filter = Mage::app()->getRequest()->getParam('filter', null);
        if (is_null($filter)) {
            $filter = $this->_defaultFilter;
        }

        if (is_string($filter)) {
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);

            $this->setFilter('from', $data['from']);

            if(isset($data['office_data'])){
                if(is_array($data['office_data'])){
                    if(isset($data['region_id']) && $data['region_id'] != 0){
                        $this->setFilter('office_data', $data['office_data'][$data['region_id']]);
                        $this->setFilter('region_id', $data['region_id']);
                    }
                } else {
                    $this->setFilter('office_data', $data['office_data']);
                }
            }
            if(isset($data['filter_from']) && isset($data['filter_to'])) {
                $this->setFilter('filter_from', $data['filter_from']);
                $this->setFilter('filter_to', $data['filter_to']);
            }

        } else {
            $this->setFilter('from', date('Y-m-d'));
        }


        return $this;
    }

    public function getFilters(){
        return $this->_filters;
    }

    public function getPeriod()
    {
        $filterDate = $this->getFilter('from');
        $date = new DateTime($filterDate);
        return $date->format('F Y');
    }

    public function setFilter($name, $value)
    {
        if ($name) {
            $this->_filters[$name] = $value;
        }
    }

    public function canShowDownlineCommission()
    {
        $userData = $this->getUser()->getSalesrepIsStoreManager();

        if ($userData) {
            return false;
        }
        return true;
    }

    public function getPositionName(){
        $positionId = $this->getUser()->getSalesrepPositionId();

        if($positionId) {
            $positionModel = Mage::getModel('cminds_positions/position')->load($positionId);
            if($positionModel->getName()){
                return $positionModel->getName();
            }
        }
        return '';
    }

    public function getChildrenPositions(){

        $posId = $this->getFilterPosition();

        $subordinatePositions = Mage::helper('cminds_positions')->getAllSubordinates($posId, true);

        return $subordinatePositions;
    }

    public function getFilterPosition(){

        if(isset($this->_filters['region_id'])){
            $posId = $this->_filters['region_id'];
        } else {
            $posId = $this->getUser()->getSalesrepPositionId();
        }
        return $posId;
    }

    /**
     * Returns children positions for national salesrep except region ids
     */
    public function getLimitedPositions(){
        $childrenPositions = $this->getChildrenPositions();

        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('parent_id', $this->getUser()->getSalesrepPositionId());

        $firstChildPosition = array();

        foreach ($positionsCollection as $position){
            $firstChildPosition[] = $position->getId();
        }

        $firstChildPosition[] = $this->getUser()->getSalesrepPositionId();

        return array_diff($childrenPositions, $firstChildPosition);
    }

    public function hasChildren(){
        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('parent_id', $this->getUser()->getSalesrepPositionId());

        if($positionsCollection->getSize()){
            return true;
        }
        return false;
    }

    public function getViewAllUrl($region_id){

        return $this->getUrl('*/sales_order/index', array('region_filter' => $region_id));
    }

    public function getPeriodArray(){
        $filters = $this->getFilters();
        $periodMonthArray = array();

        if(isset($filters['filter_from']) && isset($filters['filter_to'])) {
            $start = $month = strtotime($filters['filter_from']);
            $end = strtotime($filters['filter_to']);
            while ($month <= $end) {
                $dateArray = array();
                $dateArray['start_date'] = date('d-m-Y', $month);
                $dateArray['end_date'] = date('t-m-Y', $month);
                $nextMonth = strtotime("+1 month", $month);
                if ($nextMonth >= $end) {
                    $dateArray['end_date'] = date('d-m-Y', $end);
                }
                $month = strtotime(date("01-m-Y", $nextMonth));
                $periodMonthArray[] = $dateArray;
            }
        } else {
            if(isset($filters['from'])) {
                $start = strtotime($filters['from']);
                $dateArray = array();
                $dateArray['start_date'] = date('01-m-Y', $start);
                $dateArray['end_date'] = date('t-m-Y', $start);
                $periodMonthArray[] = $dateArray;
            }
        }
        return $periodMonthArray;
    }

    public function isTopLevel(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());

        if($positionModel->getId()){
            if($positionModel->getParentId() == 0 || $positionModel->getParentId() == null){
                return true;
            }
        }
        return false;
    }

    public function isRegional(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());


        if($positionModel->getId()){
            $childrenCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $currenUser->getSalesrepPositionId()));
            if($positionModel->getParentId() != 0 && $positionModel->getParentId() != null && $childrenCollection->getSize()){
                return true;
            }
        }
        return false;
    }

    public function getRegions(){

        $currentUser = Mage::getSingleton('admin/session')->getUser();

        $filters = $this->getFilters();

        $regionsArray = array();
        if($this->isTopLevel()) {
            $positionCollection = Mage::getModel('cminds_positions/position')
                ->getCollection();
            if(isset($filters['region_id']) && isset($filters['office_data'])) {
                if ($filters['region_id'] != 0) {
                    $positionCollection
                        ->addFieldToFilter(
                            'parent_id',
                            $filters['region_id']
                        );
                }

                if ($filters['office_data'] != 0) {
                    $positionCollection
                        ->addFieldToFilter(
                            'id',
                            $filters['office_data']
                        );
                }
            } else {
                $positionCollection
                ->addFieldToFilter('parent_id',
                    $currentUser->getSalesrepPositionId());
            }

            foreach ($positionCollection as $position) {
                $regionsArray[] = $position->getId();
            }
        } elseif($this->isRegional()){
            $positionCollection = Mage::getModel('cminds_positions/position')
                ->getCollection();

            if(isset($filters['office_data']) && $filters['office_data'] != 0) {
                $positionCollection
                    ->addFieldToFilter('id',
                        $filters['office_data']);
            } else {
                $positionCollection
                    ->addFieldToFilter('parent_id',
                    $currentUser->getSalesrepPositionId());
            }
            foreach ($positionCollection as $position) {
                $regionsArray[] = $position->getId();
            }

        } else {
            $regionsArray[] = $currentUser->getSalesrepPositionId();
        }
        return $regionsArray;
    }

    public function getRepIdsByRegion($regionId){
        $filters = $this->getFilters();
        $positionsUnderRegion = array();
        if($this->isTopLevel()) {
            if(!isset($filters['region_id'])) {
                $positionCollection = Mage::getModel('cminds_positions/position')
                    ->getCollection()
                    ->addFieldToFilter('parent_id', $regionId);


                foreach ($positionCollection as $position) {
                    $positionsUnderRegion[] = $position->getId();
                }
            } else {
                $positionsUnderRegion[] = $regionId;
            }
        } elseif($this->isRegional()){
            $positionsUnderRegion[] = $regionId;
        } else {
            $positionsUnderRegion[] = $regionId;
        }

        $adminUserCollection = Mage::getModel('admin/user')
            ->getCollection()
            ->addFieldToFilter('salesrep_position_id', array('in' => $positionsUnderRegion));

        $salesrepArray = array();

        foreach($adminUserCollection as $admin){
            $salesrepArray[] = $admin->getId();
        }

        return $salesrepArray;
    }

    protected function getSalesSummaryCollection()
    {
        $collection = new Varien_Data_Collection();

        $adminRegions = $this->getRegions();

        $periodRangeArray = $this->getPeriodArray();

        $filterFrom = $this->getFilter('from');
        $filterTo = $this->getFilter('to');

        if(empty($periodRangeArray)){
            $periodRangeArray = array();
            if($filterTo == ''){
                $filterFrom = date('Y-m-01', strtotime($filterFrom));
                $filterTo = date('Y-m-t', strtotime($filterFrom));
            }
            $periodRangeArray[] = array('start_date' => $filterFrom, 'end_date' => $filterTo);
        }

        foreach($periodRangeArray as $period) {
            $periodTimestamp = strtotime($period['start_date']);
            $periodYear = date('Y', $periodTimestamp);
            foreach ($adminRegions as $region) {
                $commissions = Mage::getModel('sales/order')->getCollection();
                $regionSalesreps = array();
                $regionSalesreps = $this->getRepIdsByRegion($region);

                $filterFrom = $period['start_date'];
                $filterTo = $period['end_date'];

                if ($filterFrom && $filterTo) {
                    $dateFrom = new Zend_Date($filterFrom, 'd-m-Y');

                    $dateTo = new Zend_Date($filterTo, 'd-m-Y');
                    $commissions->addAttributeToFilter('created_at', array(
                        'from' => $dateFrom->toString('Y-m-d 00:00:00'),
                        'to' => $dateTo->toString('Y-m-d 23:59:59')
                    ));
                }

                $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                    $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                    array('rep_id'));
                $commissions->getSelect()->where('main_table.state != ?',
                    'canceled');

                $commissions->addFieldToFilter('rep_id',
                    array('in' => $regionSalesreps));

                $periodPV = 0;

                foreach ($commissions AS $c) {
                    $periodPV += $c->getGrandTotal();
                }

                $year = date('Y',strtotime($dateFrom->toString('d-m-Y')));
                $commissions = Mage::getModel('sales/order')->getCollection()
                    ->addAttributeToFilter('created_at', array(
                        'from' => date('Y-01-01 00:00:00'),
                        'to' => date('Y-m-d H:i:s')
                    ));
                $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                    $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                    array('rep_id'));
                $commissions->getSelect()->where('main_table.state != ?',
                    'canceled');
                $commissions->addFieldToFilter('rep_id',
                    array('in' => $regionSalesreps));

                $ytdPV = 0;

                foreach ($commissions AS $c) {
                    $ytdPV += $c->getGrandTotal();
                }

                $commissions = Mage::getModel('sales/order')->getCollection();
                $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                    $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                    array('rep_id'));
                $commissions->getSelect()->where('main_table.state != ?',
                    'canceled');
                $commissions->addFieldToFilter('rep_id',
                    array('in' => $regionSalesreps));

                $lifetimePV = 0;

                foreach ($commissions AS $c) {
                    $lifetimePV += $c->getGrandTotal();
                }

                $position = Mage::getModel('cminds_positions/position')
                    ->load($region);

                $month = date('F',strtotime($dateFrom->toString('d-m-Y')));


                $s = new Varien_Object();
                $s->setTitle($position->getName());
                $s->setPeriod($periodPV);
                $s->setYtd($ytdPV);
                $s->setLifetime($lifetimePV);
                $s->setIsSeparator(false);
                $s->setRegionId($region);
                $s->setPositionId($position->getId());
                $s->setMonth($month);
                $s->setYear($periodYear);
                $s->setIsNational($this->isTopLevel());
                $s->setIsRegional($this->isRegional());
                $collection->addItem($s);
            }
        }

        $this->salesSummaryCollection = $collection;
        return $this->salesSummaryCollection;
    }

    protected function getTableName($model)
    {
        return Mage::getSingleton('core/resource')->getTableName($model);
    }

    public function getHeight(){

        $baseHeight = 30;
        $officesArray = $this->getGroupedOfficesArray();

        $officesMultiplier = count($officesArray);


        $dateMultiplier = count(reset($officesArray));

        $height = $baseHeight * $officesMultiplier * $dateMultiplier;

        if($height < 200){
            $height = 200;
        }

        return $height . 'px';
    }
    public function getGroupedOfficesArray(){
        if ($salesSummaryCollection = $this->salesSummaryCollection) {
            $filteredCollection = $salesSummaryCollection;
        } else {
            $filteredCollection = $this->getSalesSummaryCollection();
        }

        $arrayJson = array();
        $officesGroupedArray = array();
        $officesData = array();
        foreach ($filteredCollection as $element){
			if($element->getData('period') != 0) {
            $officesArray = array();
            $officesArray[] = $element->getData('period');
            $officesArray[] = $element->getData('month') . ' ' . $element->getData('year');
            $officesArray[] = $element->getData('title');
            $officesData[] = $officesArray;
		}
        }

        foreach($officesData as $office){
            $group = array();
            $group[] = $office;
            $officesGroupedArray[$office[2]][] = $office;
        }
        return $officesGroupedArray;
    }
    public function getJsonForChart(){
        $array = $this->getGroupedOfficesArray();

        return json_encode(array_values($array));
    }

    public function getSummaryTotalGoal(){
        $result = '';
        $goalData = $this->getGoalSummary();
        if(isset($goalData['can_display_header']) && $goalData['can_display_header']){
            $result = 'Total YTD '.Mage::helper('core')->currency($goalData['goal_data']['ytd'], true, false).' is '.$goalData['goal_data']['percentage'].'% of goal';
        }

        return $result;
    }
    public function getGoalSummary(){
        $resultArray = array();
        $filters = $this->getFilters();

        if($this->isTopLevel()){
            if(!isset($filters['region_id'])) {
                $resultArray = array(
                    'can_display_header' => false
                );
            } else {
                $regionId = $filters['region_id'];
                $resultArray = array(
                    'can_display_header'    => true,
                    'region_id'             => $filters['region_id']
                );
                if(!isset($filters['office_data']) || $filters['office_data'] == 0){
                    $resultArray['goal_data'] = $this->getYtdAmount($regionId, false);
                } else {
                    $officeId = $filters['office_data'];
                    $resultArray['goal_data'] = $this->getYtdAmount($regionId, $officeId);
                }

            }
        } elseif($this->isRegional()){
            $regionId = $this->getUser()->getSalesrepPositionId();
            $resultArray = array(
                'can_display_header'    => true,
                'region_id'             => $regionId
            );
            if(isset($filters['office_data'])){
                $officeId = $filters['office_data'];
            } else {
                $officeId = false;
            }
            $resultArray['goal_data'] = $this->getYtdAmount($regionId, $officeId);
        } else {
            $regionId = $this->getUser()->getSalesrepPositionId();
            $resultArray = array(
                'can_display_header'    => true,
                'region_id'             => $regionId
            );

            $resultArray['goal_data'] = $this->getYtdAmount($regionId);
        }

        return $resultArray;
    }

    public function getYtdAmount($regionId, $officeId = false){
        if($salesCollection = $this->salesSummaryCollection) {
            $salesSummaryCollection = $salesCollection;
        } else {
            $salesSummaryCollection = $this->getSalesSummaryCollection();
        }

        $goalSummary = array();
        $regionTarget = 0;
        $ytdSales = 0;
        foreach($salesSummaryCollection as $salesSummary){
            $goalsCollection = Mage::getModel('cminds_positions/customergoal')->getCollection();
            $salesSummaryRegionId = $salesSummary->getRegionId();
            if($this->isTopLevel() || $this->isRegional()) {
                if (!$officeId) {

                    $parentPosition = $this->getParentPosition($salesSummaryRegionId);
                    if ($regionId == $parentPosition) {
                        if ($salesSummary->getYtd()) {
                            $ytdSales += $salesSummary->getYtd();
                        }
                        $goalsCollection
                            ->addFieldToFilter('region_id',
                                array('eq' => $regionId))
                            ->addFieldToFilter('type_id', array('eq' => 3))
                            ->addFieldToFilter('period_type', array('eq' => 2))
                            ->addFieldToFilter('period_year',
                                array('eq' => date('Y')));

                        if ($goalsCollection->getFirstItem()->getId()) {
                            $regionTarget = $goalsCollection->getFirstItem()->getPeriodTarget();
                        }
                    }
                    $goalSummary = array(
                        'ytd' => $ytdSales,
                        'target' => $regionTarget
                    );
                } else {
                    if ($officeId == $salesSummaryRegionId) {
                        if ($salesSummary->getYtd()) {
                            $ytdSales += $salesSummary->getYtd();
                        }
                        $goalsCollection
                            ->addFieldToFilter('office_id',
                                array('eq' => $salesSummaryRegionId))
                            ->addFieldToFilter('type_id', array('eq' => 2))
                            ->addFieldToFilter('period_type', array('eq' => 2))
                            ->addFieldToFilter('period_year',
                                array('eq' => date('Y')));

                        if ($goalsCollection->getFirstItem()->getId()) {
                            $regionTarget = $goalsCollection->getFirstItem()->getPeriodTarget();
                        }
                    }
                    $goalSummary = array(
                        'ytd' => $ytdSales,
                        'target' => $regionTarget
                    );
                }
            } else {
                if ($regionId == $salesSummaryRegionId) {
                    if ($salesSummary->getYtd()) {
                        $ytdSales += $salesSummary->getYtd();
                    }
                    $goalsCollection
                        ->addFieldToFilter('office_id',
                            array('eq' => $salesSummaryRegionId))
                        ->addFieldToFilter('type_id', array('eq' => 2))
                        ->addFieldToFilter('period_type', array('eq' => 2))
                        ->addFieldToFilter('period_year',
                            array('eq' => date('Y')));

                    if ($goalsCollection->getFirstItem()->getId()) {
                        $regionTarget = $goalsCollection->getFirstItem()->getPeriodTarget();
                    }
                }
                $goalSummary = array(
                    'ytd' => $ytdSales,
                    'target' => $regionTarget
                );
            }

        }
        if($goalSummary['target'] != 0) {
            $goalSummary['percentage'] = round(($goalSummary['ytd'] / $goalSummary['target']) * 100, 2);
        } else {
            $goalSummary['percentage'] = 0;
        }

        return $goalSummary;
    }

    public function getParentPosition($id){
        $parentId = '';
        $position = Mage::getModel('cminds_positions/position')->load($id);
        if($position->getId()){
            $parentId = $position->getParentId();
        }
        return $parentId;
    }

    public function getShippingStatusUrl(){
        return $this->getUrl('*/*/saveDefaultShippingStatus');
    }

}

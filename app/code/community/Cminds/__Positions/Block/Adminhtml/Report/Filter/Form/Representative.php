<?php

class Cminds_Positions_Block_Adminhtml_Report_Filter_Form_Representative extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Report field visibility
     */
    protected $_fieldVisibility = array();

    /**
     * Report field opions
     */
    protected $_fieldOptions = array();

    /**
     * Set field visibility
     *
     * @param string Field id
     * @param bool Field visibility
     */
    public function setFieldVisibility($fieldId, $visibility)
    {
        $this->_fieldVisibility[$fieldId] = (bool)$visibility;
    }

    /**
     * Get field visibility
     *
     * @param string Field id
     * @param bool Default field visibility
     * @return bool
     */
    public function getFieldVisibility($fieldId, $defaultVisibility = true)
    {
        if (!array_key_exists($fieldId, $this->_fieldVisibility)) {
            return $defaultVisibility;
        }
        return $this->_fieldVisibility[$fieldId];
    }

    /**
     * Set field option(s)
     *
     * @param string $fieldId Field id
     * @param mixed $option Field option name
     * @param mixed $value Field option value
     */
    public function setFieldOption($fieldId, $option, $value = null)
    {
        if (is_array($option)) {
            $options = $option;
        } else {
            $options = array($option => $value);
        }
        if (!array_key_exists($fieldId, $this->_fieldOptions)) {
            $this->_fieldOptions[$fieldId] = array();
        }
        foreach ($options as $k => $v) {
            $this->_fieldOptions[$fieldId][$k] = $v;
        }
    }

    /**
     * Add report type option
     *
     * @param string $key
     * @param string $value
     * @return Mage_Adminhtml_Block_Report_Filter_Form
     */
    public function addReportTypeOption($key, $value)
    {
        return $this;
    }

    /**
     * Add fieldset with general report fields
     *
     * @return Mage_Adminhtml_Block_Report_Filter_Form
     */
    protected function _prepareForm()
    {
        $values = $this->getFilterData()->getData();

        $actionUrl = $this->getUrl('*/*/*');

        $form = new Varien_Data_Form(
            array(
                'id' => 'filter_form',
                'action' => $actionUrl,
                'method' => 'post'
            )
        );
        $htmlIdPrefix = 'sales_report_';
        $form->setHtmlIdPrefix($htmlIdPrefix);
        $fieldset = $form->addFieldset('base_fieldset',
            array('legend' => Mage::helper('reports')->__('Filter')));

        $fieldset->addField('store_ids', 'hidden', array(
            'name' => 'store_ids'
        ));

        $fieldset->addField('from', 'select', array(
            'name' => 'from',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'label' => Mage::helper('reports')->__('Select Period'),
            'title' => Mage::helper('reports')->__('Select Period'),
            'after_element_html' => '</br><a href="javascript:void(0)" id="show-custom-dates" onclick="showCustomDates(this)"><small>Custom Dates</small></a>',
            'required' => true,
            'values' => $this->_prepareMonthsValues(),
            'value' => isset($values['from'])? $values['from'] : ''
        ));

        $fieldset->addField('filter_from', 'date', array(
                'name'               => 'filter_from',
                'label'              => Mage::helper('reports')->__('From'),
                'tabindex'           => 1,
                'image'              => $this->getSkinUrl('images/grid-cal.gif'),
                'format'             => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                'class'              => isset($values['filter_from'])? 'custom-dates' : 'custom-dates is-hide',
                'value'              => isset($values['filter_from'])? $values['filter_from'] : ''
            )
        );

        $fieldset->addField('filter_to', 'date', array(
                'name'               => 'filter_to',
                'label'              => Mage::helper('reports')->__('To'),
                'after_element_html' => '</br><a href="javascript:void(0)" onclick="hideCustomDates(this)"><small>Hide Custom Dates</small></a>',
                'tabindex'           => 2,
                'image'              => $this->getSkinUrl('images/grid-cal.gif'),
                'format'             => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                'class'              => isset($values['filter_to'])? 'custom-dates' : 'custom-dates is-hide',
                'value'              => isset($values['filter_to'])? $values['filter_to'] : ''
            )
        );


        $currentAdmin = Mage::getSingleton('admin/session')->getUser();

        $positionData = $this->getChildren($currentAdmin->getSalesrepPositionId());

        if($positionData['is_national'] && $positionData['has_children']) {
            $positionTypeData = $this->getPositionTypeData($positionData['children_position_type_id']);

            $options = $this->getOptionPositions($positionData);

            $options[] = array('value' => '', 'label' => $this->__('Please select region'));
//            $options[] = array('value' => 0, 'label' => $this->__('All'));
            sort($options);

            $fieldset->addField('region_id', 'select', array(
                'name' => 'region_id',
                'label' => Mage::helper('reports')->__('Select '. $positionTypeData->getName()),
                'values' => $options,
                'onchange' =>'selectRegion(this)',
                'value' => isset($values['region_id'])? $values['region_id'] : ''
            ));

            foreach($positionData['children'] as $child) {
                $positionData = $this->getChildren($child);
                $positionTypeData = $this->getPositionTypeData($positionData['children_position_type_id']);

                $options = $this->getOptionPositions($positionData);

                $options[] = array('value' => 0, 'label' => $this->__('All'));
                sort($options);

                $canShow = isset($values['office_data'][$child]) && $values['office_data'][$child] != 0;

                $fieldset->addField('office_data['.$child.']', 'select', array(
                    'name' => 'office_data['.$child.']',
                    'label' => Mage::helper('reports')->__('Select ' . $positionTypeData->getName()),
                    'values' => $options,
                    'class' => ($canShow)? 'can-show parent-id-'.$child.' children-of-region': 'parent-id-'.$child.' children-of-region',
                    'value' => isset($values['office_data'][$child])? $values['office_data'][$child] : ''
                ));
            }
        } elseif($positionData['has_children'] && !$positionData['is_national']) {

            $positionTypeData = $this->getPositionTypeData($positionData['children_position_type_id']);

            $options = $this->getOptionPositions($positionData);

            $options[] = array('value' => 0, 'label' => $this->__('All'));
            sort($options);

            $fieldset->addField('office_data', 'select', array(
                'name' => 'office_data',
                'label' => Mage::helper('reports')->__('Select ' . $positionTypeData->getName()),
                'values' => $options,
                'value' => isset($values['office_data'])? $values['office_data'] : ''
            ));
        }

        $fieldset->addField('some_field', 'submit', array(
            'class' => 'no-display',
            'after_element_html' => '<button type="button" onclick="filterFormSubmit()" style="padding-left: 17px;">'. Mage::helper('core')->__('Show Report') .'</button>'
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _initFormValues()
    {
//        $values = $this->getFilterData()->getData();
//
//        if (count($values) <= 0) {
//            $values['order_admins'] = Mage::helper('cminds_positions/dashboard')->getUserId();
//            $date = Zend_Date::now();
//            $firstDay = clone Zend_Date::now();
//
//            $values['from'] = date('n-Y');
//        }
//
//        unset($values['filter_from']);
//        unset($values['filter_to']);
//        $this->getForm()->setValues($values);
    }

    private function _prepareMonthsValues()
    {
        $firstYear = 2012;
        $currentYear = date('Y');
        $currentDate = date('n-Y');
        $months = array();

        $month = array(
            1 => $this->__('January'),
            2 => $this->__('Ferubary'),
            3 => $this->__('March'),
            4 => $this->__('April'),
            5 => $this->__('May'),
            6 => $this->__('June'),
            7 => $this->__('July'),
            8 => $this->__('August'),
            9 => $this->__('September'),
            10 => $this->__('October'),
            11 => $this->__('November'),
            12 => $this->__('December'),
        );

        for ($firstYear; $firstYear <= $currentYear; $firstYear++) {
            for ($i = 1; $i <= 12; $i++) {
                $date = $i . '-' . $firstYear;
                if ((date('n') >= $i && $firstYear == date('Y')) || $firstYear < date('Y')) {
                    $months['01-' . $i . '-' . $firstYear] = $month[$i] . ' ' . $firstYear;
                } else {
                    break;
                }
            }
        }

        return array_reverse($months);
    }

    public function getChildren($adminsPosition){
        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('parent_id', array('eq' => $adminsPosition));

        $adminDataArray = array();

        $hasChildren = true;
        $childrenArray = array();

        if(!$positionsCollection->getSize()){
            $hasChildren = false;
        } else {
            foreach($positionsCollection as $child){
                $childrenArray[] = $child->getId();
            }
        }

        $adminDataArray['children'] = $childrenArray;

        $adminDataArray['has_children'] = $hasChildren;

        $adminDataArray['children_position_type_id'] = $positionsCollection->getFirstItem()->getTypeId();

        if($hasChildren) {
            $positionsCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id',
                    array('in' => $adminDataArray['children']));
        }
        $isNational = false;

        if($positionsCollection->getSize()){
            $isNational = true;
        }

        $adminDataArray['is_national'] = $isNational;


        return $adminDataArray;
    }

    public function getOptionPositions($positionData){

        $result = array();
        if($positionData['has_children']){
            $filteredPositionTypes = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('id', array('in' => $positionData['children']));
            foreach($filteredPositionTypes as $position){
                $result[] = array('value' => $position->getId(), 'label' => $position->getName());
            }
        } else {
            $result[] = array('value' => '', 'label' => $this->__('You have no available values.'));
        }

        return $result;
    }

    public function getPositionTypeData($id){
        return Mage::getModel('cminds_positions/positiontype')->load($id);
    }
}

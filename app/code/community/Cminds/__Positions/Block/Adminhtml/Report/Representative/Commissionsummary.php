<?php
class Cminds_Positions_Block_Adminhtml_Report_Representative_Commissionsummary extends Cminds_Positions_Block_Adminhtml_Report_Representative_Abstract {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('commissionsummary_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = new Varien_Data_Collection();

        $commissions = Mage::getModel('salesrep/salesrep')->getCollection();
        $commissions
            ->getSelect()
            ->joinLeft(array('s' => 'sales_flat_order'), 'main_table.order_id = s.entity_id', 'created_at')
            ->where('main_table.rep_id = ?', $this->getUserId())
            ->where('main_table.rep_commission_status != ?', 'ineligible')
            ->where('main_table.rep_commission_status != ?', 'canceled');

        $filterFrom = $this->getFilter('from');
        $filterTo = $this->getFilter('to');

        if($filterFrom && $filterTo) {
            $date = new Zend_Date($filterFrom, 'd/m/Y');
            $commissions->getSelect()->where('s.created_at >= ?', $date->toString('Y-m-d 00:00:00'));
            $date = new Zend_Date($filterTo, 'd/m/Y');
            $commissions->getSelect()->where('s.created_at <= ?', $date->toString('Y-m-d 23:59:59'));
        }

        $periodPV = 0;
        $periodDownline = 0;

        foreach($commissions AS $c) {
            if(!$c->getIsDownline()) {
                $periodPV += $c->getRepCommissionEarned();
            }
            if($c->getIsDownline()) {
                $periodDownline += $c->getRepCommissionEarned();
            }
        }

        $date = new DateTime('yesterday');

        $subordinatesArray = Mage::helper('cminds_positions')
            ->getAllSubordinates($this->getUserId());

        $canDisplayDownline = Mage::helper('cminds_positions')
            ->canDisplayDownline($subordinatesArray, $date, $this->getUserId());

        if (!$canDisplayDownline) {
            $periodDownline = 0;
        }

        $commissions = Mage::getModel('salesrep/salesrep')->getCollection();
        $commissions
            ->getSelect()
            ->joinLeft(array('s' => 'sales_flat_order'), 'main_table.order_id = s.entity_id', 'created_at')
            ->where('s.created_at >= ?', date('Y-01-01 00:00:00'))
            ->where('s.created_at <= ?', date('Y-m-d H:i:s'))
            ->where('main_table.rep_id = ?', $this->getUserId())
            ->where('main_table.rep_commission_status != ?', 'ineligible')
            ->where('main_table.rep_commission_status != ?', 'canceled');

        $ytdPV = 0;
        $ytdDownline = 0;

        foreach($commissions AS $c) {
            if(!$c->getIsDownline()) {
                $ytdPV += $c->getRepCommissionEarned();
            }
            if($c->getIsDownline()) {
                $ytdDownline += $c->getRepCommissionEarned();
            }
        }

        $commissions = Mage::getModel('salesrep/salesrep')->getCollection();
        $commissions
            ->getSelect()
            ->joinLeft(array('s' => 'sales_flat_order'), 'main_table.order_id = s.entity_id', 'created_at')
            ->where('s.created_at <= ?', date('Y-m-d H:i:s'))
            ->where('main_table.rep_id = ?', $this->getUserId())
            ->where('main_table.rep_commission_status != ?', 'ineligible')
            ->where('main_table.rep_commission_status != ?', 'canceled');

        $lifetimePV = 0;
        $lifetimeDownline = 0;

        foreach($commissions AS $c) {
            if(!$c->getIsDownline()) {
                $lifetimePV += $c->getRepCommissionEarned();
            }
            if($c->getIsDownline()) {
                $lifetimeDownline += $c->getRepCommissionEarned();
            }
        }

        $period = new Varien_Object();
        $period->setTitle("Commission (PV)");
        $period->setPeriod($periodPV);
        $period->setYtd($ytdPV);
        $period->setLifetime($lifetimePV);
        $collection->addItem($period);

        $ytd = new Varien_Object();
        $ytd->setTitle("Commission (Downline)");
        $ytd->setPeriod($periodDownline);
        $ytd->setYtd($ytdDownline);
        $ytd->setLifetime($lifetimeDownline);
        $collection->addItem($ytd);

        $lifetime = new Varien_Object();
        $lifetime->setTitle("Commission (Total)");
        $lifetime->setPeriod($periodPV + $periodDownline);
        $lifetime->setYtd($ytdPV + $ytdDownline);
        $lifetime->setLifetime($lifetimePV + $lifetimeDownline);
        $collection->addItem($lifetime);

        $this->setCollection($collection);
    }

    protected function _prepareColumns() {
      $baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
        $this->addColumn('title', array(
            'header'    => Mage::helper('salesrep')->__('COMMISSION SUMMARY'),
            'column_css_class' => 'main_header',
            'width'     => '25%',
            'index'     => 'title',
        ));
        $this->addColumn('period', array(
            'header'    => Mage::helper('salesrep')->__('Period'),
            'width'     => '25%',
            'index'     => 'period',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
        ));
        $this->addColumn('ytd', array(
            'header'    => Mage::helper('salesrep')->__('YTD'),
            'width'     => '25%',
            'index'     => 'ytd',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
        ));
        $this->addColumn('lifetime', array(
            'header'    => Mage::helper('salesrep')->__('Lifetime'),
            'width'     => '25%',
            'index'     => 'lifetime',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
        ));
        return parent::_prepareColumns();
    }
}

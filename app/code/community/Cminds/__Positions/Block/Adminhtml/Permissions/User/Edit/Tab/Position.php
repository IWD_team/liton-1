<?php
class Cminds_Positions_Block_Adminhtml_Permissions_User_Edit_Tab_Position
  extends Mage_Adminhtml_Block_Widget_Form
  implements Mage_Adminhtml_Block_Widget_Tab_Interface {

  public function __construct() {
    parent::__construct();
    $this->setTemplate('cminds_positions/permissions/user/edit/tab/position.phtml');
  }

  public function getPositionTypes() {
    return Mage::getModel("cminds_positions/positiontype")->getCollection();
  }

  public function getPosition($typeId) {
    return Mage::getModel("cminds_positions/position")
        ->getCollection()
        ->addFieldToFilter('type_id', array('eq' => $typeId));
  }

  public function getCurrentPosition() {
      $current_user = Mage::registry('permissions_user');
      return $current_user->getData('salesrep_position_id');
  }

  /**
  * Prepare label for tab
  *
  * @return string
  */
  public function getTabLabel() {
    return $this->__('Position');
  }

  /**
  * Prepare title for tab
  *
  * @return string
  */
  public function getTabTitle() {
    return $this->__('Position');
  }

  /**
  * Returns status flag about this tab can be shown or not
  *
  * @return true
  */
  public function canShowTab() {
    $is_admin = Mage::getSingleton('admin/session')->isAllowed('system/config');
    $edit_positions = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_positions');
//
    if ($is_admin || $edit_positions) {
      return true;
    }
    return false;
  }

  /**
  * Returns status flag about this tab hidden or not
  *
  * @return true
  */
  public function isHidden() {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return false;
    }

    $is_admin = Mage::getSingleton('admin/session')->isAllowed('system/config');
    $edit_positions = Mage::getSingleton('admin/session')->isAllowed('salesrep/user_management/edit_positions');

    if ($is_admin || $edit_positions) {
      return false;
    }

    return true;
  }
}

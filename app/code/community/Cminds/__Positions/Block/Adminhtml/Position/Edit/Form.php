<?php

class Cminds_Positions_Block_Adminhtml_Position_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        if (Mage::registry('position_data')){
            $data = Mage::registry('position_data')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));

        $form->setUseContainer(true);

        $this->setForm($form);

        $fieldset = $form->addFieldset('position_form', array(
            'legend' =>Mage::helper('cminds_positions')->__('Position Info')
        ));

        $fieldset->addField('id', 'hidden', array(
            'name'      => 'id',
        ));

        $collection = Mage::getModel('cminds_positions/position')->getCollection();

        $parentPositions = array();

        $parentPositions[0] = Mage::helper('cminds_positions')->__('No Parent Position');

        foreach($collection As $row) {
            if(isset($data['id']) && $row->getId() == $data['id']) continue;
            $parentPositions[$row->getId()] = $row->getName();
        }

        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('cminds_positions')->__('Position Name'),
            'name'      => 'name',
            'required'  => true
        ));
        $fieldset->addField('parent_id', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Parent Position'),
            'name'      => 'parent_id',
            'values'    => $parentPositions,
        ));

        $collection = Mage::getModel('cminds_positions/positiontype')->getCollection();

        $parentTypes = array();

        $parentTypes[0] = Mage::helper('cminds_positions')->__('No Parent Type');

        foreach($collection As $row) {
            $parentTypes[$row->getId()] = $row->getName();
        }

        $fieldset->addField('type_id', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Type Name'),
            'name'      => 'type_id',
            'values'    => $parentTypes,
        ));
        $form->setValues($data);

        return parent::_prepareForm();
    }
}
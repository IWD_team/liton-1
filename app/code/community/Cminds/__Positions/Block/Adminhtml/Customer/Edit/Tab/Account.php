<?php

class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Account extends Mage_Adminhtml_Block_Customer_Edit_Tab_Account
{

    /**
     * Initialize form
     *
     * @return Mage_Adminhtml_Block_Customer_Edit_Tab_Account
     */
    public function initForm()
    {
        parent::initForm();

        $isStoreAdmin = Mage::helper('cminds_positions')->isStoreAdmin();

        if(!$isStoreAdmin) {
            $form = $this->getForm();
            $fieldsets = $form->getElements();

            $availableFieldsArray = array(
              'firstname',
              'middlename',
              'lastname',
              'suffix',
              'email',
              'suffix',
              'company',
            );

            unset($fieldsets[1]);
            foreach ($fieldsets as $fieldset) {
                $fields = $fieldset->getElements();

                foreach ($fields as $i => $field) {
                    $fieldId = $field->getId();
                    if(in_array($fieldId, $availableFieldsArray)) {
                        $field->setDisabled('disabled');
                    } else {
                        unset($fields[$i]);
                    }
                }
            }
        }
        return $this;
    }
}

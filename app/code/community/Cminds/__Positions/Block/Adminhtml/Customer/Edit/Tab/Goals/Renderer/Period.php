<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals_Renderer_Period extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $periodType = $row->getData('period_type');
        $monthsArray = Cminds_Positions_Model_Source_MonthsList::toOptionArray();
        $year = $row->getData('period_year');

        if($periodType == 1) {
            $periodMonth = $row->getData('period_months');
            foreach($monthsArray as $month){
                if($month['value'] == intval($periodMonth)){
                    return $month['label'] . ' ' . $year;
                }
            }
        } else {
            return $year;
        }
    }
}
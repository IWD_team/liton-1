<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals_Renderer_Sales extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $periodType = $row->getData('period_type');

        if($periodType == 1) {
            $periodMonth = $row->getData('period_months');
            $periodYear = $row->getData('period_year');
            $startDate = date('Y-m-d 00:00:00',mktime(0,0,0,$periodMonth,1,$periodYear));
            $endDate = date('Y-m-t 23:59:59',mktime(0,0,0,$periodMonth,1,$periodYear));
        } else {
            $periodYear = $row->getData('period_year');
            $startDate = date('Y-m-d 00:00:00',mktime(0,0,0,1,1,$periodYear));
            $endDate = date('Y-m-t 23:59:59',mktime(0,0,0,12,1,$periodYear));
        }

        $salesCollection = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('customer_id', array('eq' => $row->getCustomerId()))
            ->addFieldToFilter('created_at', array('gteq' => $startDate))
            ->addFieldToFilter('created_at', array('lteq' => $endDate));

        $salesAmount = 0;

        foreach($salesCollection as $order){
            if($order->getTotalInvoiced() != null || $order->getTotalInvoiced() != 0){
                $salesAmount += $order->getTotalInvoiced();
            }
        }

        return Mage::helper('core')->currency($salesAmount, true, false);
    }
}
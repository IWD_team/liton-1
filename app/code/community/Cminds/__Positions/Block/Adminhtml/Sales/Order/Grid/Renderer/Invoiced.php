<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_Invoiced extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $totalInvoiced = 0;
        $orderModel = Mage::getModel('sales/order')->load($row->getId());
        if($orderModel->getId()){
            if($orderModel->getTotalInvoiced()){
                $totalInvoiced = $orderModel->getTotalInvoiced();
            }
        }
        return Mage::helper('core')->currency($totalInvoiced, true, false);
    }
}
<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View
{

    public function __construct()
    {
        parent::__construct();

        if(!$this->canSeeElement()){
            $this->_removeButton('delete');
            $this->_removeButton('reset');
            $this->_removeButton('save');
            $this->_removeButton('order_edit');
            $this->_removeButton('order_cancel');
            $this->_removeButton('send_notification');
            $this->_removeButton('order_creditmemo');
            $this->_removeButton('void_payment');
            $this->_removeButton('order_hold');
            $this->_removeButton('order_unhold');
            $this->_removeButton('accept_payment');
            $this->_removeButton('deny_payment');
            $this->_removeButton('get_review_payment_update');
            $this->_removeButton('order_invoice');
            $this->_removeButton('order_ship');
            $this->_removeButton('order_reorder');
        }
    }

    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }
}

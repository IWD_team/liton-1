<?php
class Cminds_Positions_Block_Adminhtml_Notifications_Container_List extends Mage_Adminhtml_Block_Widget_Grid {

  public function __construct()
  {
    parent::__construct();

    $this->setDefaultSort('office_id');
    $this->setId('salesrep_notifications_grid');
    $this->setDefaultDir('desc');
    $this->setUseAjax(true); // Using ajax grid is important
    $this->setDefaultSort('entity_id');
    $this->setDefaultFilter(array('in_products'=>1)); // By default we have added a filter for the rows, that in_products value to be 1
    $this->setSaveParametersInSession(false);
  }

  protected function _prepareLayout()
  {
    parent::_prepareLayout();
    $this->unsetChild('export_button');
    $this->unsetChild('reset_filter_button');
    $this->unsetChild('search_button');

    $this->setChild('save_notifications_button',
        $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'     => Mage::helper('adminhtml')->__('Save'),
                'onclick'   => "saveCustomerNotification('{$this->saveCustomerNotigicationUrl()}')",
                'class'   => 'task'
            ))
    );
  }

  public function getSaveButtonHtml(){
    return $this->getChildHtml('save_notifications_button');
  }

  public function getMainButtonsHtml()
  {
    $html = '';
    if($this->getFilterVisibility()){
      $html .= $this->getSaveButtonHtml();
    }
    return $html;
  }

  protected function _prepareCollection()
  {
    $params = $this->getRequest()->getParams();
    $collection = Mage::getModel('customer/customer')->getCollection();

    if(isset($params['customer_id'])){
      if($params['customer_id'] != 0){
        $collection
            ->addFieldToFilter('entity_id', array('eq' => $params['customer_id']));
      } elseif($params['region_id'] != 0) {
        $regionId = $params['region_id'];
        $officeId = $params['office_id'][$regionId];
        $salesrepArray = $this->getSalesrepsForFilter($regionId, $officeId);

        $collection
            ->getSelect()
            ->where('salesrep_rep_id IN ('. implode(', ',$salesrepArray).')');
      }
    }
    $this->setCollection($collection);

    return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
    $this->addColumn('email', array(
        'header'    => Mage::helper('cminds_positions')->__('Customer Name'),
        'index'     => 'email',
        'renderer'  => 'Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Customer',
        'filter_condition_callback' => array($this, 'filterConditionCallback')
    ));
    $this->addColumn('customer', array(
        'header'    => Mage::helper('cminds_positions')->__('Office'),
        'index'     => 'entity_id',
        'renderer'  => 'Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Office',
        'filter_condition_callback' => array($this, 'filterConditionCallback')
    ));

    $this->addColumn('entity_id', array(
        'header_css_class' => 'a-center',
        'type'            => 'checkbox',
        'name'            => 'in_products',
        'values'          => $this->_getSelectedProducts(),
        'align'           => 'center',
        'index'           => 'entity_id',
        'filter_condition_callback' => array($this, 'filterConditionCallback')
    ));

    $this->addColumn('position', array(
        'header'            => Mage::helper('catalog')->__('ID'),
        'name'              => 'position',
        'width'             => 60,
        'type'              => 'number',
        'validate_class'    => 'validate-number',
        'index'             => 'position',
        'column_css_class'  =>'no-display',
        'header_css_class'  =>'no-display',
        'editable'          => true,
        'edit_only'         => true
    ));

    return parent::_prepareColumns();
  }

  public function getGridUrl()
  {
    return $this->getUrl('*/notifications/notificationgrid', array('_current'=>true));
  }

  protected function filterConditionCallback($collection, $column)
  {
    if (!$value = $column->getFilter()->getValue()) {
      if($column->getId() != 'entity_id') {
        return $this;
      }
    }
    if (empty($value) && $column->getId() != 'entity_id') {
      return $this;
    } else {
      if($column->getId() == 'email') {
        $customerCollection = Mage::getModel('customer/customer')->getCollection()
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname');

        $customerIdsArray = array();
        foreach($customerCollection as $customer){
          if(strpos($customer->getName(), $value) !== false) {
            $customerIdsArray[] = $customer->getId();
          }
        }
        $collection
            ->addAttributeToFilter(
                'entity_id', array('in' => $customerIdsArray)
            );
      }

      if($column->getId() == 'customer') {
        $filteredCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter(
                'name', array('like' => '%' . $value . '%')
            );

        $positionIdsArray = array();
        foreach($filteredCollection as $position){
          $positionIdsArray[] = $position->getId();
        }

        $matchingReps = array();
        $adminUserCollection = Mage::getModel('admin/user')
            ->getCollection()
            ->addFieldToFilter('salesrep_position_id', array('in' => $positionIdsArray));

        foreach($adminUserCollection as $admin){
          $matchingReps[] = $admin->getUserId();
        }

        $collection
          ->getSelect()
          ->where('salesrep_rep_id IN ('. implode(', ',$matchingReps).')');
      }

      if($column->getId() == 'entity_id'){
        if($value) {
          $collection
              ->getSelect()
              ->where('`e`.salesrep_notify_when_order_placed = 1 OR salesrep_notify_when_order_shipped = 1');
        } else {
          $collection
              ->getSelect()
              ->where('salesrep_notify_when_order_placed = 0 AND salesrep_notify_when_order_shipped = 0');
        }
      }
    }
    return $this;
  }

  public function isStoreAdmin(){
    return Mage::helper('cminds_positions')->isStoreAdmin();
  }

  public function isTopLevel(){
    return Mage::helper('cminds_positions')->isTopLevel();
  }

  public function getSalesrepsForFilter($region, $office){

    $officesArray = array();
    if($office == 0){
      $positionModel = Mage::getModel('cminds_positions/position')
          ->getCollection()
          ->addFieldToFilter('parent_id', array('eq' => $region));

      foreach($positionModel as $position){
        $officesArray[] = $position->getId();
      }
    } else {
      $officesArray[] = $office;
    }

    $adminUsersCollection = Mage::getModel('admin/user')
        ->getCollection()
        ->addFieldToFilter('salesrep_position_id', array('in' => $officesArray));

    $salesrepArray = array();

    foreach($adminUsersCollection as $admin){
      $salesrepArray[] = $admin->getId();
    }

    return $salesrepArray;
  }

  public function saveCustomerNotigicationUrl(){
    return $this->getUrl('*/*/saveNotifications');
  }

  protected function _getSelectedProducts()
  {
    $products = $this->getProductsUpsell();
    if (!is_array($products)) {
      $products = array_keys($this->getSelectedUpsellProducts());
    }
    return $products;
  }

  public function getSelectedUpsellProducts()
  {
    $customers = array();
    $collection = Mage::getModel('customer/customer')->getCollection();
    foreach ($collection as $customer) {
      if($customer->getSalesrepNotifyWhenOrderPlaced() || $customer->getSalesrepNotifyWhenOrderShipped()) {
        $customers[$customer->getId()] = array('position' => $customer->getId());
      }
    }
    return $customers;
  }
}

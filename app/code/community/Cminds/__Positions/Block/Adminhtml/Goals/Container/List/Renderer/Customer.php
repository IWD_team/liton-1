<?php

class Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Customer
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $customerId = $row->getData('customer_id');

        if($customerId == 0 || $customerId == null) {
            return '';
        } else {
            $customer = Mage::getModel('customer/customer')->load($customerId);

            if($customer->getId()) {
                return $customer->getName();
            }
        }
    }
}
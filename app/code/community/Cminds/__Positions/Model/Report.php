<?php
class Cminds_Positions_Model_Report extends Mage_Reports_Model_Report {

  protected $_reportModel;

  public function initCollection($modelClass)
  {
    $this->_reportModel = Mage::getResourceModel($modelClass);

    return $this;
  }

}

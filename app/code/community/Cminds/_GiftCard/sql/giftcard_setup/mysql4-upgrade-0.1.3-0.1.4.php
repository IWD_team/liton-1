<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('sales_flat_quote_item')} ADD `delivery_date` varchar(255) DEFAULT NULL;

")->endSetup();
<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('giftcard/giftcard'),
        'refill_order_ids',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'    => 256,
            'unsigned'  => true,
            'nullable'  => true,
            'COMMENT'   => 'Refill Order Ids'
        )
    );

$installer->endSetup();
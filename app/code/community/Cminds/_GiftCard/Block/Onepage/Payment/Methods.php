<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Onepage_Payment_Methods extends Mage_Checkout_Block_Onepage_Payment_Methods
{
    /**
     * Gets the gift card payment method
     * @return object $html
     */
	public function _toHtml()
	{
	    $this->setTemplate('cminds_giftcard/checkout/onepage/payment/gc-methods.phtml');
        if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
    }
}

<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Giftcard extends Mage_Core_Block_Template
{	/**
         * Gets giftcard amount totals
         * @return type 
         */
	public function getCardAmounts()
	{
		return explode(",", Mage::getStoreConfig('sales/giftcard/amounts'));
	}
 
}
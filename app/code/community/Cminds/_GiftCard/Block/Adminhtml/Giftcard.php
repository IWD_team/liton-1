<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Adminhtml_Giftcard extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_giftcard';
        $this->_blockGroup = 'giftcard';
        $this->_headerText = Mage::helper('giftcard')->__('Manage Gift Cards');
        $this->_addButtonLabel = Mage::helper('giftcard')->__('Add Free Gift Card');

        $data = array(
            'label' => 'Add Bulk Free Gift Card',
            'onclick' => "javascript:openConfigPopup('" . Mage::helper("adminhtml")->getUrl("adminhtml/giftcard/bulk") . "')"
        );
        Mage_Adminhtml_Block_Widget_Container::addButton('Add_Bulk_Free_Gift_Card', $data, 0, 100, 'header', 'header');

        parent::__construct();
    }

}
<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Adminhtml_Giftcard_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Constructs tabs to view the gift cards information
     */
  public function __construct()
  {
      parent::__construct();
      $this->setId('giftcard_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('giftcard')->__('Gift Card'));
  }
  /**
   * Creates the tabs before sending them into HTML
   * @return object 
   */
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('giftcard')->__('Details'),
          'title'     => Mage::helper('giftcard')->__('Details'),
          'content'   => $this->getLayout()->createBlock('giftcard/adminhtml_giftcard_edit_tab_form')->toHtml(),
      ));
      
      $this->addTab('reporting_section', array(
          'label'     => Mage::helper('giftcard')->__('History'),
          'title'     => Mage::helper('giftcard')->__('History'),
          'content'   => $this->getLayout()->createBlock('giftcard/adminhtml_giftcard_edit_tab_history')->toHtml(),
      ));

      return parent::_beforeToHtml();
  }
}
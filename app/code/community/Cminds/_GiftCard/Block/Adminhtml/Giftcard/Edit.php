<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Adminhtml_Giftcard_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Constructs the grid for editing the gift card
     */
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'giftcard';
        $this->_controller = 'adminhtml_giftcard';
        
        $this->_updateButton('save', 'label', Mage::helper('giftcard')->__('Save Entry'));
        $this->_updateButton('delete', 'label', Mage::helper('giftcard')->__('Delete Entry'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
			Event.observe('type','change',function(){
				if($('type').value == '1'){
					$('send_email').value = '0';
					$('send_email').disable();
				}else if($('type').value == '2'){
					$('send_email').enable();
				}
			})
            function toggleEditor() {
                if (tinyMCE.getInstanceById('recipes_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'recipes_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'recipes_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    /**
     * Sets the header text for the grid
     * @return string 
     */
    public function getHeaderText()
    {
        if( Mage::registry('giftcard_data') && Mage::registry('giftcard_data')->getId() ) {
            return Mage::helper('giftcard')->__("Gift Card '%s'", $this->htmlEscape(Mage::registry('giftcard_data')->getNumber()));
        } else {
            return Mage::helper('giftcard')->__('Add Entry');
        }
    }
}
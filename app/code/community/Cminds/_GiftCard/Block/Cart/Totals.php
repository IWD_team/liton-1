<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Cart_Totals extends Mage_Checkout_Block_Cart_Totals
{
    /**
     * Renders the totals in the cart in HTMl
     * @return object $html 
     */
	public function _toHtml()
	{
		$str = Mage::app()->getFrontController()->getRequest()->getPathInfo();
    	if($str == '/checkout/onepage/savePayment/') {
    		$this->setTemplate('cminds_giftcard/checkout/onepage/review/gc-totals.phtml');
    	}
		if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
	}
	/** 
         * Gets the gift cards for the cart
         * @return tobject $cards
         */
	public function getGiftCards()
	{
		$cards = Mage::getModel('giftcard/payment')->getCollection()
			->addFieldToFilter('quote_id', $this->getQuote()->getId());
		$cards->getSelect()
			->join(array('gc' => (string)Mage::getConfig()->getTablePrefix().'giftcard_entity'),
				'main_table.giftcard_id = gc.giftcard_id',
				array('number'));
		
		return $cards;
	}
}

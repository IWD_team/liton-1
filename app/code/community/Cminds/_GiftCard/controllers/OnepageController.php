<?php
/**
 * CreativeMindsSolutions
 */
require_once 'Mage/Checkout/controllers/OnepageController.php';

class Cminds_GiftCard_OnepageController extends Mage_Checkout_OnepageController
{
    /**
     * If a successfull order, run this
     * @return type 
     */
	public function successAction()
    {
        if (!$this->getOnepage()->getCheckout()->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $this->getOnepage()->getCheckout()->getLastQuoteId();
        $lastOrderId = $this->getOnepage()->getCheckout()->getLastOrderId();

        if (!$lastQuoteId || !$lastOrderId) {
            $this->_redirect('checkout/cart');
            return;
        }

        Mage::getSingleton('checkout/session')->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action',array('order_id'=>$lastOrderId, 'quote_id' => $lastQuoteId));
        $this->renderLayout();
    }
}
<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_Resource_Payment_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('giftcard/payment');
    }
}

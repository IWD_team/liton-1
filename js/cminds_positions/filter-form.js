document.observe('dom:loaded', function(){
    $$('#sales_report_base_fieldset .children-of-region').each(function(el){
        if(!el.hasClassName('can-show')) {
            el.up('tr').hide();
        }
    });
    if($$('#sales_report_base_fieldset .custom-dates')[0]) {
        if ($$('#sales_report_base_fieldset .custom-dates')[0].hasClassName('is-hide')) {
            $$('#sales_report_base_fieldset .custom-dates').each(function (el) {
                el.up('tr').hide();
            });
        } else {
            $$('#sales_report_base_fieldset #show-custom-dates')[0].hide();
        }
    }

    $$('#goals_form .dynamic-select-goal-fields').each(function(el){
        if(el.options[el.selectedIndex].value == 0) {
            el.up('tr').hide();
        }
    });

    $$('#goals_form .period').each(function(el){
        if(el.options[el.selectedIndex].value == 0) {
            el.up('tr').hide();
        }
    });
});

function selectRegion(obj){
    var optionId = obj.options[obj.selectedIndex].value;
    $$('#sales_report_base_fieldset .children-of-region').each(function(el){
        el.selectedIndex = 0;
        var test = el.options[el.selectedIndex].value;
        console.log(test);
        el.up('tr').hide();
    });
    $$('#sales_report_base_fieldset .parent-id-'+ optionId).each(function(el){
        el.up('tr').show();
    });
}

function showCustomDates(el){
    $$('#sales_report_base_fieldset .custom-dates').each(function(el){
        el.up('tr').show();
    });
    $$('#sales_report_base_fieldset #show-custom-dates')[0].hide();
}
function hideCustomDates(el){

    $$('#sales_report_base_fieldset #sales_report_filter_from')[0].value = '';
    $$('#sales_report_base_fieldset .custom-dates').each(function(el){
        el.value = '';
        el.up('tr').hide();
    });
    $$('#sales_report_base_fieldset #show-custom-dates')[0].show();
}

function chooseGoalType(obj){
    var optionId = obj.options[obj.selectedIndex].value;
    $$('#goals_form .dynamic-select-goal-fields').each(function(el){
        el.up('tr').hide();
    });
    if(optionId == 1){
        $$('#goals_form #customer_id')[0].up('tr').show();
    }
    if(optionId == 2 || optionId == 3){
        $$('#goals_form #region_id')[0].up('tr').show();
    }
}

function chooseGoalRegion(obj){
    var optionId = obj.options[obj.selectedIndex].value;
    var goalType = $$('#goals_form #type_id')[0].value;
    $$('#goals_form .offices').each(function(el){
        el.up('tr').hide();
    });
    if(goalType == 2){
        if(optionId != 0) {
            $$('#goals_form .office-' + optionId)[0].up('tr').show();
        }
    }
}

function chooseGoalPeriodType(obj) {
    var optionId = obj.options[obj.selectedIndex].value;
    var periodType = $$('#goals_form #period_type')[0].value;
    $$('#goals_form .period').each(function (el) {
        el.up('tr').hide();
    });
    if (optionId != '') {
        if (periodType == 1) {
            $$('#goals_form .period-month')[0].up('tr').show();
            $$('#goals_form .period-year')[0].up('tr').show();
        }

        if (periodType == 2) {
            $$('#goals_form .period-year')[0].up('tr').show();
        }
    }

}

function chooseNotificationRegion(obj, officeId){
    if(obj == null){
        var optionId = officeId;
    } else {
        var optionId = obj.options[obj.selectedIndex].value;
    }
    $$('#notifications_form .offices').each(function (el) {
        el.up('tr').hide();
    });
    $$('#notifications_form #customer_id option').each(function (el, d) {
        el.show();
        if(optionId == 0){
            el.show();
        } else if (optionId != el.readAttribute('data-region')){
            el.hide();
            $$('#notifications_form .office-' + optionId)[0].up('tr').show();
        }
    });
}

function chooseNotificationOffice(obj){
    var optionId = obj.options[obj.selectedIndex].value;
    var regionId = $$('#notifications_form #region_id')[0].value;
    $$('#notifications_form #customer_id option').each(function (el, d) {
        el.show();
        if(optionId == 0){
            el.show();
            chooseNotificationRegion(null, optionId);
        } else if (optionId != el.readAttribute('data-office')){
            el.hide();
        }
    });
}

function changeDefaultShipmentStatusView(obj, id){
    var optionId = obj.options[obj.selectedIndex].value,
        element = $(obj),
        closestGrid = element.next('.grid'),
        tbody = closestGrid.down('tbody'),
        tr = tbody.select('tr');

    tr.each(function(item){
        var shippingColumn = item.select('.shipping-status'),
            shippingStatus = shippingColumn[0].innerText.trim();
        item.setStyle({display: 'table-row'});
        if(optionId == '0'){
            item.setStyle({display: 'table-row'});
        }
        if(optionId == '1'){
            if(shippingStatus == 'Shipped'){
                item.setStyle({display: 'table-row'});
            }
            if(shippingStatus == 'Not Shipped'){
                item.setStyle({display: 'none'});
            }
            if(shippingStatus == 'Partially Shipped'){
                item.setStyle({display: 'none'});
            }
        }

        if(optionId == '2'){
            if(shippingStatus == 'Shipped'){
                item.setStyle({display: 'none'});
            }
            if(shippingStatus == 'Not Shipped'){
                item.setStyle({display: 'table-row'});
            }
            if(shippingStatus == 'Partially Shipped'){
                item.setStyle({display: 'none'});
            }
        }


        if(optionId == '3'){
            if(shippingStatus == 'Shipped'){
                item.setStyle({display: 'none'});
            }
            if(shippingStatus == 'Not Shipped'){
                item.setStyle({display: 'none'});
            }
            if(shippingStatus == 'Partially Shipped'){
                item.setStyle({display: 'table-row'});
            }
        }
    });
}

function saveCustomerNotification(url){

    var hiddenInput = $$('input[name=links[customers_notified]]')[0].value;

    new Ajax.Request(url, {
        method: 'get',
        parameters: {
            serialized_notifications: hiddenInput
        },
        onSuccess: function(response){
            var res = JSON.parse(response.responseText);
            if(res.success){
                alert('Notifications has beed saved.');
            }
        }
    });

}
function openConfigPopup(url) {
       
        /*if ($("browser_window") && typeof(Windows) != "undefined") {
            Windows.focus("browser_window");
            return;
       }*/
        var dialogWindow = Dialog.info(null, {
            closable: true,
            resizable: false,
            draggable: true,
            className: "magento",
            windowClassName: "popup-window",
            title: "Generate Cards",
            top: 50,
            width: 700,
            height: 200,
            zIndex: 1000,
            recenterAuto: false,
            hideEffect: Element.hide,
            showEffect: Element.show,
            id: "browser_window",
            url: url,
            onClose: function(param, el) {
                window.location.reload();
            }
        });
    }
    
 function closePopup() {
        Windows.close("browser_window");
    }
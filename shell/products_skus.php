<?php
require_once 'abstract.php';

class Cminds_Shell_Process_Product_Skus extends Mage_Shell_Abstract
{
    /**
     * Run script.
     *
     * @return void
     */
    public function run()
    {
        Mage::getModel('import/products')->run();
    }
}

$shell = new Cminds_Shell_Process_Product_Skus();
$shell->run();

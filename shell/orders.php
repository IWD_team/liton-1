<?php

require_once 'abstract.php';

class Cminds_Shell_Process_Orders extends Mage_Shell_Abstract
{
    /**
     * Run script.
     *
     * @return void
     */
    public function run()
    {
        Mage::getModel('import/orders')->createOrders();
    }
}

$shell = new Cminds_Shell_Process_Orders();
$shell->run();
